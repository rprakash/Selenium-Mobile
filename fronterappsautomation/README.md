# Fronter App Automation framework 
[![Build Status](https://jenkins.fronter.net/job/Android-Fronter-Notification-Test/badge/icon)](https://jenkins.fronter.net/job/Android-Fronter-Notification-Test)

Contains automated tests for fronter mobile apps. This framework has built on top of our main [Y12 Selenium Framework](https://gitlab.fronter.net/qa/fronter-selenium).

[Read more about Framework](https://gitlab.fronter.net/qa/fronter-selenium#fronter-selenium-automation)

#### Requirements

* NodeJS
* Microsoft Dot Net framework
* Android SDK
* Appium Server
* Xcode

#### Appium download and install

Appium server can be downloaded from [Appium Downloads](http://appium.io/downloads.html) .
For documentation for Appium visit [Appium Documentation](http://appium.io/slate/en/master/?java#) .
Appium server must be started before running any test.

#### Configuring the execution

You will find all the available configuration options in the `default.properties` file like platfrom name, version etc.
`default.properties` itself explains alot about confiuration parameters to use for Android/iOS.

You can also specify options from the command line, like this:

```
ant -Dsuite=NotificationAndroid -Dinvoke.android.driver=yes -Dinitialize.browser=Firefox -Dmobile.app.path= /path/apk or app file
```
__-Dsuite__ -- Name of the xml file

__-Dinvoke.android.driver/-Dinvoke.iOS.driver__ -- if executed on android then -Dinvoke.android.driver should be kept 'yes'

__-Dinitialize.browser(optional)__ -- browser on which CreateTestData will run (if not mentioned then test will execute on firefox by default), use firefox 31 for stability

__-Dmobile.app.path__ -- /path/apk or /app file

* __Note__: For now framework supports running tests on production apk file, the credentials given inside repo will work for production only.

#### Test Data creation

We have one script named `CreateTestData`, which creates News, assignment etc. to be used by tests. Once execution is successfully finished, script saves the test data into a properties file named `test.properties` and then the rest of the tests files reads data from that properties file and does execution. 

When we run our tests via ant, first test which is executed is `CreateTestData` and after that the AndroidDriver/iOSDriver is initialized which runs other tests.

#### Supported platforms

__Android__
* Devices: All devices (tried on Samsung, Google Nexus, Cyanogenmod, HTC)
* OS: 4.1.x to 5.1.x

__iOS__
* Devices: All devices (tried on 5c, 4s)
* iOS: 8.x