package org.fronter.apps.automation.sdk;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.automation.sdk.Configuration;
import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

public class AndroidWait {
    private AndroidDriver androidDriver;
    private Configuration config = Configuration.getInstance();
    private int timeout = Integer.parseInt(config.get("selenium.explicitWaitSeconds"));

    public AndroidWait(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Waits until the given element is visible
     *
     * @param element Element which needs to be checked for visibility
     */
    public void untilElementIsVisible(By elementIdentifer) {
        WebDriverWait wait = new WebDriverWait(androidDriver, timeout);
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementIdentifer));
    }
    
    /**
     * Waits until the given element is invisible
     *
     * @param element Element which needs to be checked for invisibility
     */
    public void untilElementIsInvisible(By elementIdentifer, int time) {
        WebDriverWait wait = new WebDriverWait(androidDriver, time);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(elementIdentifer));
    }
    
    /**
     * Ensures that clicking on an element results in a second element being displayed.
     * Use this method when the element is dynamically created or the WebElement object is not available yet.
     * Do not use when the click is going to trigger navigation to a different page.
     *
     * @param clickOnThis Element to click
     * @param selector Selector that identifies the element being displayed
     */
    public void untilClickDisplaysElement(final WebElement clickOnThis, final By selector) {
        WebDriverWait wait = new WebDriverWait(androidDriver, timeout);
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            wait.until(new Function<WebDriver, Boolean>() {
                public Boolean apply(WebDriver driver) {
                    try {
                        clickOnThis.click();
                        WebElement element = driver.findElement(selector);
                        return element.isDisplayed();
                    } catch (Exception e) {
                        return false;
                    }
                }
            });
        } catch (Exception e) {
            throw e;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Ensures that clicking on an element will display another element.
     * Use ONLY on dynamic pages (not when the click will load a new page).
     *
     * @param clickOnThis Element to click
     * @param displaysThis Element that will be displayed
     */
    public void untilClickDisplaysElement(final WebElement clickOnThis, final WebElement displaysThis) {
        WebDriverWait wait = new WebDriverWait(androidDriver, timeout);
        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                if (!displaysThis.isDisplayed()) {
                    clickOnThis.click();
                }
                return displaysThis.isDisplayed();
            }
        });
    }
}
