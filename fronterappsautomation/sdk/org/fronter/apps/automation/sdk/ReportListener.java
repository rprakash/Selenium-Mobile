package org.fronter.apps.automation.sdk;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.fronter.automation.sdk.Configuration;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

/**
 * Contains listeners to be used during the test execution for taking screen shots when test fails.
 * No need to call this class or its instance anywhere.
 */
public class ReportListener extends TestListenerAdapter {

	private Configuration config = Configuration.getInstance();
	private boolean invokeAndroidDriver = config.get("invoke.android.driver").equalsIgnoreCase("yes");
	private boolean invokeIOSDriver = config.get("invoke.ios.driver").equalsIgnoreCase("yes");

    @Override
    public void onTestFailure(ITestResult result) {
        // Obtain the screenshot
        Object currentClass = result.getInstance();
        WebDriver webDriver;

        if (result.getTestClass().toString().contains("CreateTestData")) {
            webDriver = ((FronterTestBase) currentClass).driver;
        } else if (invokeAndroidDriver) {
        	webDriver = ((FronterAppsTestBase) currentClass).androidDriver;
        } else if (invokeIOSDriver) {
        	webDriver = ((FronterAppsTestBase) currentClass).iOSDriver;
        } else {
        	webDriver = ((FronterTestBase) currentClass).driver;
        }

        File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);

        // Create the destination folder
        String destDir = Configuration.getInstance().get("tests.report");
        destDir += "/screenshots";

        // Create the folder that contains the screenshots (if it doesn't exist)
        new File(destDir).mkdirs();

        String destFile = destDir + "/" + result.getName() + ".png";
        try {
            FileUtils.copyFile(scrFile, new File(destFile));
        } catch (IOException e) {
            System.err.println("Could not copy the screenshot file to " + destFile);
            e.printStackTrace();
        }
        super.onTestFailure(result);
    }
}
