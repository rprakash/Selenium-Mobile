package org.fronter.apps.automation.sdk.feedback.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.feedback.pages.android.GiveFeedbackPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Give Feedback Tool Page Class
 */
public class GiveFeedbackTool {

    private AndroidDriver androidDriver;

    public GiveFeedbackTool(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of GiveFeedbackPageAndroid class.
     *
     * @return Instance of page object which models GiveFeedbackPageAndroid
     */
    public GiveFeedbackPageAndroid giveFeedbackPage() {
        return PageFactory.initElements(androidDriver, GiveFeedbackPageAndroid.class);
    }

    /**
     * Tap on Send button
     */
    public void clickSendButton() {
        giveFeedbackPage().clickSendButton();
    }

    /**
     * Input feedback text and email
     *
     * @param feedbackText Feedback text and email
     * @param email Email
     */
    public void inputFeedback(String feedbackText, String email) {
        giveFeedbackPage().inputFeedback(feedbackText, email);
    }
}
