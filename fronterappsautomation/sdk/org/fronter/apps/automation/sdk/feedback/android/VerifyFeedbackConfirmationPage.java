package org.fronter.apps.automation.sdk.feedback.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.feedback.pages.android.FeedbackConfirmationPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields on Feedback confirmation page
 */
public class VerifyFeedbackConfirmationPage {
    AndroidDriver androidDriver;

    public VerifyFeedbackConfirmationPage(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of FeedbackConfirmationPageAndroid class.
     *
     * @return Instance of page object which models FeedbackConfirmationPageAndroid
     */
    public FeedbackConfirmationPageAndroid feedbackConfirmation() {
        return PageFactory.initElements(androidDriver, FeedbackConfirmationPageAndroid.class);
    }

    /**
     * Verify that Sending Feedback alert is displayed
     */
    public void displaysSendingFeedbackAlert() {
        Verify.checkPoint(feedbackConfirmation().displaysSendingFeedbackAlert(), "Sending Feedback alert is displayed");
    }

    /**
     * Verify that Progress bar with Please wait text is displayed
     */
    public void displaysProgressBarWithPleaseWaitText() {
        Verify.checkPoint(feedbackConfirmation().displaysProgressBarWithPleaseWaitText(),
                "Progress bar with Please wait text is displayed on clicking Send button");
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message displayed
     */
    public void displaysMessage(String message) {
        Verify.checkPoint(feedbackConfirmation().displaysMessage(message), "Meesage '" + message
                + "' is displayed on Feedback confirmation page");
    }

    /**
     * Verify that email sent with feedback is displayed
     *
     * @param feedbackEmail email
     */
    public void displaysSentFeedbackEmail(String feedbackEmail) {
        Verify.checkPoint(feedbackConfirmation().displaysFeedbackEmail(feedbackEmail), "Feedback email '" + feedbackEmail
                + "' is displayd on confirmation page");
    }

    /**
     * Verify that sent feedback text is displayed
     *
     * @param feedbackText Feedback text
     */
    public void displaysSentFeedbackText(String feedbackText) {
        Verify.checkPoint(feedbackConfirmation().displaysSentFeedbackText(feedbackText), "Feedback text '" + feedbackText
                + "' is displayd on confirmation page");
    }
}
