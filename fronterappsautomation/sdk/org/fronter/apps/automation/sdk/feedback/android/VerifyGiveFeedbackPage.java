package org.fronter.apps.automation.sdk.feedback.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.feedback.pages.android.GiveFeedbackPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields on Give Feedback page
 */
public class VerifyGiveFeedbackPage {
    AndroidDriver androidDriver;

    public VerifyGiveFeedbackPage(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of GiveFeedbackPageAndroid class.
     *
     * @return Instance of page object which models GiveFeedbackPageAndroid
     */
    public GiveFeedbackPageAndroid giveFeedback() {
        return PageFactory.initElements(androidDriver, GiveFeedbackPageAndroid.class);
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message to be displayed
     */
    public void displaysMessage(String message) {
        Verify.checkPoint(giveFeedback().displaysMessage(message), "Meesage '" + message + "' is displayed on Give Feedback page");
    }

    /**
     * Verify title is displayed as Give Feedback
     */
    public void hasTitleAsGiveFeeback() {
        Verify.checkPoint(giveFeedback().hasTitleAsGiveFeeback(), "Give Feedback title is displayed at top");
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message to be displayed
     */
    public void displaysPlaceHolderInEmail(String placeholder) {
        Verify.checkPoint(giveFeedback().displaysPlaceHolderInEmail(placeholder), "Placeholder '" + placeholder
                + "' is displayed in email field");
    }

    /**
     * Verify Send button is displayed
     */
    public void displaysSendButton() {
        Verify.checkPoint(giveFeedback().displaysSendButton(), "Send button is displayed on give feedback page");
    }
}
