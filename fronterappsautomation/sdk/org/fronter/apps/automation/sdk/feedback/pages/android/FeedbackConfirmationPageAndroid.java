package org.fronter.apps.automation.sdk.feedback.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;

/**
 * Feedback Confirmation Page Class
 */
public class FeedbackConfirmationPageAndroid {
    private AndroidDriver androidDriver;

    public FeedbackConfirmationPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    String feedbackInputId = "com.fronter.android:id/feedback_text_input";
    String emailInputId = "com.fronter.android:id/feedback_email_input";

    /**
     * Verify that Sending Feedback alert is displayed
     *
     * @return true if Sending Feedback alert is displayed
     */
    public boolean displaysSendingFeedbackAlert() {
        try {
            String alreatText = androidDriver.findElementById("android:id/alertTitle").getText();
            return alreatText.equalsIgnoreCase("Sending feedback");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that Progress bar with Please wait text is displayed
     *
     * @return true if Progress bar with Please wait text is displayed
     */
    public boolean displaysProgressBarWithPleaseWaitText() {
        try {
            boolean isProgessBarDisplayed = androidDriver.findElementById("android:id/progress").isDisplayed();
            String message = androidDriver.findElementById("android:id/message").getText();
            return message.equalsIgnoreCase("Please wait") && isProgessBarDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that sent feedback text is displayed
     *
     * @param feedbackText Feedback text
     * @return true if sent feedback text is displayed
     */
    public boolean displaysSentFeedbackText(String feedbackText) {
        try {
            String feedbackRecieved = androidDriver.findElementById("com.fronter.android:id/feedback_text_sent").getText();
            return feedbackRecieved.equalsIgnoreCase(feedbackText);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that email sent with feedback is displayed
     *
     * @param feedbackEmail email
     * @return true if email sent with feedback is displayed
     */
    public boolean displaysFeedbackEmail(String feedbackEmail) {
        try {
            String emailRecieved = androidDriver.findElementById("com.fronter.android:id/feedback_email_sent").getText();
            return emailRecieved.equals("Email: " + feedbackEmail);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message displayed
     * @return true if given message is displayed
     */
    public boolean displaysMessage(String message) {
        try {
            return androidDriver.findElementByName(message).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on Send more feedback link
     */
    public void clickSendMoreFeedbackLink() {
        androidDriver.findElementById("com.fronter.android:id/sendMoreFeedback").click();
    }

}
