package org.fronter.apps.automation.sdk.feedback.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.feedback.pages.android.FeedbackConfirmationPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Feedback confirmation Tool Page Class
 */
public class FeedbackConfirmationTool {

    private AndroidDriver androidDriver;

    public FeedbackConfirmationTool(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of FeedbackConfirmationPageAndroid class.
     *
     * @return Instance of page object which models FeedbackConfirmationPageAndroid
     */
    public FeedbackConfirmationPageAndroid feedbackConfirmation() {
        return PageFactory.initElements(androidDriver, FeedbackConfirmationPageAndroid.class);
    }

    /**
     * Tap on Send more feedback link button
     */
    public void clickSendMoreFeedbackLink() {
        feedbackConfirmation().clickSendMoreFeedbackLink();
    }
}
