package org.fronter.apps.automation.sdk.feedback.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Give Feedback Page Class
 */
public class GiveFeedbackPageAndroid {
    private AndroidDriver androidDriver;

    public GiveFeedbackPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    String feedbackInputId = "com.fronter.android:id/feedback_text_input";
    String emailInputId = "com.fronter.android:id/feedback_email_input";

    /**
     * Verify title is displayed as Give Feedback
     *
     * @return true if title is displayed as Give Feedback
     */
    public boolean hasTitleAsGiveFeeback() {
        try {
            String titleText = androidDriver.findElementById("com.fronter.android:id/toolbar_title").getText();
            return titleText.equalsIgnoreCase("Give Feedback");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message displayed
     * @return true if given message is displayed
     */
    public boolean displaysMessage(String message) {
        try {
            return androidDriver.findElementByName(message).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify given message is displayed
     *
     * @param message Message displayed
     * @return true if given message is displayed
     */
    public boolean displaysPlaceHolderInEmail(String placeholder) {
        try {
            String textDisplayed = androidDriver.findElementById(emailInputId).getText();
            return textDisplayed.contains(placeholder);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Send button is displayed
     *
     * @return true if Send button is displayed
     */
    public boolean displaysSendButton() {
        try {
            return androidDriver.findElementById("com.fronter.android:id/sendFeedbackButton").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Input feedback text and email
     *
     * @param feedbackText Feedback text and email
     * @param email Email of feedback provider
     */
    public void inputFeedback(String feedbackText, String email) {
        if (feedbackText != null) {
            WebElement feedbackInput = androidDriver.findElementById(feedbackInputId);
            feedbackInput.sendKeys(feedbackText);
        }
        if (email != null) {
            WebElement emailInput = androidDriver.findElementById(emailInputId);
            emailInput.sendKeys(email);
        }
        androidDriver.hideKeyboard();
    }


    /**
     * Tap on Send button
     */
    public void clickSendButton() {
        androidDriver.findElementById("com.fronter.android:id/sendFeedbackButton").click();
    }

}
