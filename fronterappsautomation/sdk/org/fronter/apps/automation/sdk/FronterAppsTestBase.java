package org.fronter.apps.automation.sdk;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.feedback.android.FeedbackConfirmationTool;
import org.fronter.apps.automation.sdk.feedback.android.GiveFeedbackTool;
import org.fronter.apps.automation.sdk.feedback.android.VerifyFeedbackConfirmationPage;
import org.fronter.apps.automation.sdk.feedback.android.VerifyGiveFeedbackPage;
import org.fronter.apps.automation.sdk.notification.android.AccountToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.HeaderToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.LoginToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.NewsListToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.NewsViewToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.NotificationListToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.NotificationViewToolAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyAccountPageAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyLoginPageAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyNewsListPageAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyNewsViewPageAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyNotificationListPageAndroid;
import org.fronter.apps.automation.sdk.notification.android.VerifyNotificationViewPageAndroid;
import org.fronter.apps.automation.sdk.notification.ios.AccountToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.LoginToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NewsListToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NewsViewToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyAccountPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyLoginPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNewsListPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNewsViewPageIOS;
import org.fronter.automation.sdk.Configuration;
import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Base class responsible for doing all initialization. All test class should
 * extend this class.
 */
public class FronterAppsTestBase {
    protected WebDriver driver;
    protected AndroidDriver androidDriver;
    protected IOSDriver iOSDriver;
    public Configuration config;

    protected LoginToolAndroid loginToolAndroid;
    protected NewsListToolAndroid newsListToolAndroid;
    protected AccountToolAndroid accountToolAndroid;
    protected VerifyAccountPageAndroid verifyAccountPageAndroid;
    protected VerifyNewsListPageAndroid verifyNewsListPageAndroid;
    protected VerifyNotificationListPageAndroid verifyNotificationListPageAndroid;
    protected NewsViewToolAndroid newsViewToolAndroid;
    protected VerifyNewsViewPageAndroid verifyNewsViewPageAndroid;
    protected NotificationListToolAndroid notificationListToolAndroid;
    protected NotificationViewToolAndroid notificationViewToolAndroid;
    protected VerifyNotificationViewPageAndroid verifyNotificationViewPageAndroid;
    protected VerifyLoginPageAndroid verifyLoginPageAndroid;
    protected HeaderToolAndroid headerToolAndroid;
    protected GiveFeedbackTool giveFeedbackTool;
    protected FeedbackConfirmationTool feedbackConfirmationTool;
    protected VerifyGiveFeedbackPage verifyGiveFeedbackPage;
    protected VerifyFeedbackConfirmationPage verifyFeedbackConfirmationPage;

    protected LoginToolIOS loginToolIOS;
    protected NewsListToolIOS newsListToolIOS;
    protected AccountToolIOS accountToolIOS;
    protected VerifyAccountPageIOS verifyAccountPageIOS;
    protected VerifyNewsListPageIOS verifyNewsListPageIOS;
    protected VerifyNewsViewPageIOS verifyNewsViewPageIOS;
    protected NewsViewToolIOS newsViewToolIOS;
    protected VerifyLoginPageIOS verifyLoginPageIOS;

    protected TestData testData = new TestData();
    protected ReadAndWriteTestData readWriteFile = new ReadAndWriteTestData();

    @BeforeClass
    public void instantiateDriver() {
        if (initializeAndroidDriver()) {
            androidDriver = WebDriverFactory.getInstance().getAndroidDriver();
        } else if (initializeIOSDriver()) {
            iOSDriver = WebDriverFactory.getInstance().getIOSDriver();
        } else {
            driver = WebDriverFactory.getInstance().getDriver();
        }

        loginToolAndroid = new LoginToolAndroid(androidDriver);
        newsListToolAndroid = new NewsListToolAndroid(androidDriver);
        accountToolAndroid = new AccountToolAndroid(androidDriver);
        verifyAccountPageAndroid = new VerifyAccountPageAndroid(androidDriver);
        verifyNewsListPageAndroid = new VerifyNewsListPageAndroid(androidDriver);
        verifyNotificationListPageAndroid = new VerifyNotificationListPageAndroid(androidDriver);
        newsViewToolAndroid = new NewsViewToolAndroid(androidDriver);
        verifyNewsViewPageAndroid = new VerifyNewsViewPageAndroid(androidDriver);
        notificationListToolAndroid = new NotificationListToolAndroid(androidDriver);
        verifyNotificationViewPageAndroid = new VerifyNotificationViewPageAndroid(androidDriver);
        notificationViewToolAndroid = new NotificationViewToolAndroid(androidDriver);
        verifyLoginPageAndroid = new VerifyLoginPageAndroid(androidDriver);
        headerToolAndroid = new HeaderToolAndroid(androidDriver);
        giveFeedbackTool = new GiveFeedbackTool(androidDriver);
        feedbackConfirmationTool = new FeedbackConfirmationTool(androidDriver);
        verifyGiveFeedbackPage = new VerifyGiveFeedbackPage(androidDriver);
        verifyFeedbackConfirmationPage = new VerifyFeedbackConfirmationPage(androidDriver);

        loginToolIOS = new LoginToolIOS(iOSDriver);
        newsListToolIOS = new NewsListToolIOS(iOSDriver);
        accountToolIOS = new AccountToolIOS(iOSDriver);
        verifyAccountPageIOS = new VerifyAccountPageIOS(iOSDriver);
        verifyNewsListPageIOS = new VerifyNewsListPageIOS(iOSDriver);
        verifyNewsViewPageIOS = new VerifyNewsViewPageIOS(iOSDriver);
        newsViewToolIOS = new NewsViewToolIOS(iOSDriver);
        verifyLoginPageIOS = new VerifyLoginPageIOS(iOSDriver);
    }

    @AfterClass(alwaysRun = true)
    public void closeBrowser() {
        if (initializeAndroidDriver()) {
            androidDriver.quit();
        } else if (initializeIOSDriver()) {
            iOSDriver.quit();
        } else {
            driver.quit();
        }
    }

    /**
     * Checks if android driver needs to be initialized
     *
     * @return True if android driver needs to be initialized
     */
    public boolean initializeAndroidDriver() {
        config = Configuration.getInstance();
        boolean invokeAndroidDriver = config.get("invoke.android.driver").equalsIgnoreCase("yes");
        return invokeAndroidDriver;
    }

    /**
     * Checks if iOS driver needs to be initialized
     *
     * @return True if iOS driver needs to be initialized
     */
    public boolean initializeIOSDriver() {
        config = Configuration.getInstance();
        boolean invokeIOSDriver = config.get("invoke.ios.driver").equalsIgnoreCase("yes");
        return invokeIOSDriver;
    }
}
