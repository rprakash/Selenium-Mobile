package org.fronter.apps.automation.sdk.kolibri.register.pages;

import java.util.List;

import org.fronter.automation.sdk.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class KolibriChannelListPage {
    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]")
    @CacheLookup
    private WebElement channelName;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]")
    private WebElement newChannelName;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]")
    @CacheLookup
    private WebElement addIcon;

    @FindBy(name = "Create new channel")
    @CacheLookup
    private WebElement newChannel;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATextField[1]")
    @CacheLookup
    private WebElement newChannelField;

    @FindBy(name = "Create")
    @CacheLookup
    private WebElement create;

    @FindBy(name = "Join existing channel")
    @CacheLookup
    private WebElement joinChannel;

    @FindBy(name = "//UIAApplication[1]/UIAWindow[1]/UIATextField[1]/UIATextField[1]")
    @CacheLookup
    private WebElement code1;

    @FindBy(name = "//UIAApplication[1]/UIAWindow[1]/UIATextField[2]/UIATextField[1]")
    @CacheLookup
    private WebElement code2;

    @FindBy(name = "//UIAApplication[1]/UIAWindow[1]/UIATextField[3]/UIATextField[1]")
    @CacheLookup
    private WebElement code3;

    @FindBy(name = "Join")
    @CacheLookup
    private WebElement joinButton;

    @FindBy(name = "Back")
    @CacheLookup
    private WebElement backButton;

    @FindBy(name = "Account")
    @CacheLookup
    private WebElement accountIcon;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIATextField[1]")
    @CacheLookup
    private WebElement emailIdTextBox;

    @FindBy(name = "Done")
    @CacheLookup
    private WebElement buttonDone;

    @FindBy(name = "Channels")
    @CacheLookup
    private WebElement channelsIcon;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]")
    @CacheLookup
    private WebElement userNameTextBox;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[2]")
    @CacheLookup
    private WebElement userScreenName;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[3]")
    @CacheLookup
    private WebElement message;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAStatusBar[1]/UIAElement[3]")
    @CacheLookup
    private WebElement currentTime;

    @FindBy(xpath = " //UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[4]")
    @CacheLookup
    private WebElement timeStamp;

    private WebDriver driver;
    private Wait wait;

    public KolibriChannelListPage(WebDriver driver) {
        this.driver = driver;
        wait = new Wait(driver);
    }

    /**
     * Verify Channel name exists on channel list page
     *
     * @param channel Name of the cHannel on channel list page
     * @return true if channel name present else false
     */
    public boolean hasChannelName(String channel) {
        try {
            return channelName.getText().equalsIgnoreCase(channel + "'s channel");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Channel name exists on channel list page
     *
     * @param channel Name of the cHannel on channel list page
     * @return true if channel name present else false
     */
    public boolean hasNewChannelName(String editChannel) {
        try {
            return newChannelName.getText().equalsIgnoreCase(editChannel);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Swipe on the channel on Channel List Page
     */
    public void swipeOnChannel() {
        wait.untilElementIsVisible(channelName);
        channelName.click();
    }

    /**
     * Swipe on the 'new channel' on Channel List Page
     */
    public void swipeOnNewChannel() {
        newChannelName.click();
    }

    /**
     * Tap on '+' Add button on Channel List Page
     */
    public void tapOnAddIcon() {
        addIcon.click();
    }

    /**
     * Creating a new channel
     *
     * @param channel Channel Name
     */
    public void createNewChannel(String channel) {
        newChannel.click();
        newChannelField.sendKeys(channel);
        create.click();
    }

    /**
     * Verify Edited Channel name exists on channel list page
     *
     * @param editChannel Name of the edited channel on channel list page
     * @return true if edit channel name present else false
     */
    public boolean hasEditedChannelName(String editChannel) {
        try {
            return newChannelName.getText().contains(editChannel);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Join Channel Which is already Exists
     */
    public void joinChannel() {
        String channelId = driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]")).getText();
        String[] id = channelId.split("#");
        String[] codeId = id[1].split("-");
        wait.untilElementIsVisible(backButton);
        backButton.click();
        addIcon.click();
        joinChannel.click();
        wait.untilElementIsVisible(joinButton);

        List<WebElement> EditText = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[1]"));
        EditText.get(0).sendKeys(codeId[0].trim());

        List<WebElement> EditText1 = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[2]"));
        EditText1.get(0).sendKeys(codeId[1].trim());

        List<WebElement> EditText2 = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[3]"));
        EditText2.get(0).sendKeys(codeId[2].trim());
        joinButton.click();
    }

    /**
     * Tap on Accounts on Channel List Page
     */
    public void tapOnAccountSettings() {
        accountIcon.click();
    }

    /**
     * Tap Channels Icon on Channel list page
     */
    public void tapOnChannelsIcon() {
        channelsIcon.click();
    }

    /**
     * Verify UserName is edited on Channel List Page
     *
     * @param editUserName Edited User Screen name
     * @return true if edited UserScreenName with Message present else false
     */
    public boolean hasEditedUserScreenName(String editUserName) {
        try {
            return userScreenName.getText().equalsIgnoreCase(editUserName + ":");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Last Message send on channel page exists on Channel list page
     *
     * @param channelName User Screen Name
     * @param messageOnChannel message on channel list page
     * @return true if message present else false
     */
    public boolean hasMessage(String messageOnChannel) {
        try {
            return message.getText().equalsIgnoreCase(messageOnChannel);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify First line of MultiLine Message exists on channel list page
     *
     * @param messageOnChannel message
     * @return true if message present else false
     */
    public boolean hasFirstLineMessage(String messageOnChannel) {
        try {
            String fullMessage = message.getText();
            String[] message = fullMessage.split(" ");
            return message[0].equalsIgnoreCase(messageOnChannel);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Channel List Page has a message indicates image
     *
     * @return true if Image present else false
     */
    public boolean hasImage() {
        try {
            return message.getText().equalsIgnoreCase("(image)");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Time Stamp or Time age with userName on Channel list page
     *
     * @param time Current time
     * @return true if Time Stamp present else false
     */
    public boolean hasTimeStamp(String time) {
        try {
            return timeStamp.getText().equalsIgnoreCase(time);
        } catch (Exception e) {
            return false;
        }
    }
}
