package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriRegisterPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class KolibriRegisterTool {

    WebDriver driver;

    public KolibriRegisterTool(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Tap on Get Started button
     */
    public void tapGetStarted() {
        kolibriRegisterPage().tapGetStarted();
    }

    /**
     * Enter User E-mail id and Screen Name of the User
     *
     * @param emailId User E-mail Id
     * @param screenName User Screen Name
     */
    public void enterUserDetails(String emailId, String screenName) {
        kolibriRegisterPage().enterUserDetails(emailId, screenName);
    }

    /**
     * Returns an instance of KolibriRegisterPage class.
     *
     * @return Instance of page object which models KolibriRegisterPage
     */
    public KolibriRegisterPage kolibriRegisterPage() {
        return PageFactory.initElements(driver, KolibriRegisterPage.class);
    }
}
