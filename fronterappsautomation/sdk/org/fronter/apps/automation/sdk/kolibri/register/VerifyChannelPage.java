package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriChannelPage;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class VerifyChannelPage {
    WebDriver driver;

    public VerifyChannelPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Verify New Channel exists on channel page
     *
     * @param channelName New Channel Name
     */
    public void hasNewChannelName(String channelName) {
        Verify.checkPoint(
                kolibriChannelPage().hasNewChannelName(channelName),
                "New Channel Name Exists on channel page"
            );
    }

    /**
     * Verify Channel exists on channel page
     *
     * @param channelName Channel name
     */
    public void hasChannelName(String channelName) {
        Verify.checkPoint(
                kolibriChannelPage().hasChannelName(channelName),
                "Channel Name Exists on channel page"
            );
    }

    /**
     * User send message exists on channel page
     *
     * @param message Message on channel page
     */
    public void hasMessage(String message) {
        Verify.checkPoint(
                kolibriChannelPage().hasMessage(message),
                "Message Exists on channel page"
            );
    }

    /**
     * Returns an instance of KolibriChannelPage class.
     *
     * @return Instance of page object which models KolibriChannelPage
     */
    public KolibriChannelPage kolibriChannelPage() {
        return PageFactory.initElements(driver, KolibriChannelPage.class);
    }

    /**
     * Verify Edited Channel name exists on channel page
     *
     * @param editChannel Edited Channel Page
     */
    public void hasEditedChannelName(String editChannel) {
        Verify.checkPoint(
                kolibriChannelPage().hasNewChannelName(editChannel),
                "Channel Name Exists on channel page"
            );
    }

    /**
     * Verify Edited User Screen Name exists on the channel page
     *
     * @param presentDate present day Date
     * @param editUserName Edited UserName
     * @param time current time
     */
    public void hasEditedUserName(String presentDate, String time, String editUserName) {
        Verify.checkPoint(
                kolibriChannelPage().hasTimeStampWithUserName(presentDate, time, editUserName),
                "Edited User Screen Name Exists on channel page"
            );
    }

    /**
     * Verify Joined channel exists on Channel page
     *
     * @param newChannel Channel name
     */
    public void hasJoinedChannelName(String newChannel) {
        Verify.checkPoint(
                kolibriChannelPage().hasNewChannelName(newChannel),
                "Joined Channel Name Exists on channel page"
            );
    }

    /**
     * Verify Mute channel function enabled on channel page
     */
    public void hasMuteChannelEnabled() {
        Verify.checkPoint(
                kolibriChannelPage().hasMuteChannelEnabled(),
                "Mute Channel Enabled and Mute Channel icon Exists on channel page"
            );
    }

    /**
     * Verify favorite channel function enabled on channel page
     */
    public void hasFavoriteChannelEnabled() {
        Verify.checkPoint(
                kolibriChannelPage().hasFavoriteChannelEnabled(),
                "Favorite Channel Enabled and Favorite Channel icon Exists on channel page"
            );
    }

    /**
     * Verify All Messages exists on channel page
     *
     * @param message message on channel page
     */
    public void hasMultipleMessages(String[] message) {
        Verify.checkPoint(
                kolibriChannelPage().hasMultipleMessages(message),
                "Messages Exists on channel page"
            );
    }

    /**
     * Verify Time Stamp or Time age with userName on channel page
     *
     * @param presentDate present day Date
     * @param channelName User Screen Name
     */
    public void hasTimeStampWithUserName(String presentDate, String time, String userName) {
        Verify.checkPoint(
                kolibriChannelPage().hasTimeStampWithUserName(presentDate, time, userName),
                "Time stamp with User screen name Exists on channel page"
            );
    }
}
