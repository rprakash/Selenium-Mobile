package org.fronter.apps.automation.sdk.kolibri.register.pages;

import java.util.HashMap;
import java.util.List;

import org.fronter.automation.sdk.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class KolibriChannelPage {

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]")
    private WebElement channelName;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]")
    @CacheLookup
    private WebElement messageTextBox;

    @FindBy(name = "Send")
    @CacheLookup
    private WebElement sendIcon;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextView[1]")
    @CacheLookup
    private WebElement messageOnChannelPage;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIATextView[1]")
    @CacheLookup
    private WebElement messageTwo;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIATextView[1]")
    @CacheLookup
    private WebElement messageThree;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]/UIATextView[1]")
    @CacheLookup
    private WebElement messageFourth;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[5]/UIATextView[1]")
    @CacheLookup
    private WebElement messageFifth;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]")
    private WebElement backButton;

    @FindBy(name = "gear filled")
    @CacheLookup
    private WebElement settingIcon;

    @FindBy(name = "Channel settings")
    @CacheLookup
    private WebElement channelSetting;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[12]")
    @CacheLookup
    private WebElement buttonS;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[8]")
    @CacheLookup
    private WebElement buttonI;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[5]")
    @CacheLookup
    private WebElement buttonT;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAKeyboard[1]/UIAKey[11]")
    @CacheLookup
    private WebElement buttonA;

    @FindBy(name = "Done")
    @CacheLookup
    private WebElement buttonDone;

    @FindBy(name = "Leave channel")
    @CacheLookup
    private WebElement closeChannel;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]")
    @CacheLookup
    private WebElement channelCode;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]")
    private WebElement userScreenName;

    @FindBy(name = "Mute channel")
    @CacheLookup
    private WebElement muteChannelButton;

    @FindBy(name = "Add as favourite")
    @CacheLookup
    private WebElement favoriteChannelButton;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAImage[2]")
    @CacheLookup
    private WebElement muteChannelIcon;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIAImage[3]")
    @CacheLookup
    private WebElement favoriteChannelIcon;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[3]/UIAStatusBar[1]/UIAElement[3]")
    @CacheLookup
    private WebElement currentTime;

    @FindBy(name = "Return")
    private WebElement returnKey;

    @FindBy(name = "camera")
    @CacheLookup
    private WebElement camera;

    @FindBy(name = "Pictures gallery")
    @CacheLookup
    private WebElement gallery;

    @FindBy(name = "Moments")
    @CacheLookup
    private WebElement moments;

    private WebDriver driver;
    private Wait wait;

    public KolibriChannelPage(WebDriver driver) {
        this.driver = driver;
        wait = new Wait(driver);
    }

    /**
     * Verify Channel name exists on channel page
     *
     * @param channel ChannelName
     * @return true if channel name present else false
     */
    public boolean hasChannelName(String channel) {
        try {
            return channelName.getText().equalsIgnoreCase(channel + "'s channel");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify New Channel name exists on channel page
     *
     * @param channel Channel Name
     * @return true if New Channel Name present else false
     */
    public boolean hasNewChannelName(String channel) {
        wait.untilElementIsVisible(channelName);
        try {
            return channelName.getText().equalsIgnoreCase(channel);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Message exists on channel page
     *
     * @param message Message
     * @return true if message present else false
     */
    public boolean hasMessage(String message) {
        try {
            return messageOnChannelPage.getText().equalsIgnoreCase(message);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Send Message on the Channel Page
     *
     * @param message Message
     */
    public void sendMessage(String message) {
        messageTextBox.sendKeys(message);
        sendIcon.click();
    }

    /**
     * Tap on Back button on Channel page
     */
    public void tapOnBackButton() {
        wait.untilElementIsVisible(backButton);
        backButton.click();
    }

    /**
     * Tap on settings Icon on channel page
     */
    public void tapOnSettings() {
        settingIcon.click();
    }

    /**
     * Edit Channel name which is created by user
     *
     * @param editChannel Edited Channel Name
     */
    public void editChannelName(String editChannel) {
        channelSetting.click();
        JavascriptExecutor js = (JavascriptExecutor) driver;

        // Precise tap executed on channel text box field
        js.executeScript("mobile: tap", new HashMap<String, Double>() {
            private static final long serialVersionUID = 1L;
            {
                put("tapCount", 1.0);
                put("touchCount", 1.0);
                put("duration", 0.8908789);
                put("x", 145.0);
                put("y", 126.0);
            }
        });

        driver.findElement(By.name("Clear text")).click();
        List<WebElement> EditText =
                driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]"));
        EditText.get(0).sendKeys(editChannel);
        buttonDone.click();
    }

    /**
     * Delete Edited Channel which is edited by user
     */
    public void deleteChannel() {
        closeChannel.click();
        driver.switchTo().alert().accept();
    }

    /**
     * Tap on Mute Channel button to enable
     */
    public void tapOnMuteChannel() {
        muteChannelButton.click();
    }

    /**
     * Tap on Favorite Channel button to enable
     */
    public void tapOnFavoriteChannel() {
        favoriteChannelButton.click();
    }

    /**
     * Verify Mute channel function enabled on channel page
     *
     * @return true if mute channel icon present else false
     */
    public boolean hasMuteChannelEnabled() {
        try {
            return muteChannelIcon.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Favorite channel function enabled on channel page
     *
     * @return true if favorite channel icon present else false
     */
    public boolean hasFavoriteChannelEnabled() {
        try {
            return favoriteChannelIcon.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Send Multiple Messages on the channel page
     *
     * @param message message on channel
     */
    public void sendMultipleMessage(String[] message) {
        for (int i = 0; i < message.length; i++) {
            messageTextBox.sendKeys(message[i]);
            sendIcon.click();
        }
    }

    /**
     * Verify All messages exists on channel page
     *
     * @param message message on the channel page
     * @return true if message present else false
     */
    public boolean hasMultipleMessages(String[] message) {
        try {
            for (int i = 1; i <= message.length; i++) {
                boolean correctMessage =
                        driver.findElement(
                                By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[" + i
                                        + "]/UIATextView[1]")).getText().equalsIgnoreCase(message[i - 1]);
                if (!correctMessage) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Scroll to First message or top message
     */
    public void scrollToTopMessage() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        HashMap<String, String> scrollObject = new HashMap<String, String>();
        scrollObject.put("direction", "up");
        js.executeScript("mobile: scroll", scrollObject);
    }

    /**
     * Verify Time Stamp or Time age on channel page
     *
     * @param timeAge Age of the time
     * @param channelName User Screen Name
     *
     * @return boolean
     */
    public boolean hasTimeStamp(String timeAge, String channelName) {
        try {
            return userScreenName.getText().equalsIgnoreCase(timeAge + " " + channelName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Send Multiple Line Message using Return key
     *
     * @param message Message on Channel Page
     */
    public void sendMultipleLineMessage(String[] message) {
        for (int i = 0; i < message.length; i++) {
            messageTextBox.sendKeys(message[i]);
            returnKey.click();
        }
        sendIcon.click();
    }

    /**
     * Verify Time Stamp or Time age with UserName on channel page
     *
     * @param presentDate Date with UserName
     * @param channelName User Screen Name
     * @param time current time
     *
     * @return boolean
     */
    public boolean hasTimeStampWithUserName(String presentDate, String time, String channelName) {
        try {
            return userScreenName.getText().equalsIgnoreCase(presentDate + ", " + time + " by " + channelName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Fetch current time on the Simulator
     *
     * @return Instance time on the Simulator
     */
    public String getCurrentTimeOnSimulator() {
        return currentTime.getText();
    }

    /**
     * Send Image via Gallery on Channel Page
     */
    public void sendImage() {
        camera.click();
        gallery.click();
        driver.switchTo().alert().accept();
        moments.click();
        driver.findElement(By.xpath(
                "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]"))
                .click();
    }
}
