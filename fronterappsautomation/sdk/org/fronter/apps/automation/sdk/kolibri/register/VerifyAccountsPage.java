package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriAcountsPage;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class VerifyAccountsPage {
    WebDriver driver;

    public VerifyAccountsPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Verify E-mail Id is edited on Accounts Settings Page
     *
     * @param completeEditedEmailId Edited E-mail Id
     */
    public void hasEditedEmailId(String completeEditedEmailId) {
        Verify.checkPoint(
            kolibriAcountsPage().hasEditedEmailId(completeEditedEmailId),
            "Edited E-mail id Exists on Accont Settings Page"
        );
    }

    /**
     * Verify UserName is edited on Accounts Settings Page
     *
     * @param editUserName UserName or Screen name
     */
    public void hasEditedUserScreenName(String editUserName) {
        Verify.checkPoint(
            kolibriAcountsPage().hasEditedUserScreenName(editUserName),
            "Edited User Screen Name Exists on Account Settings Page"
        );
    }

    /**
     * Returns an instance of KolibriAccountsPage class.
     *
     * @return Instance of page object which models KolibriAcountsPage
     */
    public KolibriAcountsPage kolibriAcountsPage() {
        return PageFactory.initElements(driver, KolibriAcountsPage.class);
    }

    /**
     * Verify Feedback alert message appears
     */
    public void hasFeedbackAlert() {
        Verify.checkPoint(
            kolibriAcountsPage().hasFeedbackAlert(),
            "Feedback Alert Message Exists"
        );
    }
}
