package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriChannelPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class KolibriChannelTool {

    private WebDriver driver;

    public KolibriChannelTool(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Send Message on the Channel Page
     *
     * @param message Message on Channel Page
     */
    public void sendMessage(String message) {
        kolibriChannelPage().sendMessage(message);
    }

    /**
     * Tap on 'Back' button on Channel page
     */
    public void tapOnBackButton() {
        kolibriChannelPage().tapOnBackButton();
    }

    /**
     * Returns an instance of KolibriChannelPage class.
     *
     * @return Instance of page object which models KolibriChannelPage
     */
    public KolibriChannelPage kolibriChannelPage() {
        return PageFactory.initElements(driver, KolibriChannelPage.class);
    }

    /**
     * Tap on settings Icon on channel page
     */
    public void tapOnSettings() {
        kolibriChannelPage().tapOnSettings();
    }

    /**
     * Edit Channel name which is created by user
     *
     * @param editChannel Edited Channel Name
     */
    public void editChannelName(String editChannel) {
        kolibriChannelPage().editChannelName(editChannel);
        kolibriChannelPage().tapOnBackButton();
        kolibriChannelPage().tapOnBackButton();
    }

    /**
     * Delete Edited Channel which is edited by user
     */
    public void deleteChannel() {
        kolibriChannelPage().deleteChannel();
    }

    /**
     * Tap on Mute channel button
     */
    public void tapOnMuteChannel() {
        kolibriChannelPage().tapOnMuteChannel();
    }

    /**
     * Tap on Favorite channel button
     */
    public void tapOnFavoriteChannel() {
        kolibriChannelPage().tapOnFavoriteChannel();
    }

    /**
     * Send Multiple Messages on the Channel Page
     *
     * @param message Message on channel page
     */
    public void sendMultipleMessage(String[] message) {
        kolibriChannelPage().sendMultipleMessage(message);
    }

    /**
     * Scroll to First message or top message
     */
    public void scrollToTopMessage() {
        kolibriChannelPage().scrollToTopMessage();
    }

    /**
     * Send Multiple Line Message using Return key
     *
     * @param message Message on Channel Page
     */
    public void sendMultipleLineMessage(String[] message) {
        kolibriChannelPage().sendMultipleLineMessage(message);
    }

    /**
     * Send Image via Gallery on Channel Page
     */
    public void sendImage() {
        kolibriChannelPage().sendImage();
    }

    /**
     * Fetch current time on the Simulator
     *
     * @return Instance time on the simulator
     */
    public String getCurrentTimeOnSimulator() {
        return kolibriChannelPage().getCurrentTimeOnSimulator();
    }
}
