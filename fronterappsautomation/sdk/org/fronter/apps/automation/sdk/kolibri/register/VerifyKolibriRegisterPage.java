package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriRegisterPage;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class VerifyKolibriRegisterPage {

    WebDriver driver;

    public VerifyKolibriRegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Clicks on Get Started button
     */
    public void hasWelcomeMessage(String welcomeMessage) {
        Verify.checkPoint(
                kolibriRegisterPage().hasWelcomeMessage(welcomeMessage),
                "Welcome message displayed"
            );
    }

    /**
     * Returns an instance of KolibriRegisterPage class.
     *
     * @return Instance of page object which models KolibriRegisterPage
     */
    public KolibriRegisterPage kolibriRegisterPage() {
        return PageFactory.initElements(driver, KolibriRegisterPage.class);
    }
}
