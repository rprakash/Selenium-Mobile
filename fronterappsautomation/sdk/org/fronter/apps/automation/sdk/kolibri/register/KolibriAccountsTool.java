package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriAcountsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class KolibriAccountsTool {

    private WebDriver driver;

    public KolibriAccountsTool(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Returns an instance of KolibriAccountsPage class.
     *
     * @return Instance of page object which models KolibriAcountsPage
     */
    public KolibriAcountsPage kolibriAcountsPage() {
        return PageFactory.initElements(driver, KolibriAcountsPage.class);
    }

    /**
     * Change Email-id on Accounts Page
     *
     * @param editEmail edit E-mail id
     */
    public void changeEmailId(String editEmail) {
        kolibriAcountsPage().changeEmailId(editEmail);
    }

    /**
     * Edit UserScreenName on Accounts Settings Page
     *
     * @param editUserName Edit UserName
     */
    public void changeUserName(String editUserName) {
        kolibriAcountsPage().changeUserName(editUserName);
    }

    /**
     * Send Feedback on Account Settings page
     *
     * @param feedbackMessage Message send as feedback
     */
    public void sendFeedback(String feedbackMessage) {
        kolibriAcountsPage().sendFeedback(feedbackMessage);
    }

    /**
     * Accept alert appears after sending feedback
     */
    public void acceptConfirmationAlert() {
        kolibriAcountsPage().acceptConfirmationAlert();
    }
}
