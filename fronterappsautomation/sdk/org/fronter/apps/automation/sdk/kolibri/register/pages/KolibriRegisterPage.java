package org.fronter.apps.automation.sdk.kolibri.register.pages;

import java.util.List;

import org.fronter.automation.sdk.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class KolibriRegisterPage {
    @FindBy(name = "Get started!")
    @CacheLookup
    private WebElement getStartedButton;

    @FindBy(name = "Welcome to Messenger")
    @CacheLookup
    private WebElement welcomeMessage;

    @FindBy(name = "next step")
    private WebElement nextStep;

    @FindBy(name = "OK!")
    @CacheLookup
    private WebElement ok;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[2]/UIATextField[1]")
    @CacheLookup
    private WebElement emailID;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[2]/UIATextField[1]")
    @CacheLookup
    private WebElement channelScreenName;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]")
    @CacheLookup
    private WebElement channelName;

    private Wait wait;
    private WebDriver driver;

    public KolibriRegisterPage(WebDriver driver) {
        wait = new Wait(driver);
        this.driver = driver;
    }

    /**
     * Tap on Get Started button
     */
    public void tapGetStarted() {
        wait.untilElementIsVisible(getStartedButton);
        getStartedButton.click();
        wait.untilElementIsVisible(nextStep);
    }

    /**
     * Verify Welcome message present on the Welcome page
     *
     * @param message Message
     * @return true if message present else false
     */
    public boolean hasWelcomeMessage(String message) {
        try {
            return welcomeMessage.getText().equalsIgnoreCase(message);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Enter User E-mail id and Screen Name of the User
     *
     * @param emailId User E-mailId
     * @param screenName User Screen name
     */
    public void enterUserDetails(String emailId, String screenName) {
        List<WebElement> EditText = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[1]"));
        EditText.get(0).sendKeys(emailId);
        nextStep.click();
        wait.untilElementIsVisible(nextStep);
        List<WebElement> EditText1 = driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATextField[1]"));
        EditText1.get(0).sendKeys(screenName);
        nextStep.click();
        driver.switchTo().alert().accept();
        wait.untilElementIsVisible(ok);
        ok.click();
        wait.untilElementIsVisible(channelName);
    }
}
