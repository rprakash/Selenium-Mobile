package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriChannelListPage;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class VerifyChannelListPage {
    WebDriver driver;

    public VerifyChannelListPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Verify channel exists on channel list page
     *
     * @param channelName name of the channel
     */
    public void hasChannelName(String channelName) {
        Verify.checkPoint(
                kolibriChannelListPage().hasChannelName(channelName),
                "Channel Name Exists on Channel List Page"
            );
    }

    /**
     * Verify channel exists on channel list page
     *
     * @param channelName new channel name on channel list page
     */
    public void hasNewChannelName(String channelName) {
        Verify.checkPoint(
                kolibriChannelListPage().hasNewChannelName(channelName),
                "New Channel Name Exists on Channel List Page"
            );
    }

    /**
     * Returns an instance of KolibriChannelPage class.
     *
     * @return Instance of page object which models KolibriChannelPage
     */
    public KolibriChannelListPage kolibriChannelListPage() {
        return PageFactory.initElements(driver, KolibriChannelListPage.class);
    }

    /**
     * Verify edited channel exists on channel list page
     *
     * @param editChannel name of the edited channel
     */
    public void hasEditedChannelName(String editChannel) {
        Verify.checkPoint(
                kolibriChannelListPage().hasNewChannelName(editChannel),
                "Edited Channel Name Exists on Channel List Page"
            );
    }

    /**
     * Verify edited channel does not exists on channel list page
     *
     * @param editChannel name of the edited channel
     */
    public void doesNotHaveChannel(String editChannel) {
        Verify.checkPoint(
                !kolibriChannelListPage().hasNewChannelName(editChannel),
                "Edited Channel Does not Exists on Channel List Page"
            );
    }

    /**
     * Verify UserName is edited on Channel List Page
     *
     * @param editUserName User SCreen Name
     */
    public void hasEditedUserScreenName(String editUserName) {
        Verify.checkPoint(
                kolibriChannelListPage().hasEditedUserScreenName(editUserName),
                "Edited User Screen Name Exists on Channel List Page"
            );
    }

    /**
     * Verify Last Message sent on channel exists on channel list page
     *
     * @param messageOnChannel message
     * @param channelName User Screen Name
     */
    public void hasMessage(String messageOnChannel) {
        Verify.checkPoint(
                kolibriChannelListPage().hasMessage(messageOnChannel),
                "Last send Message Exists on Channel List Page"
            );
    }

    /**
     * Verify First line of MultiLine Message exists on channel list page
     *
     * @param messageOnChannel message
     */
    public void hasFirstLineMessage(String messageOnChannel) {
        Verify.checkPoint(
                kolibriChannelListPage().hasFirstLineMessage(messageOnChannel),
                "First Line Message of MultiLine Message Exists on Channel List Page"
            );
    }

    /**
     * Verify Channel List Page has a message indicates image
     */
    public void hasImage() {
        Verify.checkPoint(
                kolibriChannelListPage().hasImage(),
                "Message on Channel List Page Indicates Image send on Channel"
            );
    }

    /**
     * Verify Time Stamp or Time age with userName on Channel list page
     *
     * @param time Current time
     */
    public void hasTimeStamp(String time) {
        Verify.checkPoint(
                kolibriChannelListPage().hasTimeStamp(time),
                "Time stamp Exists on channel list page"
            );
    }
}
