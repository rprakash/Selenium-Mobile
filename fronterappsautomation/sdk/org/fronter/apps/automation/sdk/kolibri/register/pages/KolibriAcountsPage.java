package org.fronter.apps.automation.sdk.kolibri.register.pages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class KolibriAcountsPage {

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIATextField[1]")
    @CacheLookup
    private WebElement emailIdTextBox;

    @FindBy(name = "Done")
    @CacheLookup
    private WebElement buttonDone;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]")
    @CacheLookup
    private WebElement userNameTextBox;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[2]")
    @CacheLookup
    private WebElement userScreenName;

    @FindBy(name = "Send feedback")
    @CacheLookup
    private WebElement sendFeedbackLink;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]")
    @CacheLookup
    private WebElement feedbackTextField;

    @FindBy(name = "Send")
    @CacheLookup
    private WebElement sendButton;

    @FindBy(xpath = "//UIAApplication[1]/UIAWindow[4]/UIAAlert[1]/UIAScrollView[1]/UIAStaticText[1]")
    @CacheLookup
    private WebElement alertMessage;

    private WebDriver driver;

    public KolibriAcountsPage(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Change Email-id on Accounts Settings Page
     *
     * @param editEmail Edit E-mail Id
     */
    public void changeEmailId(String editEmail) {
        JavascriptExecutor js = (JavascriptExecutor) driver;

        // Precise Tap on Text box
        js.executeScript("mobile: tap", new HashMap<String, Double>() {
            private static final long serialVersionUID = 1L;
            {
                put("tapCount", 1.0);
                put("touchCount", 1.0);
                put("duration", 1.16);
                put("x", 177.0);
                put("y", 210.0);
            }
        });

        driver.findElement(By.name("Clear text")).click();
        List<WebElement> EditText =
                driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIATextField[1]"));
        EditText.get(0).sendKeys(editEmail);
        buttonDone.click();
    }

    /**
     * Verify E-mail Id is edited on Accounts Settings Page
     *
     * @param completeEditedEmailId Edited E-mail Id
     * @return true if edited E-mail Id present else false
     */
    public boolean hasEditedEmailId(String completeEditedEmailId) {
        try {
            return emailIdTextBox.getText().equalsIgnoreCase(completeEditedEmailId);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Edit User Screen Name on Accounts Settings Page
     *
     * @param editUserName UserName
     */
    public void changeUserName(String editUserName) {
        JavascriptExecutor js = (JavascriptExecutor) driver;

        // Precise tap executed on channel text box field
        js.executeScript("mobile: tap", new HashMap<String, Double>() {
            private static final long serialVersionUID = 1L;

            {
                put("tapCount", 1.0);
                put("touchCount", 1.0);
                put("duration", 0.8908789);
                put("x", 148.0);
                put("y", 124.0);
            }
        });

        driver.findElement(By.name("Clear text")).click();
        List<WebElement> EditText =
                driver.findElements(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIATextField[1]"));
        EditText.get(0).sendKeys(editUserName);
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[2]/UIAKeyboard[1]/UIAElement[1]")).click();
        driver.findElement(By.name("Delete")).click();
        buttonDone.click();
    }

    /**
     * Verify UserName is Edited on Accounts settings page
     *
     * @param editUserName UserName or Screen name
     * @return true if edited UserName present else false
     */
    public boolean hasEditedUserScreenName(String editUserName) {
        try {
            return userNameTextBox.getText().equalsIgnoreCase(editUserName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Send Feedback on Account Settings page
     *
     * @param feedbackMessage Message send as feedback
     */
    public void sendFeedback(String feedbackMessage) {
        sendFeedbackLink.click();
        feedbackTextField.sendKeys(feedbackMessage);
        sendButton.click();
    }

    /**
     * Verify Feedback alert message appears
     *
     * @return true if Feedback Sent message is displayed on alert
     */
    public boolean hasFeedbackAlert() {
        try {
            return alertMessage.getText().equalsIgnoreCase("Feedback sent");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Accept alert appears after sending feedback
     */
    public void acceptConfirmationAlert() {
        driver.switchTo().alert().accept();
    }
}
