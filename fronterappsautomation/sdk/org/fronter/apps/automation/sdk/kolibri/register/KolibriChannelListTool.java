package org.fronter.apps.automation.sdk.kolibri.register;

import org.fronter.apps.automation.sdk.kolibri.register.pages.KolibriChannelListPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class KolibriChannelListTool {

    private WebDriver driver;

    public KolibriChannelListTool(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Returns an instance of KolibriChannelListPage class.
     *
     * @return Instance of page object which models KolibriChannelListPage
     */
    public KolibriChannelListPage kolibriChannelListPage() {
        return PageFactory.initElements(driver, KolibriChannelListPage.class);
    }

    /**
     * Swipe on the channel on Channel List Page
     */
    public void swipeOnChannel() {
        kolibriChannelListPage().swipeOnChannel();
    }

    /**
     * Tap on '+' Add button on Channel List Page
     */
    public void tapOnAddIcon() {
        kolibriChannelListPage().tapOnAddIcon();
    }

    /**
     * Create New Channel
     *
     * @param channel Channel Name
     */
    public void createNewChannel(String channel) {
        kolibriChannelListPage().createNewChannel(channel);
    }

    /**
     * Swipe on New Channel on Channel List page
     */
    public void swipeOnNewChannel() {
        kolibriChannelListPage().swipeOnNewChannel();
    }

    /**
     * Tap on '+' add icon and join Channel
     */
    public void joinChannel() {
        kolibriChannelListPage().joinChannel();

    }

    /**
     * Tap on Accounts on Channel List Page
     */
    public void tapOnAccountSettings() {
        kolibriChannelListPage().tapOnAccountSettings();
    }

    /**
     * Tap on Channels Icon on Channel list page
     */
    public void tapOnChannelsIcon() {
        kolibriChannelListPage().tapOnChannelsIcon();
    }
}
