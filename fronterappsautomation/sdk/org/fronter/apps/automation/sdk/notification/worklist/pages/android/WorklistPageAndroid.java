package org.fronter.apps.automation.sdk.notification.worklist.pages.android;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Worklist Page Class
 */
public class WorklistPageAndroid {
    private AndroidDriver androidDriver;

    public WorklistPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Verify given tab is displayed on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is displayed
     */
    public boolean hasTab(String tabName) {
        try {
            return androidDriver.findElementByName(tabName).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that assignment is displayed in worklist
     *
     * @param assignmentName Name of assignment
     * @return true if assignment is displayed in worklist
     */
    public boolean hasAssignment(String assignmentName) {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            boolean isAssignmentDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]")).isDisplayed();

            return isAssignmentDisplayed;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Verify that assignment with correct date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param date Date of assignment
     * @return true if assignment with correct date and Room name is displayed
     */
    public boolean hasAssignmentWithDateAndRoomName(String assignmentName, String roomName, String date, String assignmentStatus) {
        try {
            WebElement assignmentBox = androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]/.."));

            String displayedRoom =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_assignment_room")).getText().trim();

            String displayedDate =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_assignment_due_date")).getText().trim();

            return displayedRoom.equals(roomName) && displayedDate.contains(assignmentStatus + ": " + date);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that number of due days left for assignment is displayed
     *
     * @param assignmentName Name of assignment
     * @param dueDaysLeft Due days left
     * @return true if number of due days left for assignment is displayed
     */
    public boolean displaysDueDaysLeft(String assignmentName, String dueDaysLeft) {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            WebElement assignmentBox = androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]/../.."));

            String daysDisplayed =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_due_days_left_count")).getText().trim();
            String displayedDaysText =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_due_days_left")).getText().trim();
            String dueDaysDisplayed = daysDisplayed + " " + displayedDaysText;

            String expectedDaysText = "days left";

            return dueDaysDisplayed.equals(dueDaysLeft + " " + expectedDaysText);
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Verify that due at time is displayed for assignment
     *
     * @param assignmentName Name of assignment
     * @param dueAtTime Due at time
     * @return true if due at time is displayed for assignment
     */
    public boolean displaysDueAtTime(String assignmentName, String dueAtTime) {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            WebElement assignmentBox = androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]/../.."));

            String dueAtTimeDisplayed =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_due_today_time")).getText().trim();

            return dueAtTimeDisplayed.equals(dueAtTime);
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Verify that due days field is displayed or not
     *
     * @param assignmentName Name of assignment
     * @return true if due days field is displayed
     */
    public boolean displaysDueDaysField(String assignmentName) {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            WebElement assignmentBox = androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]/../.."));

            boolean isDayFeldDisplayed =
                    assignmentBox.findElement(By.id("com.fronter.android:id/worklist_due_days_left_count")).isDisplayed();

            return isDayFeldDisplayed;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Verify that image icon for assignment is displayed
     *
     * @param assignmentName Name of assignment
     * @return true if image icon for assignment is displayed
     */
    public boolean displaysIconforAssignment(String assignmentName) {
        try {
            WebElement assignmentBox = androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]/../.."));

            boolean isImageIconDisplayed = assignmentBox.findElement(By.className("android.widget.ImageView")).isDisplayed();

            return isImageIconDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on Not Submitted tab in Account Page
     */
    public void clickOnNotSubmittedTab() {
        androidDriver.findElementByName("Not submitted").click();
    }

    /**
     * Tap on Submitted tab in Account Page
     */
    public void clickOnSubmittedTab() {
        androidDriver.findElementByName("Submitted").click();
    }

    /**
     * Tap on Evaluated tab in Account Page
     */
    public void clickOnEvaluatedTab() {
        androidDriver.findElementByName("Evaluated").click();
    }

    /**
     * Tab on assignment name to open it
     *
     * @param assignmentName Name of the assignment
     * @return Instance of page object which models AssignmentHeaderPageAndroid
     */
    public AssignmentHeaderPageAndroid clickOnAssignmentHeading(String assignmentName) {
        androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentName + "')]")).click();
        return PageFactory.initElements(androidDriver, AssignmentHeaderPageAndroid.class);
    }

    /**
     * Verify that proper message is displayed
     *
     * @param message message to be verify
     * @return true if message is displayed otherwise false
     */
    public boolean hasMessage(String message) {
        try {
            String msg = androidDriver.findElementById("com.fronter.android:id/no_more_items").getText();
            return msg.equals(message);
        } catch (Exception e) {
            return false;
        }
    }
}
