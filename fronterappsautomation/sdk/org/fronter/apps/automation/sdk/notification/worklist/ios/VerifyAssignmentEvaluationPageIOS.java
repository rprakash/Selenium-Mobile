package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.AssignmentEvaluationPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields of the assignment evaluation page
 */
public class VerifyAssignmentEvaluationPageIOS {
    private IOSDriver iOSDriver;

    public VerifyAssignmentEvaluationPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Returns an instance of AssignmentEvaluationPageIOS class.
     *
     * @return Instance of page object which models AssignmentEvaluationPageIOS
     */
    public AssignmentEvaluationPageIOS assignmentEvaluationPage() {
        return PageFactory.initElements(iOSDriver, AssignmentEvaluationPageIOS.class);
    }

    /**
     * Verify grade and comment of evaluated assignment is displayed
     *
     * @param assignmentGrade Evaluation grade
     * @param evaluationComment Evaluation comment
     */
    public void hasEvaluationGradeAndComments(String assignmentGrade, String evaluationComment) {
        Verify.checkPoint(assignmentEvaluationPage().hasEvaluationGradeAndComments(assignmentGrade, evaluationComment),
                "Evaluated assignment grade '" + assignmentGrade + "', comment '" + evaluationComment + "' is displayed");
    }

    /**
     * Verify the message displayed in evaluation tab
     */
    public void displaysMessage() {
        Verify.checkPoint(assignmentEvaluationPage().displaysMessage(),
                "For full assessment, please log in to Fronter in a browser. message is displayed on evaluation page");
    }

}
