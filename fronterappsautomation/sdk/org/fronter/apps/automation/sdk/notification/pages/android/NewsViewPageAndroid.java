package org.fronter.apps.automation.sdk.notification.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.AndroidWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification News View Page Class
 */
public class NewsViewPageAndroid {
    private AndroidDriver androidDriver;
    private AndroidWait wait;
    private By fullScreenImage = By.id("com.fronter.android:id/fullScreenImage");
    private By detailImage = By.id("com.fronter.android:id/detailImage");
    private By downloadBtn = By.id("com.fronter.android:id/action_download");

    public NewsViewPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
        wait = new AndroidWait(androidDriver);
    }

    /**
     * Verify Room Name exists on News View page
     *
     * @param roomName Name of the Room
     * @return true if Room Name present else false
     */
    public boolean hasRoomName(String roomName) {
        wait.untilElementIsVisible(By.id("com.fronter.android:id/detailHeader"));
        try {
            boolean isRoomNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + roomName + "')]")).isDisplayed();
            return isRoomNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on the News Heading on News List
     */
    public void openNews() {
        androidDriver.findElementById("com.fronter.android:id/list_item_news_header").click();
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        androidDriver.findElementByAccessibilityId("Navigate up").click();
    }



    /**
     * Verify Author First and Last Name exists on News View page
     *
     * @param firstName first name of author
     * @param lastName last name of author
     * @return true if Author First and Last Name present else false
     */
    public boolean hasAuthorName(String firstName, String lastName) {
        try {
            boolean isAuthorNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + firstName + " " + lastName + "')]"))
                            .isDisplayed();

            return isAuthorNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading and News Content exists on News View page
     *
     * @param newsHeading Heading of the News
     * @param newsContent Content of the News
     * @return true if News Heading and News Content present else false
     */
    public boolean hasNewsHeadingWithNewsContent(String newsHeading, String newsContent) {
        try {
            boolean isNewsHeadingDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + newsHeading + "')]")).isDisplayed();
            boolean isNewsContentDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + newsContent + "')]")).isDisplayed();

            return isNewsHeadingDisplayed && isNewsContentDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that image is displayed on news view page
     *
     * @return true if image is displayed on news view page
     */
    public boolean hasImage() {
        try {
            return androidDriver.findElement(detailImage).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Clicks on image
     */
    public void clickOnImage() {
        wait.untilClickDisplaysElement(androidDriver.findElement(detailImage), fullScreenImage);
    }

    /**
     * Verify that full image is displayed with download button on news view page
     *
     * @return true if full image is displayed with download button on news view page
     */
    public boolean hasFullImageWithDownloadButton() {
        try {

            boolean isFullImageDisplayed = androidDriver.findElement(fullScreenImage).isDisplayed();
            boolean isDownloadBtnDisplayed = androidDriver.findElement(downloadBtn).isDisplayed();

            return isFullImageDisplayed && isDownloadBtnDisplayed;
        } catch (Exception e) {
            return false;
        }
    }
}
