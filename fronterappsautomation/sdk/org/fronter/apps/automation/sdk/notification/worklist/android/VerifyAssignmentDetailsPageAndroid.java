package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentDetailsPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class VerifyAssignmentDetailsPageAndroid {
    AndroidDriver androidDriver;

    public VerifyAssignmentDetailsPageAndroid(AndroidDriver androidDriver){
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of AssignmentDetailsPageAndroid class.
     *
     * @return Instance of page object which models AssignmentDetailsPageAndroid
     */
    public AssignmentDetailsPageAndroid assignmentDetailsPageAndroid() {
        return PageFactory.initElements(androidDriver, AssignmentDetailsPageAndroid.class);
    }

    /**
     * Verify that assignment with correct due date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param dueDate Due Date of assignment
     */
    public void hasAssignmentDueDateAndRoomName(String assignmentName, String roomName, String dueDate) {
        Verify.checkPoint(
                assignmentDetailsPageAndroid().hasAssignmentWithDateAndRoomName(assignmentName, roomName, dueDate),
                "Correct room name '" + roomName + "' and due date '" + dueDate + "' is "
                 + "displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that description is displayed
     *
     * @param descriptionTxt text of the description
     */
    public void hasDescription(String descriptionTxt) {
        Verify.checkPoint(assignmentDetailsPageAndroid().hasDescription(descriptionTxt),
                "Description with text '" + descriptionTxt + "' is displayed");
    }

    /**
     * Verifies that Assignment Icon with text is displayed
     */
    public void displaysAssignmentIcon() {
        Verify.checkPoint(assignmentDetailsPageAndroid().displaysIconWithText("Assignment", "Assignment"),
                "Assignment Icon with text is displayed");
    }

    /**
     * Verifies that Individual Icon with text is displayed
     */
    public void displaysIndividualIcon() {
        Verify.checkPoint(
                assignmentDetailsPageAndroid().displaysIconWithText("Individual", "This assignment must be submitted individually"),
                "Individual Icon with text is displayed");
    }

    /**
     * Verifies that Group Icon with text is displayed
     */
    public void displaysGroupIcon() {
        Verify.checkPoint(assignmentDetailsPageAndroid().displaysIconWithText("Group", "This is a group assignment"),
                "Individual Icon with text is displayed");
    }

    /**
     * Verifies that Multiple tries Icon with text is displayed
     */
    public void displaysMultipleTriesIcon() {
        Verify.checkPoint(
                assignmentDetailsPageAndroid().displaysIconWithText("Multiple tries", "This assignment can be submitted multiple times"),
                "Multiple tries Icon with text is displayed");
    }

    /**
     * Verifies that One try Icon with text is displayed
     */
    public void displaysOneTryIcon() {
        Verify.checkPoint(assignmentDetailsPageAndroid().displaysIconWithText("One try", "This assignment only allows one attempt"),
                "One try Icon with text is displayed");
    }

    /**
     * Verifies that uploaded file is displayed
     *
     * @param file file uploaded
     */
    public void hasUploadedFile(String file) {
        Verify.checkPoint(assignmentDetailsPageAndroid().hasFile(file), "Uploaded file '" + file + "' is displayed");
    }
}
