package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.WorklistPageAndroid;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Worklist Page Class
 */
public class WorklistToolAndroid {

    private AndroidDriver androidDriver;

    public WorklistToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of WorklistPageAndroid class.
     *
     * @return Instance of page object which models WorklistPageAndroid
     */
    public WorklistPageAndroid worklistPageAndroid() {
        return PageFactory.initElements(androidDriver, WorklistPageAndroid.class);
    }

    /**
     * Tap on Not Submitted tab in Account Page
     */
    public void clickOnNotSubmittedTab() {
        worklistPageAndroid().clickOnNotSubmittedTab();
    }

    /**
     * Tap on Submitted tab in Account Page
     */
    public void clickOnSubmittedTab() {
        worklistPageAndroid().clickOnSubmittedTab();
    }

    /**
     * Tap on Evaluated tab in Account Page
     */
    public void clickOnEvaluatedTab() {
        worklistPageAndroid().clickOnEvaluatedTab();
    }

    /**
     * Tap on assignment heading
     *
     * @param assignmentName Name of the assignment
     */
	public void openAssignment(String assignmentName) {
		worklistPageAndroid().clickOnAssignmentHeading(assignmentName);
	}
}
