package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NewsListPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification NewsList Tool Page Class
 */
public class NewsListToolIOS {
    private IOSDriver iOSDriver;

    public NewsListToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NewsListPageIOS class.
     *
     * @return Instance of page object which models NewsListPageIOS
     */
    public NewsListPageIOS notificationIOSNewsListPage() {
        return PageFactory.initElements(iOSDriver, NewsListPageIOS.class);
    }

    /**
     * Open account page after login
     */
    public void openAccount() {
        notificationIOSNewsListPage().openAccount();
    }

    /**
     * Tap on the Latest News
     *
     * @param newsTitle Heading of the news
     */
    public void openNews(String newsTitle) {
        notificationIOSNewsListPage().openNews(newsTitle);
    }

    /**
     * Tap on notification Counter
     */
    public void tapOnNotificationIcon() {
        notificationIOSNewsListPage().tapOnNotificationIcon();
    }

    /**
     * Scroll to Bottom of the page on News List
     */
    public void scrollToBottom() {
        notificationIOSNewsListPage().scrollToBottom();
    }
}
