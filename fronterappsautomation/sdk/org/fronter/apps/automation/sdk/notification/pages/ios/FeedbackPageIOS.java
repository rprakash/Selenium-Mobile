package org.fronter.apps.automation.sdk.notification.pages.ios;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.ios.IOSDriver;

/**
 * Feedback page class
 */
public class FeedbackPageIOS {
    private IOSDriver iOSDriver;

    public FeedbackPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Verify feedback title is displayed
     *
     * @return true if Feedback title is displayed on the page, else false
     */
    public boolean hasFeedbackTitle() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean feedbackTitle = iOSDriver.findElementByXPath("//*[contains(@name,'Feedback')]").isDisplayed();
            return feedbackTitle;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify confirmation pop up along with confirmation message is displayed
     *
     * @return true if confirmation pop up is displayed on feedback page, else false
     */
    public boolean displaysConfirmationMessage() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean confirmationTitle =
                    iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Send feedback')]").isDisplayed();
            boolean confirmationMessage =
                    iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Are you sure you want to send the feedback?')]")
                            .isDisplayed();
            boolean cancelButton = iOSDriver.findElementByXPath("//UIACollectionCell[contains(@name,'Cancel')]").isDisplayed();
            boolean sendButton = iOSDriver.findElementByXPath("//UIACollectionCell[contains(@name,'Send')]").isDisplayed();
            return confirmationTitle && confirmationMessage && cancelButton && sendButton;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify error message displayed when feedback is send without text
     *
     * @return true if error message is displayed, else false
     */
    public boolean displaysErrorMessage() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean errorTitle = iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Error')]").isDisplayed();
            boolean errorMsg =
                    iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Please enter your feedback.')]").isDisplayed();
            return errorTitle && errorMsg;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Enter sender email and feedback in the feedback page
     *
     * @param senderEmail email address of the sender
     * @param feedbackText feedback to be send
     */
    public void inputFeedback(String senderEmail, String feedbackText) {
        iOSDriver.findElementByXPath("//UIATextField[contains(@value,'Your e-mail (optional)')]").sendKeys(senderEmail);
        iOSDriver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIATextView[1]").sendKeys(feedbackText);
    }

    /**
     * Click on send feedback button
     */
    public void sendFeedback() {
        iOSDriver.findElementByXPath("//UIAButton[contains(@name,'Send')]").click();
    }

    /**
     * Click on cancel button in confirmation pop up
     */
    public void cancelFeedback() {
        iOSDriver.findElementByXPath("//UIACollectionCell[contains(@name,'Cancel')]").click();
    }

    /**
     * Click on Ok button in error pop up
     */
    public void clickOkButton() {
        iOSDriver.findElementByXPath("//UIACollectionCell[contains(@name,'OK')]").click();
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        iOSDriver.findElementByName("Back").click();
    }
}
