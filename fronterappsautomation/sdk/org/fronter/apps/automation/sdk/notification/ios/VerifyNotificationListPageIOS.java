package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NewsListPageIOS;
import org.fronter.apps.automation.sdk.notification.pages.ios.NotificationListPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify News List Page Class
 */
public class VerifyNotificationListPageIOS {
    IOSDriver iOSDriver;

    public VerifyNotificationListPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NotificationNewsListPageIOS class.
     *
     * @return Instance of page object which models NotificationNewsListPageIOS
     */
    public NotificationListPageIOS notificationListPageIOS() {
        return PageFactory.initElements(iOSDriver, NotificationListPageIOS.class);
    }

    /**
     * Returns an instance of NotificationNewsListPageIOS class.
     *
     * @return Instance of page object which models NotificationNewsListPageIOS
     */
    public NewsListPageIOS newsListPageIOS() {
        return PageFactory.initElements(iOSDriver, NewsListPageIOS.class);
    }

    /**
     * Verify News Heading with Room Name exists on Notification List page
     *
     * @param newsHeading News heading
     * @param roomName Name of the room
     */
    public void displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        Verify.checkPoint(notificationListPageIOS().displaysNewsNotificationWithRoomName(newsHeading, roomName), "News heading '"
                + newsHeading + "' and room name '" + roomName + "' is displayed on Notification List page");
    }

    /**
     * Verify Test Heading with Room Name exists on Notification List page
     *
     * @param testNotification Notification of test
     * @param roomName Name of the room
     */
    public void displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        Verify.checkPoint(notificationListPageIOS().displaysTestNotificationWithRoomName(testNotification, roomName),
                "Test heading '" + testNotification + "' and room name '" + roomName + "' is displayed on Notification List page");

    }

    /**
     * Verify Assignment Heading with Room Name exists on Notification List page
     *
     * @param assignmentNotification Notification of Assignment
     * @param roomName Name of the room
     */
    public void displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        Verify.checkPoint(notificationListPageIOS().displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName),
                "Assignment heading '" + assignmentNotification + "' and room name '" + roomName
                        + "' is displayed on Notification List page");
    }


    /**
     * Verify correct message exists at bottom of the notification list
     *
     * @param message Message on Notification List
     */
    public void displaysMessage(String message) {
        Verify.checkPoint(newsListPageIOS().displaysMessage(message), "'" + message + "' is displayed on notification List page");
    }

    /**
     * Verify that 'No Notification' message exists on Notification list page
     */
    public void displaysNoNotificationsMessage() {
        Verify.checkPoint(notificationListPageIOS().displaysNoNotificationsMessage(),
                "'No Notification' message is displayed when there is no notification on Notification list page");
    }
}
