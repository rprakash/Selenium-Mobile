package org.fronter.apps.automation.sdk.notification.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification News List Page Class
 */
public class NotificationListPageIOS {
    private IOSDriver iOSDriver;

    public NotificationListPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Verify News Heading Room Name exists on Notification List page
     *
     * @param newsHeading News heading
     * @param roomName Name of the Room
     * @return true if News Heading with Room Name present else false
     */
    public boolean displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        try {
            return iOSDriver
                    .findElement(By.xpath("//*[contains(@name,'" + newsHeading + "')]//following-sibling::UIAStaticText[3]"))
                    .getText().trim().equalsIgnoreCase("in " + roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Test Heading with Room Name exists on Notification List page
     *
     * @param testNotification Notification of test
     * @param roomName Name of the Room
     * @return true if test heading with Room Name present else false
     */
    public boolean displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        try {
            return iOSDriver
                    .findElement(By.xpath("//*[contains(@name,'" + testNotification + "')]//following-sibling::UIAStaticText[1]"))
                    .getText().trim().equalsIgnoreCase("in " + roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Assignment Heading with Room Name exists on Notification List page
     *
     * @param assignmentNotification Notification of Assignment
     * @param roomName Name of the Room
     * @return true if assignment heading with Room Name present else false
     */
    public boolean displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        try {
            return iOSDriver
                    .findElement(
                            By.xpath("//*[contains(@name,'" + assignmentNotification + "')]//following-sibling::UIAStaticText[1]"))
                    .getText().trim().equalsIgnoreCase("in " + roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Open Test Notification on Notification List
     *
     * @param testNotification Notification of the test
     */
    public void openTestNotification(String testNotification) {
        iOSDriver.findElement(By.xpath("//*[contains(@name,'" + testNotification + "')]")).click();
    }

    /**
     * Open assignment Notification on Notification List
     *
     * @param assignmentNotification Notification of the assignment
     */
    public void openAssignmentNotification(String assignmentNotification) {
        iOSDriver.findElement(By.xpath("//*[contains(@name,'" + assignmentNotification + "')]")).click();
    }


    /**
     * Open News Notification on Notification List
     *
     * @param newsHeading Heading of the news
     */
    public void openNewsNotification(String newsHeading) {
        iOSDriver.findElement(By.xpath("//*[contains(@name,'" + newsHeading + "')]")).click();
    }

    /**
     * Verify that 'No Notification' message exists on Notification list page
     *
     * @return true if 'No notification' message exists on notification list page, else false
     */
    public boolean displaysNoNotificationsMessage() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            return iOSDriver.findElementByXPath("//*[contains(@name,'No notifications')]").isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }
}
