package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NewsListPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify News List Page Class
 */
public class VerifyNewsListPageAndroid {
    AndroidDriver androidDriver;

    public VerifyNewsListPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationNewsListPageAndroid class.
     *
     * @return Instance of page object which models NotificationNewsListPageAndroid
     */
    public NewsListPageAndroid newsListPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsListPageAndroid.class);
    }

    /**
     * Verify room name is displayed with news on News List page
     *
     * @param newsHeading Heading of the News
     * @param roomName Name of the Room
     */
    public void hasRoomNameWithNews(String newsHeading, String roomName) {
        Verify.checkPoint(newsListPageAndroid().hasRoomNameWithNews(newsHeading, roomName), "Room '" + roomName
                + "' is displayed with news '" + newsHeading + "");
    }

    /**
     * Verify Empty News list has 'No news' message
     */
    public void hasEmptyNews() {
        Verify.checkPoint(newsListPageAndroid().hasEmptyNews(), "Empty News with 'no news' message appear on News List page");
    }

    /**
     * Verify News Heading with News description exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param newsDescription Description of the News
     */
    public void hasNewsHeadingWithNewsDescription(String newsHeading, String newsDescription) {
        Verify.checkPoint(newsListPageAndroid().hasNewsHeadingWithNewsDescription(newsHeading, newsDescription), "News '"
                + newsHeading + "' is displayed with news description '" + newsDescription + "");
    }

    /**
     * Verify News Heading with date exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param presentDate date of the News
     */
    public void hasNewsWithDate(String newsHeading, String presentDate) {
        Verify.checkPoint(newsListPageAndroid().hasNewsWithDate(newsHeading, presentDate), "News heading '" + newsHeading
                + "' with Date '" + presentDate + "' is displayed on News List page");
    }

    /**
     * Verify News Heading with label exists on News List page
     *
     * @param newsHeading Heading of the News
     */
    public void hasTodayLabel(String newsHeading) {
        Verify.checkPoint(newsListPageAndroid().hasTodayLabel(newsHeading), "News heading '" + newsHeading
                + "' with Label 'Today' is displayed on News List page");
    }

    /**
     * Verify News Heading with label exists on News List page
     *
     * @param newsHeading Heading of the News
     */
    public void hasYesterdayLabel(String newsHeading) {
        Verify.checkPoint(newsListPageAndroid().hasYesterdayLabel(newsHeading), "News heading '" + newsHeading
                + "' with Label 'Yesterday' is displayed on News List page");

    }

    /**
     * Verify No more news message is displayed at the end of new list
     *
     * @param msg Message to be verified
     */
	public void displaysNoMoreNewsMsg(String msg) {
		Verify.checkPoint(newsListPageAndroid().displaysNoMoreNewsMsg(msg),
				"'" + msg + "' message is displayed at the end of news list");
	}
}
