package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.AssignmentDetailsPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.AssignmentSubmissionPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields of the assignment submission page
 */
public class VerifyAssignmentSubmissionPageIOS {
    private IOSDriver iOSDriver;

    public VerifyAssignmentSubmissionPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of AssignmentSubmissionPageIOS class.
     *
     * @return Instance of page object which models AssignmentSubmissionPageIOS
     */
    public AssignmentSubmissionPageIOS assignmentSubmissionPageIOS() {
        return PageFactory.initElements(iOSDriver, AssignmentSubmissionPageIOS.class);
    }

    /**
     * Returns an instance of AssignmentDetailsPageIOS class.
     *
     * @return Instance of page object which models AssignmentDetailsPageIOS
     */
    public AssignmentDetailsPageIOS assignmentDetailsPageIOS() {
        return PageFactory.initElements(iOSDriver, AssignmentDetailsPageIOS.class);
    }

    /**
     * Verify that submission date is displayed in assignment submission page
     *
     * @param assignmentSubmittedDate Assignment submission date
     */
    public void hasSubmittedDate(String assignmentSubmittedDate) {
        Verify.checkPoint(assignmentSubmissionPageIOS().hasSubmittedDate(assignmentSubmittedDate),
                "Assignment has submitted date at '" + assignmentSubmittedDate + "'");
    }

    /**
     * Verify that uploaded file is displayed
     *
     * @param uploadedFileName Name of the file
     */
    public void hasUploadedFile(String uploadedFileName) {
        Verify.checkPoint(assignmentDetailsPageIOS().hasFile(uploadedFileName), "Uploaded file '" + uploadedFileName
                + "' is displayed");
    }
}
