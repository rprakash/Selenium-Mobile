package org.fronter.apps.automation.sdk.notification.android;

import org.fronter.apps.automation.sdk.notification.pages.android.HeaderPageAndroid;
import org.fronter.apps.automation.sdk.notification.pages.android.NewsListPageAndroid;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Notification NewsList Tool Page Class
 */
public class NewsListToolAndroid {
    private AndroidDriver androidDriver;

    public NewsListToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NewsListPageAndroid class.
     *
     * @return Instance of page object which models NewsListPageAndroid
     */
    public NewsListPageAndroid newsListPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsListPageAndroid.class);
    }

    /**
     * Tap on the Latest News
     *
     * @param newsHeading News Heading
     */
    public void openNews(String newsHeading) {
        newsListPageAndroid().openNews(newsHeading);
    }

    /**
     * Tap on Notification Counter
     */
    public void tapOnNotificationCounter() {
        headerPageAndroid().tapOnNotificationCounter();
    }

    /**
     * Returns an instance of HeaderPageAndroid class.
     *
     * @return Instance of page object which models HeaderPageAndroid
     */
    public HeaderPageAndroid headerPageAndroid(){
        return PageFactory.initElements(androidDriver, HeaderPageAndroid.class);
    }

}
