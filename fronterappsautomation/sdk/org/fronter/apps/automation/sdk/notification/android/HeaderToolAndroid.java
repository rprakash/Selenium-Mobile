package org.fronter.apps.automation.sdk.notification.android;

import org.fronter.apps.automation.sdk.notification.pages.android.HeaderPageAndroid;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class HeaderToolAndroid {

    private AndroidDriver androidDriver;

    public HeaderToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of HeaderPageAndroid class.
     *
     * @return Instance of page object which models HeaderPageAndroid
     */
    public HeaderPageAndroid headerPageAndroid() {
        return PageFactory.initElements(androidDriver, HeaderPageAndroid.class);
    }

    /**
     * Open account page after login
     */
    public void openAccount() {
        headerPageAndroid().openAccount();
    }

}
