package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.AccountPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification Account Tool Page Class
 */
public class AccountToolAndroid {

    private AndroidDriver androidDriver;

    public AccountToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of AccountPageAndroid class.
     *
     * @return Instance of page object which models AccountPageAndroid
     */
    public AccountPageAndroid accountPageAndroid() {
        return PageFactory.initElements(androidDriver, AccountPageAndroid.class);
    }

    /**
     * Tap on Logout in Account Page
     */
    public void logout() {
        accountPageAndroid().logout();
    }

    /**
     * Tap on News in Account Page
     */
    public void clickOnNews() {
        accountPageAndroid().clickOnNews();
    }

    /**
     * Tap on Worklist in Account Page
     */
    public void clickOnWorklist() {
        accountPageAndroid().clickOnWorklist();
    }

    /**
     * Tap on Give Feedback in Account Page
     */
    public void clickOnGiveFeedback() {
        accountPageAndroid().clickOnGiveFeedback();
    }

    /**
     * Tap on Institution list drop down
     */
    public void clickOnInstitution() {
        accountPageAndroid().clickOnInstitutionListDropdown();
    }

    /**
     * Tap on institution name from drop down
     *
     * @param institutionName Name of the institution
     */
    public void clickOnInstitutionName(String institutionName) {
        accountPageAndroid().clickOnInstitutionName(institutionName);
    }

    /**
     * Tap on logout
     */
    public void clickOnLogout() {
        accountPageAndroid().clickOnLogout();
    }

    /**
     * Tap on Cancel button of logout pop-up
     */
    public void cancelLogout() {
        accountPageAndroid().clickOnCancelButton();
    }
}
