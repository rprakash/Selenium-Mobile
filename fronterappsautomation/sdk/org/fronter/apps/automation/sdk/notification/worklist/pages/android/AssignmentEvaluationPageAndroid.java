package org.fronter.apps.automation.sdk.notification.worklist.pages.android;

import org.fronter.apps.automation.sdk.AndroidWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * Assignments Submitted Page Class
 */
public class AssignmentEvaluationPageAndroid {
    private AndroidDriver androidDriver;
    private AndroidWait wait;

    public AssignmentEvaluationPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
        wait = new AndroidWait(androidDriver);
    }

    /**
     * Verify given tab is displayed on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is displayed otherwise false
     */
    public boolean hasTab(String tabName) {
        try {
            return androidDriver.findElementByName(tabName).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify grade, comment and date of evaluated assignment are displayed
     *
     * @param grade Evaluation grade
     * @param evaluationComment Evaluation comment
     * @param evaluationDate Evaluation date
     *
     * @return true if grade, comment and date are displayed otherwise false
     */
    public boolean displaysGradeCommentAndDate(String grade, String evaluationComment, String evaluationDate) {
        By gradeSection = By.xpath("//*[contains(@text,'Grade')]/..");
        wait.untilElementIsVisible(gradeSection);
        try {
            WebElement evalGrade = androidDriver.findElement(gradeSection);
            String gradeText =
                    evalGrade.findElement(By.id("com.fronter.android:id/assignment_evaluation_grade")).getText().trim();

            String evaluationText =
                    androidDriver.findElement(By.id("com.fronter.android:id/assignment_evaluation_comment")).getText().trim();

            WebElement evalDate = androidDriver.findElement(By.xpath("//*[contains(@text,'Evaluation date')]/.."));
            String date = evalDate.findElement(By.id("com.fronter.android:id/assignment_evaluation_date")).getText().trim();

            return (gradeText.equals(grade) && evaluationText.equals(evaluationComment) && date.contains(evaluationDate));
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify informatory message
     *
     * @param message message to be verified
     * @return true if infomatory message is displayed
     */
    public boolean hasMessage(String message) {
        try {
            return (androidDriver.findElement(By.xpath("//*[contains(@text,'" + message + "')]")).isDisplayed());
        } catch (Exception e) {
            return false;
        }
    }
}
