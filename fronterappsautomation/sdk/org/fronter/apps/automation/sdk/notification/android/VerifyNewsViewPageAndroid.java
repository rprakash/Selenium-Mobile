package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NewsViewPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify News View Page Class
 */
public class VerifyNewsViewPageAndroid {
    AndroidDriver androidDriver;

    public VerifyNewsViewPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationNewsViewPageAndroid class.
     *
     * @return Instance of page object which models NotificationNewsViewPageAndroid
     */
    public NewsViewPageAndroid newsViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsViewPageAndroid.class);
    }

    /**
     * Verify Room Name exists on News View page
     *
     * @param roomName Name of the Room
     */
    public void hasRoomName(String roomName) {
        Verify.checkPoint(newsViewPageAndroid().hasRoomName(roomName), "Room '" + roomName + "' exists on News View page");
    }

    /**
     * Verify Author First and Last Name exists on News View page
     *
     * @param firstname first name of author
     * @param lastname last name of author
     */
    public void hasAuthorName(String firstName, String lastName) {
        Verify.checkPoint(newsViewPageAndroid().hasAuthorName(firstName, lastName), "Author First name '" + firstName
                + "' and Last Name '" + lastName + "' exists on News View page");
    }

    /**
     * Verify News Heading and News Content exists on News View page
     *
     * @param newsHeading Heading of the News
     * @param newsContent Content of the News
     */
    public void hasNewsHeadingWithNewsContent(String newsHeading, String newsContent) {
        Verify.checkPoint(newsViewPageAndroid().hasNewsHeadingWithNewsContent(newsHeading, newsContent), "News Heading '"
                + newsHeading + "' with News Content exists on News View page");
    }

    /**
     * Verify that image is displayed on news view page
     */
    public void hasImage() {
        Verify.checkPoint(newsViewPageAndroid().hasImage(), "Image is displayed in News View Page");
    }

    /**
     * Verify that full image is displayed with download button on news view page
     */
    public void hasFullImageWithDownloadButton() {
        Verify.checkPoint(newsViewPageAndroid().hasFullImageWithDownloadButton(), "Full Image with Download button is displayed");
    }
}
