package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NewsViewPageAndroid;
import org.fronter.apps.automation.sdk.notification.pages.android.NotificationViewPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify Notification View Page Class
 */
public class VerifyNotificationViewPageAndroid {
    AndroidDriver androidDriver;

    public VerifyNotificationViewPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationViewPageAndroid class.
     *
     * @return Instance of page object which models NotificationViewPageAndroid
     */
    public NotificationViewPageAndroid notificationViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NotificationViewPageAndroid.class);
    }

    /**
     * Returns an instance of NotificationNewsViewPageAndroid class.
     *
     * @return Instance of page object which models NotificationNewsViewPageAndroid
     */
    public NewsViewPageAndroid notificationNewsViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsViewPageAndroid.class);
    }

    /**
     * Verify Test Notification exists on Notification detailed view page
     *
     * @param testNotification Name of the Test Notification
     * @param roomName Name of the Room
     */
    public void displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        Verify.checkPoint(notificationViewPageAndroid().displaysTestNotificationWithRoomName(testNotification, roomName),
                "Test Notification exists on Notification View page");
    }

    /**
     * Verify Assignment Notification exists on Notification detailed view page
     *
     * @param assignmentNotification Name of the Assignment Notification
     * @param roomName Name of the Room
     */
    public void displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        Verify.checkPoint(
                notificationViewPageAndroid().displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName),
                "Assignment Notification exists on Notification View page");
    }

    /**
     * Verify News Notification exists on Notification detailed view page
     *
     * @param newsHeading Name of the News Notification
     * @param roomName Name of the Room
     */
    public void displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        Verify.checkPoint(notificationViewPageAndroid().displaysNewsNotificationWithRoomName(newsHeading, roomName),
                "News Notification exists on Notification View page");

    }

    /**
     * Verify 'Open in Fronter' link exists on Notification detailed view page
     */
    public void displaysOpenInFronterLink() {
        Verify.checkPoint(notificationViewPageAndroid().displaysOpenInFronterLink(),
                "'Open in Fronter' link exists on Notification View page");
    }

    /**
     * Verify that image is displayed on news notification view page
     */
    public void hasImage() {
        Verify.checkPoint(notificationNewsViewPageAndroid().hasImage(), "Image is displayed in Notification View Page");
    }

    /**
     * Verify that full image is displayed with download button on Notification view page
     */
    public void hasFullImageWithDownloadButton() {
        Verify.checkPoint(notificationNewsViewPageAndroid().hasFullImageWithDownloadButton(),
                "Full Image with Download button is displayed in Notification View Page");
    }
}
