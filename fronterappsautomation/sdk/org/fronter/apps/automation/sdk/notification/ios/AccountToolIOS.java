package org.fronter.apps.automation.sdk.notification.ios;

import org.fronter.apps.automation.sdk.notification.pages.ios.AccountPageIOS;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

/**
 * Notification Account Tool Page Class
 */
public class AccountToolIOS {
    private IOSDriver iOSDriver;

    public AccountToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of AccountPageIOS class.
     *
     * @return Instance of page object which models AccountPageIOS
     */
    public AccountPageIOS accountPageIOS() {
        return PageFactory.initElements(iOSDriver, AccountPageIOS.class);
    }

    /**
     * Tap on Account icon
     */
    public void tapOnAccountIcon() {
        accountPageIOS().tapOnAccountIcon();
    }

    /**
     * Tap on Logout in Account Page
     */
    public void logout() {
        accountPageIOS().logout();
    }

    /**
     * Tap on institution name
     *
     * @param institutionName Name of the institution
     */
    public void tapOnInstitutionName(String institutionName) {
        accountPageIOS().tapOnInstitutionName(institutionName);
    }

    /**
     * Tap on change button in change institution confirmation pop up
     */
    public void changeInstitution(){
        accountPageIOS().changeInstitution();
    }

    /**
     * Tap on Send Feedback link
     */
    public void tapOnFeedbackLink(){
        accountPageIOS().tapOnFeedbackLink();
    }
}
