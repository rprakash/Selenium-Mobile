package org.fronter.apps.automation.sdk.notification.ios;

import org.fronter.apps.automation.sdk.notification.pages.ios.FeedbackPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

/**
 * Verify Feedback Page class
 */
public class VerifyFeedbackPageIOS {
    private IOSDriver iOSDriver;

    public VerifyFeedbackPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Returns an instance of FeedbackPageIOS class.
     *
     * @return Instance of page object which models FeedbackPageIOS
     */
    public FeedbackPageIOS feedbackPageIOS() {
        return PageFactory.initElements(iOSDriver, FeedbackPageIOS.class);
    }

    /**
     * Verify feedback title is displayed
     */
    public void hasFeedbackTitle() {
        Verify.checkPoint(feedbackPageIOS().hasFeedbackTitle(), "Feedback title is displayed");
    }

    /**
     * Verify confirmation pop up along with confirmation message is displayed
     */
    public void displaysConfirmationMessage() {
        Verify.checkPoint(feedbackPageIOS().displaysConfirmationMessage(), "Send Feedback Confirmation pop up is displayed");
    }

    /**
     * Verify error message pop up is displayed
     */
    public void displaysErrorMessage() {
        Verify.checkPoint(feedbackPageIOS().displaysErrorMessage(), "Error message pop up is displayed");
    }
}
