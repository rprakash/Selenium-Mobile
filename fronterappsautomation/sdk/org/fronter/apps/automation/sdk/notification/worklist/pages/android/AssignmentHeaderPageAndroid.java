package org.fronter.apps.automation.sdk.notification.worklist.pages.android;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;

public class AssignmentHeaderPageAndroid {
    private AndroidDriver androidDriver;

    public AssignmentHeaderPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Verify that specified tab is displayed
     *
     * @param tabName Name of tab
     * @return true if specified tab is displayed otherwise false
     */
    public boolean hasTab(String tabName) {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            return androidDriver.findElementByXPath("//*[contains(@text,'" + tabName + "')]").isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }

    /**
     * Tab on Details tab
     *
     * @param tabName Name of tab
     */
    public void clickOnTab(String tabName) {
        androidDriver.findElementByXPath("//*[contains(@text, '" + tabName + "')]").click();
    }

    /**
     * Verify that assignment title and status is displayed
     *
     * @param assignmentName Name of the assignment
     * @param assignmentStatus Status of the assignment
     * @return true if assignment with correct status is displayed
     */
    public boolean hasAssignmentTitleWithStatus(String assignmentName, String assignmentStatus) {
        try {
            String assignmentTitle =
                    androidDriver.findElement(By.id("com.fronter.android:id/toolbar_title")).getText().trim();
            String status;
            if(assignmentStatus.equals("Not submitted")) {
                status = androidDriver.findElement(By.id("com.fronter.android:id/toolbar_assignment_status_not_submitted")).getText().trim();
            } else {
                status = androidDriver.findElement(By.id("com.fronter.android:id/toolbar_assignment_status_"+assignmentStatus.toLowerCase())).getText().trim();
            }
            return (assignmentTitle.equals(assignmentName) && status.equals(assignmentStatus));
        } catch (Exception e) {
            return false;
        }
    }
}
