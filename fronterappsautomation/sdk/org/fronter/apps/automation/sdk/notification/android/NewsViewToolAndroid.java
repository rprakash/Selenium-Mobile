package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NewsViewPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification News View Tool Page Class
 */
public class NewsViewToolAndroid {
    private AndroidDriver androidDriver;

    public NewsViewToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NewsViewPageAndroid class.
     *
     * @return Instance of page object which models NewsViewPageAndroid
     */
    public NewsViewPageAndroid newsViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsViewPageAndroid.class);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        newsViewPageAndroid().navigateBack();
    }

    /**
     * Clicks on image
     */
    public void clickOnImage() {
        newsViewPageAndroid().clickOnImage();
    }
}
