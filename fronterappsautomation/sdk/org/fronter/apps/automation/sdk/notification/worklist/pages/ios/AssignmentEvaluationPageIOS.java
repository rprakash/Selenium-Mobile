package org.fronter.apps.automation.sdk.notification.worklist.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;

/**
 * Models the assignment evaluation page class
 */
public class AssignmentEvaluationPageIOS {
    private IOSDriver iOSDriver;

    public AssignmentEvaluationPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Verify grade and comment of evaluated assignment is displayed
     *
     * @param assignmentGrade Evaluation grade
     * @param evaluationComment Evaluation comment
     * @return true if grade, comment and date are displayed, else false
     */
    public boolean hasEvaluationGradeAndComments(String assignmentGrade, String evaluationComment) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean displayedGrade = iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentGrade + "')]").isDisplayed();
            boolean displayedComment =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + evaluationComment + "')]").isDisplayed();
            return displayedGrade && displayedComment;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify "For full assessment, please log in to Fronter in a browser." message displayed in evaluation tab
     *
     * @return true if given message is displayed on the evaluation page, else false
     */
    public boolean displaysMessage() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean displayedMessage =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@name,'For full assessment, please log in to Fronter in a browser.')]").isDisplayed();
            return displayedMessage;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }
}
