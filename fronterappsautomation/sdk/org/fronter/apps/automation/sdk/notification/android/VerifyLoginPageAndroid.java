package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.LoginPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify Login Page Class
 */
public class VerifyLoginPageAndroid {
    AndroidDriver androidDriver;

    public VerifyLoginPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationLoginPageAndroid class.
     *
     * @return Instance of page object which models NotificationLoginPageAndroid
     */
    public LoginPageAndroid loginPageAndroid() {
        return PageFactory.initElements(androidDriver, LoginPageAndroid.class);
    }

    /**
     * Verify 'Wrong username or password!' Message appears when user entered invalid user credentials
     *
     * @param errorMessage error Message
     */
    public void hasMessageDisplayed(String errorMessage) {
        Verify.checkPoint(loginPageAndroid().hasMessageDisplayed(errorMessage),
                "Wrong username or password! message exists on Login page");
    }
}
