package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.AssignmentDetailsPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to perform operation on Assignment Details Page
 */
public class AssignmentDetailsToolIOS {
    private IOSDriver iOSDriver;

    public AssignmentDetailsToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of AssignmentDetailsPageIOS class.
     *
     * @return Instance of page object which models AssignmentDetailsPageIOS
     */
    public AssignmentDetailsPageIOS assignmentDetailsPage() {
        return PageFactory.initElements(iOSDriver, AssignmentDetailsPageIOS.class);
    }

    /**
     * Tap on Submitted tab in WorkList Page
     */
    public void clickOnSubmissionTab() {
        assignmentDetailsPage().clickOnTab("Submission");
    }

    /**
     * Tap on Evaluated tab in WorkList Page
     */
    public void clickOnEvaluationTab() {
        assignmentDetailsPage().clickOnTab("Evaluation");
    }
}
