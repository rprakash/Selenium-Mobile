package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NewsListPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify News List Page Class
 */
public class VerifyNewsListPageIOS {
    IOSDriver iOSDriver;

    public VerifyNewsListPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NewsListPageIOS class.
     *
     * @return Instance of page object which models NewsListPageIOS
     */
    public NewsListPageIOS newsListPageIOS() {
        return PageFactory.initElements(iOSDriver, NewsListPageIOS.class);
    }

    /**
     * Verify Room Name exists on News List page
     *
     * @param newsHeading News heading
     * @param roomName Name of the room
     */
    public void displaysNewsWithRoomName(String newsHeading, String roomName) {
        Verify.checkPoint(newsListPageIOS().displaysNewsWithRoomName(newsHeading, roomName), "News heading '" + newsHeading
                + "' and room name '" + roomName + "' is displayed on News List page");
    }

    /**
     * Verify correct message exists at bottom of the news list
     *
     * @param message Message on News Page
     */
    public void displaysMessage(String message) {
        Verify.checkPoint(newsListPageIOS().displaysMessage(message), "'" + message
                + "' is displayed at bottom of News List page");
    }

    /**
     * Verify News Heading with date exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param presentDate date of the News
     */
    public void hasNewsWithDate(String newsHeading, String presentDate) {
        Verify.checkPoint(newsListPageIOS().hasNewsWithDate(newsHeading, presentDate), "News heading '" + newsHeading
                + "' with Date '" + presentDate + "' is displayed on News List page");
    }

    /**
     * Verify News Heading with label exists on News List page
     *
     * @param newsHeading Heading of the News
     */
    public void hasTodayLabel(String newsHeading) {
        Verify.checkPoint(newsListPageIOS().hasTodayLabel(newsHeading), "News heading '" + newsHeading
                + "' with Label 'Today' is displayed on News List page");
    }

    /**
     * Verify News Heading with label exists on News List page
     *
     * @param newsHeading Heading of the News
     */
    public void hasYesterdayLabel(String newsHeading) {
        Verify.checkPoint(newsListPageIOS().hasYesterdayLabel(newsHeading), "News heading '" + newsHeading
                + "' with Label ' Yesterday ' is displayed on News List page");
    }

    /**
     * Verify that 'No News' message exists on news List page
     */
    public void displaysNoNewsMessage() {
        Verify.checkPoint(newsListPageIOS().displaysNoNewsMessage(),
                "'No news' message is displayed when user does not have active news");
    }
}
