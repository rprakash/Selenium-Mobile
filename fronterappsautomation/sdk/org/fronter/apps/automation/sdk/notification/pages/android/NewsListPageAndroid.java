package org.fronter.apps.automation.sdk.notification.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.AndroidWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification Login Page Class
 */
public class NewsListPageAndroid {
    private AndroidDriver androidDriver;
    private String NewsHeader = "com.fronter.android:id/list_item_news_header";
    private AndroidWait wait;

    public NewsListPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
        wait = new AndroidWait(androidDriver);
    }

    /**
     * Verify room name is displayed with news on News List page
     *
     * @param newsHeading Heading of the News
     * @param roomName Name of the Room
     * @return true if room name is displayed with news
     */
    public boolean hasRoomNameWithNews(String newsHeading, String roomName) {
        wait.untilElementIsVisible(By.id(NewsHeader));
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + newsHeading
                                    + "')]/../../..//android.widget.LinearLayout[1]/android.widget.TextView[1]")).getText()
                    .trim().equalsIgnoreCase(roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Open News on News List
     *
     * @param newsHeading News Heading
     */
    public void openNews(String newsHeading) {
        androidDriver.findElement(By.xpath("//*[contains(@text,'" + newsHeading + "')]")).click();
    }

    /**
     * Verify Empty News list has 'No news' message
     *
     * @return true if No news message present else false
     */
    public boolean hasEmptyNews() {
        wait.untilElementIsVisible(By.name("No news, pull down to refresh"));
        try {
            return androidDriver.findElementByName("No news, pull down to refresh").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with News Description exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param newsDescription Description of the News
     * @return true if News Heading with News Description present else false
     */
    public boolean hasNewsHeadingWithNewsDescription(String newsHeading, String newsDescription) {
        wait.untilElementIsVisible(By.id(NewsHeader));
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + newsHeading + "')]/following-sibling::android.widget.TextView[1]"))
                    .getText().trim().equalsIgnoreCase(newsDescription);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with date exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param presentDate Date of the News
     * @return true if date present else false
     */
    public boolean hasNewsWithDate(String newsHeading, String presentDate) {
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + newsHeading
                                    + "')]/../../..//android.widget.LinearLayout[1]/android.widget.TextView[2]")).getText()
                    .trim().equalsIgnoreCase(presentDate);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with today label exists on News List page
     *
     * @param newsHeading Heading of the News
     * @return true if today label present else false
     */
    public boolean hasTodayLabel(String newsHeading) {
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + newsHeading
                                    + "')]/../../../../../..//android.widget.LinearLayout[1]/android.widget.TextView[1]"))

                    .getText().trim().equalsIgnoreCase("Today");

        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with yesterday label exists on News List page
     *
     * @param newsHeading Heading of the News
     * @return true if yesterday label present else false
     */
    public boolean hasYesterdayLabel(String newsHeading) {
        try {
            boolean hasYesterdayLabel =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + newsHeading + "')]/../../../../..//*[contains(@text,'Yesterday')]"))
                    .isDisplayed();
            return hasYesterdayLabel;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify No more news message is displayed at the end of new list
     *
     * @param msg msg to be verified 
     */
	public boolean displaysNoMoreNewsMsg(String msg) {
		try{
			String message = androidDriver.findElement(By.id("com.fronter.android:id/no_more_news")).getText();
			return msg.equals(message);
		}catch(Exception e){
			return false;
		}
	}
}
