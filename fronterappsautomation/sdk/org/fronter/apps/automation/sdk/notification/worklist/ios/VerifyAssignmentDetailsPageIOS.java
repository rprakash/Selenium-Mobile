package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.AssignmentDetailsPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields of the assignment details page
 */
public class VerifyAssignmentDetailsPageIOS {
    IOSDriver iOSDriver;

    public VerifyAssignmentDetailsPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of AssignmentDetailsPageIOS class.
     *
     * @return Instance of page object which models AssignmentDetailsPageIOS
     */
    public AssignmentDetailsPageIOS assignmentDetailsPageIOS() {
        return PageFactory.initElements(iOSDriver, AssignmentDetailsPageIOS.class);
    }

    /**
     * Verify that specified tab is displayed
     *
     * @param tabName Name of tab
     */
    public void hasTab(String tabName) {
        Verify.checkPoint(assignmentDetailsPageIOS().hasTab(tabName), "'" + tabName + "' tab is displayed");
    }

    /**
     * Verify that Details tab is displayed
     */
    public void hasDetailsTab() {
        hasTab("Details");
    }

    /**
     * Verify that Submission tab is displayed
     */
    public void hasSubmissionTab() {
        hasTab("Submission");
    }

    /**
     * Verify that Evaluation tab is displayed
     */
    public void hasEvaluationTab() {
        hasTab("Evaluation");
    }

    /**
     * Verify that specified tab is disabled
     *
     * @param tabName Name of tab
     */
    public void isTabDisabled(String tabName) {
        Verify.checkPoint(!assignmentDetailsPageIOS().isTabEnabled(tabName), "'" + tabName + "' tab is disabled");
    }

    /**
     * Verify that Submission tab is disabled
     */
    public void hasDisabledSubmissionTab() {
        isTabDisabled("Submission");
    }

    /**
     * Verify that Evaluation tab is disabled
     */
    public void hasDisabledEvaluationTab() {
        isTabDisabled("Evaluation");
    }

    /**
     * Verify that assignment with correct due date and status is displayed in assignment detail view
     *
     * @param assignmentName Name of assignment
     * @param assignmentDueDate Due Date of assignment
     * @param assignmentStatus Status for the assignment e.g "Not Submitted"
     */
    public void hasAssignmentWithDateAndStatus(String assignmentName, String assignmentDueDate, String assignmentStatus) {
        Verify.checkPoint(
                assignmentDetailsPageIOS().hasAssignmentWithDateAndStatus(assignmentName, assignmentDueDate, assignmentStatus),
                "Assignment with title '" + assignmentName + "' with due date '" + assignmentDueDate + "' and status '"
                        + assignmentStatus + "'is displayed");
    }

    /**
     * Verify that assignment with correct due date and Not Submitted status is displayed
     *
     * @param assignmentName Name of assignment
     * @param assignmentDueDate Due Date of assignment
     */
    public void hasNotSubmittedAssignmentWithDate(String assignmentName, String assignmentDueDate) {
        hasAssignmentWithDateAndStatus(assignmentName, assignmentDueDate, "Not submitted");
    }

    /**
     * Verify that assignment with correct due date and Submitted status is displayed
     *
     * @param assignmentName Name of assignment
     * @param assignmentDueDate Due Date of assignment
     */
    public void hasSubmittedAssignmentWithDate(String assignmentName, String assignmentDueDate) {
        hasAssignmentWithDateAndStatus(assignmentName, assignmentDueDate, "Submitted");
    }

    /**
     * Verify that assignment with correct due date and Evaluated status is displayed
     *
     * @param assignmentName Name of assignment
     * @param assignmentDueDate Due Date of assignment
     */
    public void hasEvaluatedAssignmentWithDate(String assignmentName, String assignmentDueDate) {
        hasAssignmentWithDateAndStatus(assignmentName, assignmentDueDate, "Evaluated");
    }

    /**
     * Verifies that Assignment text is displayed
     */
    public void displaysAssignmentIconText() {
        Verify.checkPoint(assignmentDetailsPageIOS().displaysIconText("Assignment"), "Assignment icon with text is displayed");
    }

    /**
     * Verifies that Individual text is displayed
     */
    public void displaysIndividualIconText() {
        Verify.checkPoint(assignmentDetailsPageIOS().displaysIconText("Individual"), "Individual icon with text is displayed");
    }

    /**
     * Verifies that Multiple tries text is displayed
     */
    public void displaysMultipleTriesIconText() {
        Verify.checkPoint(assignmentDetailsPageIOS().displaysIconText("Multiple tries"),
                "Multiple tries icon with text is displayed");
    }

    /**
     * Verify that description is displayed
     *
     * @param assignmentDesc text of the description
     */
    public void hasDescription(String assignmentDesc) {
        Verify.checkPoint(assignmentDetailsPageIOS().hasDescription(assignmentDesc), "Description with text '" + assignmentDesc
                + "' is displayed");
    }

    /**
     * Verify that uploaded file is displayed
     *
     * @param uploadedFileName Name of the file
     */
    public void hasUploadedFile(String uploadedFileName) {
        Verify.checkPoint(assignmentDetailsPageIOS().hasFile(uploadedFileName), "Uploaded file '" + uploadedFileName
                + "' is displayed");
    }
}
