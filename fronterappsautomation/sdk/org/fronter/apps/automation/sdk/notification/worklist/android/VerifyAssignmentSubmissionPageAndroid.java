package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentSubmissionPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class VerifyAssignmentSubmissionPageAndroid {
    AndroidDriver androidDriver;

    public VerifyAssignmentSubmissionPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Returns an instance of AssignmentSubmissionPageAndroid class.
     *
     * @return Instance of page object which models AssignmentSubmissionPageAndroid
     */
    public AssignmentSubmissionPageAndroid assignmentSubmissionPageAndroid(){
        return PageFactory.initElements(androidDriver, AssignmentSubmissionPageAndroid.class);
    }

    /**
     * Verify assignment submitted date in Submission Page
     * @param submittedDate Date of submission
     */
    public void hasSubmittedDate(String submittedDate) {
        Verify.checkPoint(assignmentSubmissionPageAndroid().hasSubmittedDate(submittedDate),
                "Submitted Date of the assignment is displayed");
    }
}
