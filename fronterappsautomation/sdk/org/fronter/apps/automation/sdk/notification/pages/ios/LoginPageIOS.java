package org.fronter.apps.automation.sdk.notification.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Notification Login Page Class
 */
public class LoginPageIOS {
    private IOSDriver iOSDriver;


    public LoginPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    // Mobile Elements identifiers
    String installerName = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]";
    String userIdTestBox = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIATextField[1]";
    String passwdTestBox = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIASecureTextField[1]";
    String loginButton = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAButton[1]";

    /**
     * Enter Installation Name
     *
     * @param installationName name of the url
     */
    public void enterInstallationName(String installationName) {
        WebDriverWait wait = new WebDriverWait(iOSDriver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(installerName)));
        iOSDriver.findElementByXPath(installerName).sendKeys(installationName);
        iOSDriver.findElementByXPath("//UIAStaticText[contains(@label,'" + installationName + "')]").click();
    }

    /**
     * Selects the login provide e.g Fronter IDP Login
     */
    public void selectLoginProvider(String loginProvider) {
        iOSDriver.findElementByXPath("//UIAStaticText[contains(@label,'" + loginProvider + "')]").click();
    }

    /**
     * Enter Login Credentials of User and tap on login button
     *
     * @param userName userId of the User
     * @param password password of the User
     */
    public void login(String userName, String password) {
        iOSDriver.findElementByXPath(userIdTestBox).sendKeys(userName);
        iOSDriver.findElementByXPath(passwdTestBox).sendKeys(password);
        iOSDriver.findElementByName("Done").click();
        iOSDriver.findElementByXPath(loginButton).click();
    }

    /**
     * Enter Login Credentials of User and tap on "GO" button
     *
     * @param userName userId of the User
     * @param password password of the User
     */
    public void loginWithGoButton(String userName, String password) {
        iOSDriver.findElementByXPath(userIdTestBox).sendKeys(userName);
        iOSDriver.findElementByXPath(passwdTestBox).sendKeys(password);
        iOSDriver.findElementByName("Go").click();
    }

    /**
     * Verify 'Wrong username or password!' Message appears when user entered invalid user credentials
     *
     * @param errorMessage error Message
     * @return true if 'Wrong username or password!' message appears else false
     */
    public boolean hasMessageDisplayed(String errorMessage) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + errorMessage + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
