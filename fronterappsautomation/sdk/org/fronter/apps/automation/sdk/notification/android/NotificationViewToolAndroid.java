package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NewsViewPageAndroid;
import org.fronter.apps.automation.sdk.notification.pages.android.NotificationViewPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification View Tool Page Class
 */
public class NotificationViewToolAndroid {
    private AndroidDriver androidDriver;

    public NotificationViewToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationNewsViewPageAndroid class.
     *
     * @return Instance of page object which models NotificationNewsViewPageAndroid
     */
    public NotificationViewPageAndroid notificationViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NotificationViewPageAndroid.class);
    }

    /**
     * Returns an instance of NotificationNewsViewPageAndroid class.
     *
     * @return Instance of page object which models NotificationNewsViewPageAndroid
     */
    public NewsViewPageAndroid notificationNewsViewPageAndroid() {
        return PageFactory.initElements(androidDriver, NewsViewPageAndroid.class);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        notificationViewPageAndroid().navigateBack();
    }

    /**
     * Tap on the Back Button on phone
     */
    public void clickBackButtonOnPhone() {
        notificationViewPageAndroid().clickBackButtonOnPhone();
    }

    /**
     * Click Open In Fronter link
     */
    public void clickOpenInFronterLink() {
        notificationViewPageAndroid().clickOpenInFronterLink();
    }

    /**
     * Clicks on image
     */
    public void clickOnImage() {
        notificationNewsViewPageAndroid().clickOnImage();
    }
}
