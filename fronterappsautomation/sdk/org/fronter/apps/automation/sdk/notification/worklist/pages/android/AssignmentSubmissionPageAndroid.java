package org.fronter.apps.automation.sdk.notification.worklist.pages.android;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;

public class AssignmentSubmissionPageAndroid {
    private AndroidDriver androidDriver;

    public AssignmentSubmissionPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * /**
     * Verify assignment submitted date in Submission Page
     * @param submittedDate Date of submission
     *
     * @return true if submitted date is displayed otherwise false
     */
    public boolean hasSubmittedDate(String submittedDate) {
        String displayedDate = androidDriver.findElement(By.id("com.fronter.android:id/assignment_submission_date"))
                .getText().trim();
        return displayedDate.contains(submittedDate);
    }

}
