package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.LoginPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification Login Tool Page Class
 */
public class LoginToolIOS {
    private IOSDriver iOSDriver;

    public LoginToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of LoginPageIOS class.
     *
     * @return Instance of page object which models LoginPageIOS
     */
    public LoginPageIOS loginPageIOS() {
        return PageFactory.initElements(iOSDriver, LoginPageIOS.class);
    }

    /**
     * Enter Installation Name
     *
     * @param installationName name of the installation
     */
    public void enterInstallationName(String installationName) {
        loginPageIOS().enterInstallationName(installationName);
    }

    /**
     * Selects the login provide e.g Fronter IDP Login
     */
    public void selectFronterLoginProvider() {
        loginPageIOS().selectLoginProvider("Fronter Login");
    }

    /**
     * Enter Login Credentials of User and tap on Login Button
     *
     * @param userName userId of the user
     * @param password password of the user
     */
    public void login(String userName, String password) {
        loginPageIOS().login(userName, password);
    }

    /**
     * Enter Login Credentials of User and tap on 'Go' button
     *
     * @param userName userId of the user
     * @param password password of the user
     */
    public void loginWithGoButton(String userName, String password) {
        loginPageIOS().loginWithGoButton(userName, password);
    }
}
