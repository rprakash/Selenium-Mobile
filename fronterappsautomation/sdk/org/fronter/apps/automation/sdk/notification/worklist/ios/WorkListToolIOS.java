package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.WorkListPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to perform operation on Worklist Page
 */
public class WorkListToolIOS {
    private IOSDriver iOSDriver;

    public WorkListToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of WorkListPageIOS class.
     *
     * @return Instance of page object which models WorkListPageIOS
     */
    public WorkListPageIOS worklistListPage() {
        return PageFactory.initElements(iOSDriver, WorkListPageIOS.class);
    }

    /**
     * Tap on Worklist in WorkList Page
     */
    public void tapOnWorkListIcon() {
        worklistListPage().tapOnWorkListIcon();
    }

    /**
     * Tap on Not Submitted tab in WorkList Page
     */
    public void clickOnNotSubmittedTab() {
        worklistListPage().clickOnTab("Not submitted");
    }

    /**
     * Tap on Submitted tab in WorkList Page
     */
    public void clickOnSubmittedTab() {
        worklistListPage().clickOnTab("Submitted");
    }

    /**
     * Tap on Evaluated tab in WorkList Page
     */
    public void clickOnEvaluatedTab() {
        worklistListPage().clickOnTab("Evaluated");
    }

    /**
     * Tap on assignment heading
     *
     * @param assignmentName Name of the assignment
     */
    public void openAssignment(String assignmentName) {
        worklistListPage().clickOnAssignmentHeading(assignmentName);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        worklistListPage().navigateBack();
    }
}
