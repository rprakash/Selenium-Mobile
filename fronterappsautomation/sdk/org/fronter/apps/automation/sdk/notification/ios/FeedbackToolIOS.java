package org.fronter.apps.automation.sdk.notification.ios;

import org.fronter.apps.automation.sdk.notification.pages.ios.FeedbackPageIOS;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

/**
 * Feedback Tool Page Class
 */
public class FeedbackToolIOS {
    private IOSDriver iOSDriver;

    public FeedbackToolIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Returns an instance of FeedbackPageIOS class.
     *
     * @return Instance of page object which models FeedbackPageIOS
     */
    public FeedbackPageIOS feedbackPageIOS() {
        return PageFactory.initElements(iOSDriver, FeedbackPageIOS.class);
    }

    /**
     * Enter sender email and feedback in the feedback page
     *
     * @param senderEmail email address of the sender
     * @param feedbackText feedback to be send
     */
    public void inputFeedback(String senderEmail, String feedbackText) {
        feedbackPageIOS().inputFeedback(senderEmail, feedbackText);
    }

    /**
     * Click on send feedback button
     */
    public void sendFeedback() {
        feedbackPageIOS().sendFeedback();
    }

    /**
     * Click on cancel button in confirmation pop up
     */
    public void cancelFeedback() {
        feedbackPageIOS().cancelFeedback();
    }

    /**
     * Click on Ok button in error pop up
     */
    public void clickOkButton() {
        feedbackPageIOS().clickOkButton();
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        feedbackPageIOS().navigateBack();
    }
}
