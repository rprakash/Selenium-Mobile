package org.fronter.apps.automation.sdk.notification.pages.android;

import org.fronter.apps.automation.sdk.AndroidWait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.android.AndroidDriver;

/**
 * Notification List Page Class
 */
public class NotificationListPageAndroid {
    private AndroidDriver androidDriver;
    private AndroidWait wait;

    public NotificationListPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
        wait = new AndroidWait(androidDriver);
    }

    /**
     * Tap on the Notification
     *
     * @param notificationHeading Notification heading
     */
    public void openNotification(String notificationHeading) {
        androidDriver.findElementByName(notificationHeading).click();
    }

    /**
     * Verify Test Notification exists on Notification List page
     *
     * @param testNotification name of test notification
     * @param roomName name of the room
     * @return true if Test notification is present else false
     */
    public boolean displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + testNotification
                                    + "')]/following-sibling::android.widget.TextView[1]")).getText().trim()
                    .equalsIgnoreCase("In " + roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Assignment Notification exists on Notification List page
     *
     * @param assignmentNotification name of the notification
     * @param roomName name of the Room
     * @return true if AssignmentNotification present else false
     */
    public boolean displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        try {
            return androidDriver
                    .findElement(
                            By.xpath("//*[contains(@text,'" + assignmentNotification
                                    + "')]/following-sibling::android.widget.TextView[1]")).getText().trim()
                    .equalsIgnoreCase("In " + roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Empty Notification list has 'No notification' message
     *
     * @return true if No notification message present else false
     */
    public boolean hasEmptyNotification() {
        wait.untilElementIsVisible(By.name("No notification"));
        try {
            return androidDriver.findElementByName("No notification").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Notification exists on Notification List page
     *
     * @param newsHeading name of the News Notification
     * @param roomName name of the Room
     * @return true if newsHeading present else false
     */
    public boolean displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        try {
            return androidDriver.findElement(
                    By.xpath("//*[contains(@text,'" + newsHeading + "')]/..//*[contains(@text,'" + roomName + "')]"))
                    .isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify 'No Due date' message exists on Notification View page
     *
     * @return true if 'No Due date' message exists on Notification View page, else false
     */
    public boolean hasNoDueDate() {
        try {
            return androidDriver.findElementById("com.fronter.android:id/detailLine").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify 'Today' is displayed in List header
     *
     * @return true if 'Today' is displayed in List header
     */
    public boolean displaysHeaderAsToday() {
        try {
            WebElement listHeader = androidDriver.findElementById("com.fronter.android:id/list_item_notification_section_header");
            return listHeader.getText().equalsIgnoreCase("Today");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that notification icon is displayed in list
     *
     * @return true if notification icon is displayed in list
     */
    public boolean hasNotificationIcon() {
        try {
            return androidDriver.findElementById("com.fronter.android:id/list_item_notification_icon").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
