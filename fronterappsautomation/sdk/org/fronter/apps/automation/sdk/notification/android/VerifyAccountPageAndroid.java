package org.fronter.apps.automation.sdk.notification.android;

import org.fronter.apps.automation.sdk.notification.pages.android.AccountPageAndroid;
import org.fronter.apps.automation.sdk.notification.pages.android.HeaderPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Verify Account Page Class
 */
public class VerifyAccountPageAndroid {
    AndroidDriver androidDriver;

    public VerifyAccountPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Verify First name and Last name exists on Account page
     *
     * @param firstName First Name of the student
     * @param lastName Last Name of the student
     */
    public void hasFirstAndLastname(String firstName, String lastName) {
        Verify.checkPoint(accountPageAndroid().hasFirstAndLastname(firstName, lastName),
                "First name and Last name exists on Account page");
    }

    /**
     * Returns an instance of NotificationAccountPageAndroid class.
     *
     * @return Instance of page object which models NotificationAccountPageAndroid
     */
    public AccountPageAndroid accountPageAndroid() {
        return PageFactory.initElements(androidDriver, AccountPageAndroid.class);
    }

    /**
     * Verify institution list names
     *
     * @param institutionList String array of institutions names
     */
    public void hasInstitutionList(String[] institutionList) {
        Verify.checkPoint(accountPageAndroid().hasInstitutionList(institutionList), "Institution Names are displayed");
    }

    /**
     * Verify institution name
     *
     * @param institutionName Name of the institution to be verified
     */
    public void hasInstitutionName(String institutionName) {
        Verify.checkPoint(accountPageAndroid().hasInstitutionName(institutionName), "Institution name is displayed");
    }

    /**
     * Returns an instance of HeaderPageAndroid class.
     *
     * @return Instance of page object which models HeaderPageAndroid
     */
    public HeaderPageAndroid headerPageAndroid(){
        return PageFactory.initElements(androidDriver, HeaderPageAndroid.class);
    }

    /**
     * Verify logout confirmation message
     *
     * @param message message to be verified
     */
    public void hasLogoutConfirmationMsg(String message) {
        Verify.checkPoint(accountPageAndroid().hasConfirmationMessage(message),
                "Logout confirmation message '" + message + "' is displaye");

    }

    /**
     * Verify Fronter logo is displayed
     */
    public void hasFronterLogo() {
        Verify.checkPoint(headerPageAndroid().hasFronterLogo(), "Fronter logo is displayed");
    }
}
