package org.fronter.apps.automation.sdk.notification.worklist.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.WebDriver;

/**
 * Assignments Evaluation Page Class
 */
public class AssignmentSubmittedPageAndroid {
    private AndroidDriver androidDriver;

    public AssignmentSubmittedPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }


    // Sample method
    /**
     * Verify given tab is displayed on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is displayed
     */
    public boolean hasTab(String tabName) {
        try {
            return androidDriver.findElementByName(tabName).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
