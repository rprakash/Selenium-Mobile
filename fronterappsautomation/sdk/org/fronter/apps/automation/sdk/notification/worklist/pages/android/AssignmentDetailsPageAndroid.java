package org.fronter.apps.automation.sdk.notification.worklist.pages.android;
import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Assignments Details Page Class
 */
public class AssignmentDetailsPageAndroid {
    private AndroidDriver androidDriver;

    public AssignmentDetailsPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

     /**
     * Verify that assignment with correct date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param dueDate Due date of assignment
     * @return true if assignment with correct date and Room name is displayed
     */
    public boolean hasAssignmentWithDateAndRoomName(String assignmentName, String roomName, String dueDate) {
        try {
            String assignmentTitle = androidDriver.findElement(By.id("com.fronter.android:id/assignment_title"))
                    .getText().trim();
            String displayedRoom = androidDriver.findElement(By.id("com.fronter.android:id/assignment_room_title"))
                    .getText().trim();
            String displayedDate = androidDriver.findElement(By.id("com.fronter.android:id/assignment_due_date"))
                    .getText().trim();
            return (assignmentTitle.equals(assignmentName) && displayedRoom.contains(roomName)
                    && displayedDate.contains(dueDate));
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * Verify that description is displayed
     *
     * @param descriptionTxt text of the description
     */
    public boolean hasDescription(String descriptionTxt) {
        try{
            WebElement description = androidDriver.findElement(By.xpath("//*[contains(@text,'Description')]/.."));
            String descText = description.findElement(By.id("com.fronter.android:id/assignment_description")).getText().trim();
            return descText.equals(descriptionTxt);
        }catch(Exception e){
            return false;
        }
    }

    /**
     * Verifies that specified Icon with text is displayed
     *
     * @return true if specified Icon with text is displayed
     */
    public boolean displaysIconWithText(String iconText, String iconDes) {
        try {
            boolean iconDisplayed = androidDriver.findElementByAccessibilityId(iconDes).isDisplayed();

            boolean iconTextDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + iconText + "')]")).isDisplayed();
            return iconDisplayed && iconTextDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verifies that uploaded file is displayed
     *
     * @param file file uploaded
     * @return true if file is displayed otherwise false
     */
    public boolean hasFile(String file) {
        try {
            boolean uploadedFile = androidDriver.findElement(By.xpath("//*[contains(@text,'" + file + "')]")).isDisplayed();
            return uploadedFile;
        } catch (Exception e) {
            return false;
        }
    }
}
