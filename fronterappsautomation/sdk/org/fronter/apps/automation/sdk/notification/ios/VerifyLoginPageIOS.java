package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.LoginPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify Login Page Class
 */
public class VerifyLoginPageIOS {
    IOSDriver iOSDriver;

    public VerifyLoginPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of LoginPageIOS class.
     *
     * @return Instance of page object which models LoginPageIOS
     */
    public LoginPageIOS loginPageIOS() {
        return PageFactory.initElements(iOSDriver, LoginPageIOS.class);
    }

    /**
     *
     * Verify 'Wrong username or password!' Message appears when user entered invalid user credentials
     *
     * @param errorMessage error Mesage
     */
    public void hasMessageDisplayed(String errorMessage) {
        Verify.checkPoint(loginPageIOS().hasMessageDisplayed(errorMessage), "Error message exists on Login page");
    }
}
