package org.fronter.apps.automation.sdk.notification.android;

import org.fronter.apps.automation.sdk.TestData;
import org.fronter.apps.automation.sdk.notification.pages.android.LoginPageAndroid;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Notification Login Tool Page Class
 */
public class LoginToolAndroid {
    private AndroidDriver androidDriver;
    protected TestData testData = new TestData();
    protected String installation = testData.installation1;

    public LoginToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of LoginPageAndroid class.
     *
     * @return Instance of page object which models LoginPageAndroid
     */
    public LoginPageAndroid loginPageAndroid() {
        return PageFactory.initElements(androidDriver, LoginPageAndroid.class);
    }

    /**
     * Enter Installation Name
     *
     * @param installationName name of the school url
     */
    public void enterInstallationName(String installationName) {
        loginPageAndroid().enterInstallationName(installationName);
    }

    /**
     * Enter Login Credentials of user
     *
     * @param userName userId of the user
     * @param password password of the user
     */
    public void login(String userName, String password) {
        loginPageAndroid().login(userName, password);
    }

    /**
     * Selects Installation and login
     *
     * @param installationName Installation Name
     * @param userName Username
     * @param password Password
     */
    public void login(String installationName, String userName, String password) {
        loginPageAndroid().login(installationName, userName, password);
    }

    /**
     * Login as Student 1
     */
    public void loginAsStudent1() {
        loginPageAndroid().login(installation, testData.student1UserName, testData.student1Password);
    }

    /**
     * Login as Student 2
     */
    public void loginAsStudent2() {
        loginPageAndroid().login(installation, testData.student2UserNameWithNoData, testData.student2Password);
    }

    /**
     * Login with invalid credentials
     */
    public void loginWithInvalidCrendentials() {
        loginPageAndroid().login(installation, testData.invalidUserName, testData.invalidPassword);
    }
}
