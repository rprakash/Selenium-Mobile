package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentEvaluationPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class VerifyAssignmentEvaluationPageAndroid {
    private AndroidDriver androidDriver;

    public VerifyAssignmentEvaluationPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Returns an instance of AssignmentEvaluationPageAndroid class.
     *
     * @return Instance of page object which models AssignmentEvaluationPageAndroid
     */
    public AssignmentEvaluationPageAndroid assignmentEvaluationPageAndroid(){
        return PageFactory.initElements(androidDriver, AssignmentEvaluationPageAndroid.class);
    }

    /**
     * Verify grade, comment and date of evaluated assignment is displayed
     *
     * @param grade Evaluation grade
     * @param evaluationComment Evaluation comment
     * @param evaluationDate Evaluation date
     */
    public void displaysGradeCommentAndDate(String grade, String evaluationComment, String evaluationDate) {
        Verify.checkPoint(
                assignmentEvaluationPageAndroid().displaysGradeCommentAndDate(grade, evaluationComment, evaluationDate),
                "Evaluated assignment grade '" + grade + "', comment '" + evaluationComment + "' and date '" + evaluationDate
                        + "' is displayed on Evaluation Page");
    }

    /**
     * Verify informatory message
     *
     * @param message message to be verified
     */
    public void hasInformatoryMessage(String message) {
        Verify.checkPoint(assignmentEvaluationPageAndroid().hasMessage(message), "Correct message '" + message + "' is displayed on Evaluation Page");
    }
}
