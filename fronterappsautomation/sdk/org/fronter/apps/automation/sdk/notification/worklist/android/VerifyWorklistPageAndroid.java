package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.WorklistPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Verify Account Page Class
 */
public class VerifyWorklistPageAndroid {
    AndroidDriver androidDriver;

    public VerifyWorklistPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Verify Not Submitted tab is displayed on worklist Page
     */
    public void hasNotSubmittedTab() {
        Verify.checkPoint(
                worklistPageAndroid().hasTab("Not submitted"),
                "Not Submitted tab is displayed on Worklist Page"
            );
    }

    /**
     * Verify Submitted tab is displayed on worklist Page
     */
    public void hasSubmittedTab() {
        Verify.checkPoint(
                worklistPageAndroid().hasTab("Submitted"),
                "Submitted tab is displayed on Worklist Page"
            );
    }

    /**
     * Verify Evaluated tab is displayed on worklist Page
     */
    public void hasEvaluatedTab() {
        Verify.checkPoint(
                worklistPageAndroid().hasTab("Evaluated"),
                "Not Submitted tab is displayed on Worklist Page"
            );
    }

    /**
     * Verify that assignment is displayed in worklist
     *
     * @param assignmentName Name of assignment
     * @param tabName Name of tab i.e. 'Not Submitted', 'Submitted', or 'Evaluated'
     */
    private void hasAssignment(String assignmentName, String tabName) {
        Verify.checkPoint(
                worklistPageAndroid().hasAssignment(assignmentName),
                "Assignment '" + assignmentName + "' is displayed under '" + tabName + "' tab"
            );
    }

    /**
     * Verify that assignment is displayed in worklist under Not Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderNotSubmittedTab(String assignmentName) {
        hasAssignment(assignmentName, "Not Submitted");
    }

    /**
     * Verify that assignment is displayed in worklist under Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderSubmittedTab(String assignmentName) {
        hasAssignment(assignmentName, "Submitted");
    }

    /**
     * Verify that assignment is displayed in worklist under Evaluated tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderEvaluatedTab(String assignmentName) {
        hasAssignment(assignmentName, "Evaluated");
    }

    /**
     * Verify that assignment is not displayed in worklist
     *
     * @param assignmentName Name of assignment
     */
    private void doesNotHaveAssignment(String assignmentName, String tabName) {
        Verify.checkPoint(
                !worklistPageAndroid().hasAssignment(assignmentName),
                "Assignment '" + assignmentName + "' is not displayed under '" + tabName + "' tab"
            );
    }

    /**
     * Verify that assignment is not displayed in worklist under Not Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderNotSubmittedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Not Submitted");
    }

    /**
     * Verify that assignment is not displayed in worklist under Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderSubmittedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Submitted");
    }

    /**
     * Verify that assignment is not displayed in worklist under Evaluated tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderEvaluatedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Evaluated");
    }

    /**
     * Verify that assignment with correct due date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param dueDate Due Date of assignment
     */
    public void hasAssignmentWithDueDateAndRoomName(String assignmentName, String roomName, String dueDate) {
        Verify.checkPoint(
                worklistPageAndroid().hasAssignmentWithDateAndRoomName(assignmentName, roomName, dueDate, "Due"),
                "Correct room name '" + roomName + "' and due date '" + dueDate + "' is "
                 + "displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that assignment with correct submitted date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param submittedDate Submitted Date of assignment
     */
    public void hasAssignmentWithSubmittedDateAndRoomName(String assignmentName, String roomName, String submittedDate) {
        Verify.checkPoint(
                worklistPageAndroid().hasAssignmentWithDateAndRoomName(assignmentName, roomName, submittedDate, "Submitted"),
                "Correct room name '" + roomName + "' and submitted date '" + submittedDate + "' is "
                 + "displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that number of due days left for assignment is displayed
     *
     * @param assignmentName Name of assignment
     * @param dueDaysLeft Due days left
     */
    public void displaysDueDaysLeft(String assignmentName, String dueDaysLeft) {
        Verify.checkPoint(
                worklistPageAndroid().displaysDueDaysLeft(assignmentName, dueDaysLeft),
                "Correct number of due day(s) left i.e. '" + dueDaysLeft + "' is displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that due at time is displayed for assignment
     *
     * @param assignmentName Name of assignment
     * @param dueAtTime Due at time
     */
    public void displaysDueAtTime(String assignmentName, String dueAtTime) {
        Verify.checkPoint(
                worklistPageAndroid().displaysDueAtTime(assignmentName, dueAtTime),
                "Correct due at time i.e. '" + dueAtTime + "' is displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that image icon for assignment is displayed
     *
     * @param assignmentName Name of assignment
     */
    public void displaysIconforAssignment(String assignmentName) {
        Verify.checkPoint(
                worklistPageAndroid().displaysIconforAssignment(assignmentName),
                "Image icon is displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Returns an instance of WorklistPageAndroid class.
     *
     * @return Instance of page object which models WorklistPageAndroid
     */
    public WorklistPageAndroid worklistPageAndroid() {
        return PageFactory.initElements(androidDriver, WorklistPageAndroid.class);
    }

    /**
     * Verify that assignment with correct evaluated date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param evaluatedDate Evaluated Date of assignment
     */
    public void hasAssignmentWithEvalDateAndRoomName(String assignmentName, String roomName, String evaluatedDate) {
        Verify.checkPoint(
                worklistPageAndroid().hasAssignmentWithDateAndRoomName(assignmentName, roomName, evaluatedDate, "Evaluated"),
                "Correct room name '" + roomName + "' and submitted date '" + evaluatedDate + "' is "
                 + "displayed for assignment '" + assignmentName + "'"
            );
    }

    /**
     * Verify that due days field is not displayed for assignment
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotDisplayDueDaysField(String assignmentName) {
        Verify.checkPoint(!worklistPageAndroid().displaysDueDaysField(assignmentName),
                "No due date is displayed for assignment '" + assignmentName + "'");
    }

    /**
     * Verify that proper message is displayed
     *
     * @param message message to be verify
     */
    public void hasMessage(String message) {
        Verify.checkPoint(worklistPageAndroid().hasMessage(message),
                "'" + message + "' message is displayed at the bottom of the list of Assignment");
    }
}
