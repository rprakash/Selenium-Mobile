package org.fronter.apps.automation.sdk.notification.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification Account Page Class
 */
public class AccountPageAndroid {
    private AndroidDriver androidDriver;

    public AccountPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Verify First name and Last name exists on Account page
     *
     * @param firstName First Name of the student
     * @param lastName Last Name of the student
     * @return true if firstName and lastName present else false
     */
    public boolean hasFirstAndLastname(String firstName, String lastName) {
        try {
            return androidDriver.findElementById("com.fronter.android:id/logged_user_info_text").getText().trim()
                    .equalsIgnoreCase(firstName + " " + lastName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on Logout in Account Page
     */
    public void logout() {
        clickOnLogout();
        clickOnContinueBtn();
    }

    /**
     * Tap on Continue button for confirm logout
     */
    private void clickOnContinueBtn() {
        androidDriver.findElement(By.id("android:id/button1")).click();
    }

    /**
     * Tap on News in Account Page
     */
    public void clickOnNews() {
        androidDriver.findElementByName("News").click();
    }

    /**
     * Tap on Worklist in Account Page
     */
    public void clickOnWorklist() {
        androidDriver.findElementByName("Worklist").click();
    }

    /**
     * Tap on Give Feedback in Account Page
     */
    public void clickOnGiveFeedback() {
        androidDriver.findElementByName("Give Feedback").click();
    }

    /**
     * Tap on Institution list drop down
     */
    public void clickOnInstitutionListDropdown() {
        androidDriver.findElementById("com.fronter.android:id/institution_selector").click();
    }

    /**
     * Verify institutions name
     *
     * @param institutionList String array of institutions name
     * @return true if institutions list are displayed otherwise false
     */
    public boolean hasInstitutionList(String[] institutionList) {
        try {
            for (int i = 0; i < institutionList.length; i++) {
                if (!androidDriver.findElementByName(institutionList[i]).isDisplayed()) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Tap on institution name from drop down
     *
     * @param institutionName Name of the institution
     */
    public void clickOnInstitutionName(String institutionName) {
        androidDriver.findElementByName(institutionName).click();
    }

    /**
     * Verify institution name
     *
     * @param institutionName Name of the institution to be verified
     * @return true if name is displayed otherwise false
     */
    public boolean hasInstitutionName(String institutionName) {
        try {
            return androidDriver.findElementByName(institutionName).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on logout button
     */
    public void clickOnLogout() {
        androidDriver.findElementByName("Log out").click();
    }

    /**
     * Verify logout confirmation message
     *
     * @param message message to be verified
     * @return true if message is displayed otherwise false
     */
    public boolean hasConfirmationMessage(String message) {
        try {
            return androidDriver.findElementByName(message).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on Cancel button of logout pop-up
     */
    public void clickOnCancelButton() {
        androidDriver.findElementByName("Cancel").click();
    }
}
