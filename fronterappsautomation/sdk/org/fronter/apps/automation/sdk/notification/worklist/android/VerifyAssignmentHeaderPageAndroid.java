package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentHeaderPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class VerifyAssignmentHeaderPageAndroid {

    AndroidDriver androidDriver;

    public VerifyAssignmentHeaderPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of AssignmentDetailsPageAndroid class.
     *
     * @return Instance of page object which models AssignmentDetailsPageAndroid
     */
    public AssignmentHeaderPageAndroid assignmentHeaderPageAndroid() {
        return PageFactory.initElements(androidDriver, AssignmentHeaderPageAndroid.class);
    }

    /**
     * Verify that specified tab is displayed
     *
     * @param tabName Name of tab
     */
    public void hasTab(String tabName) {
        Verify.checkPoint(assignmentHeaderPageAndroid().hasTab(tabName), "'" + tabName + "' tab is displayed");
    }

    /**
     * Verify that specified tab is not displayed
     *
     * @param tabName Name of tab
     */
    public void doesNotHaveTab(String tabName) {
        Verify.checkPoint(!assignmentHeaderPageAndroid().hasTab(tabName), "'" + tabName + "' tab is not displayed");
    }

    /**
     * Verify that Details tab is displayed
     */
    public void hasDetailsTab() {
        hasTab("Details");
    }

    /**
     * Verify that Submission tab is displayed
     */
    public void hasSubmissionTab() {
        hasTab("Submission");
    }

    /**
     * Verify that Evaluation tab is displayed
     */
    public void hasEvaluationTab() {
        hasTab("Evaluation");
    }

    /**
     * Verify that Details tab is displayed
     */
    public void doesNotHaveDetailsTab() {
        doesNotHaveTab("Details");
    }

    /**
     * Verify that Submission tab is displayed
     */
    public void doesNotHaveSubmissionTab() {
        doesNotHaveTab("Submission");
    }

    /**
     * Verify that Evaluation tab is displayed
     */
    public void doesNotHaveEvaluationTab() {
        doesNotHaveTab("Evaluation");
    }

    /**
     * Verify that assignment title with status is displayed at top
     *
     * @param assignmentName Name of the assignment
     */
    private void displaysAsignmentTitleWithStatus(String assignmentName, String assignmentStatus) {
        Verify.checkPoint(assignmentHeaderPageAndroid().hasAssignmentTitleWithStatus(assignmentName, assignmentStatus),
                "Assignment title with name '"+assignmentName+"' and status '"+assignmentStatus+"' is display at the top");
    }

    /**
     * Verify that assignment title with Not submitted status is displayed at top
     *
     * @param assignmentName Name of the assignment
     */
    public void displaysAssignmentTitleWithNotSubStatus(String assignmentName) {
        displaysAsignmentTitleWithStatus(assignmentName, "Not submitted");
    }

    /**
     * Verify that assignment title with submitted status is displayed at top
     *
     * @param assignmentName Name of the assignment
     */
    public void hasAssignmentTitleWithSubmittedStatus(String assignmentName) {
        displaysAsignmentTitleWithStatus(assignmentName, "Submitted");
    }

    /**
     * Verify that assignment title with Evaluated status is displayed at top
     *
     * @param assignmentName Name of the assignment
     */
    public void hasAssignmentTitleWithEvaluatedStatus(String assignmentName) {
        displaysAsignmentTitleWithStatus(assignmentName, "Evaluated");
    }
}
