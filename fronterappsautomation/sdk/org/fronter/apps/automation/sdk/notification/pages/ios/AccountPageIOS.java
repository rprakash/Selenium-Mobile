package org.fronter.apps.automation.sdk.notification.pages.ios;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.ios.IOSDriver;

/**
 * Notification Account Page Class
 */
public class AccountPageIOS {
    private IOSDriver iOSDriver;

    public AccountPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    String userName = " //UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]";

    /**
     * Verify First name and Last name exists on Account page
     *
     * @param firstName First Name of the student
     * @param lastName Last Name of the student
     * @return true if firstName and lastName present else false
     */
    public boolean hasFirstAndLastname(String firstName, String lastName) {
        try {
            return iOSDriver.findElementByXPath(userName).getText().equalsIgnoreCase(firstName + " " + lastName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that all the institutions exists on the Account page
     *
     * @param institutionName Name of the institution
     * @return true if all the institution names are displayed on the account page, else false
     */
    public boolean hasInstitutionName(String[] institutionName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            for (int i = 0; i < institutionName.length; i++) {
                if (!iOSDriver.findElementByXPath("//*[contains(@name,'" + institutionName[i] + "')]").isDisplayed()) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Verify that confirmation pop up is displayed on accounts page
     *
     * @return true if confirmation pop up is displayed on account page, else false
     */
    public boolean displaysInstitutionChangeConfirmation() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean changeBoxTitle =
                    iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Change institution')]").isDisplayed();
            boolean changeBoxMessage = iOSDriver
                    .findElementByXPath("//UIAStaticText[contains(@name,'Are you sure you want to change to')]").isDisplayed();
            boolean cancelButton = iOSDriver.findElementByXPath("//UIAButton[contains(@name,'Cancel')]").isDisplayed();
            boolean changeButton = iOSDriver.findElementByXPath("//UIAButton[contains(@name,'Change')]").isDisplayed();
            return changeBoxTitle && changeBoxMessage && cancelButton && changeButton;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Tap on Account icon
     */
    public void tapOnAccountIcon() {
        iOSDriver.findElementByName("Account").click();
    }

    /**
     * Tap on Logout in Account Page
     */
    public void logout() {
        iOSDriver.findElementByName("Log out").click();
    }

    /**
     * Tap on a institution name
     *
     * @param institutionName Name of the institution
     */
    public void tapOnInstitutionName(String institutionName) {
        iOSDriver.findElementByXPath("//*[contains(@name,'" + institutionName + "')]").click();
    }

    /**
     * Tap on change button in change institution confirmation pop up
     */
    public void changeInstitution(){
        iOSDriver.findElementByXPath("//UIAButton[contains(@name,'Change')]").click();
    }

    /**
     * Tap on Send Feedback link
     */
    public void tapOnFeedbackLink(){
        iOSDriver.findElementByXPath("//UIAStaticText[contains(@name,'Send us your feedback')]").click();
    }
}
