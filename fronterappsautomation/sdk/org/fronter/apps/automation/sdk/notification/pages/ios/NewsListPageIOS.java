package org.fronter.apps.automation.sdk.notification.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Notification News List Page Class
 */
public class NewsListPageIOS {
    private IOSDriver iOSDriver;

    public NewsListPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    // Mobile Elements identifiers
    String accountLable = "//UIAApplication[1]/UIAWindow[1]/UIATabBar[1]/UIAButton[4]";
    String newsTitle = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]";
    String room = "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[2]";

    /**
     * Open account page after login
     */
    public void openAccount() {
        WebDriverWait wait = new WebDriverWait(iOSDriver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(accountLable)));
        iOSDriver.findElementByXPath(accountLable).click();
    }

    /**
     * Verify Latest News exists on News List page
     *
     * @param newsHeading Heading of the News
     * @return true if News Heading present else false
     */
    public boolean hasLatestNewsHeading(String newsHeading) {
        WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsTitle)));
        try {
            return iOSDriver.findElementByXPath(newsTitle).getText().trim().equalsIgnoreCase(newsHeading);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Room Name exists on News List page
     *
     * @param newsHeading News heading
     * @param roomName Name of the Room
     * @return true if Room Name present else false
     */
    public boolean displaysNewsWithRoomName(String newsHeading, String roomName) {
        try {
            return iOSDriver.findElement(By.xpath("//*[@name='" + newsHeading + "']/following-sibling::*/following-sibling::*"))
                    .getText().trim().equalsIgnoreCase(roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on the Latest News
     *
     * @param newsTitle Heading of the news
     */
    public void openNews(String newsTitle) {
        iOSDriver.findElementByName(newsTitle).click();
    }

    /**
     * Tap on Notification Icon
     */
    public void tapOnNotificationIcon() {
        iOSDriver.findElementByName("Notifications").click();
    }

    /**
     * Verify correct message exists at bottom of the page
     *
     * @param message Message at bottom of the page
     * @return true if No more news message exists else false
     */
    public boolean displaysMessage(String message) {
        try {
            return iOSDriver.findElement(By.xpath("//*[@name='" + message + "']")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Scroll to Bottom of the page on News List
     */
    public void scrollToBottom() {
        List<WebElement> newsDates = iOSDriver.findElementsByXPath("//*[contains(@name,'-2')]");
        iOSDriver.scrollTo(newsDates.get(newsDates.size() - 1).getText());
    }

    /**
     * Verify News Heading with date exists on News List page
     *
     * @param newsHeading Heading of the News
     * @param presentDate date of the News
     * @return true if date present else false
     */
    public boolean hasNewsWithDate(String newsHeading, String presentDate) {
        try {
            return iOSDriver.findElement(By.xpath("//*[@name='" + newsHeading + "']//following-sibling::UIAStaticText[4]"))
                    .getText().trim().equalsIgnoreCase(presentDate);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with today label exists on News List page
     *
     * @param newsHeading Heading of the News
     * @return true if today label present else false
     */
    public boolean hasTodayLabel(String newsHeading) {
        try {
            return iOSDriver
                    .findElement(By.xpath("//*[contains(@name,'" + newsHeading + "')]/../..//UIATableGroup[1]/UIAStaticText[1]"))
                    .getText().trim().equalsIgnoreCase("Today");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading with yesterday label exists on News List page
     *
     * @param newsHeading Heading of the News
     * @return true if yesterday label present else false
     */
    public boolean hasYesterdayLabel(String newsHeading) {
        try {
            return iOSDriver
                    .findElement(By.xpath("//*[contains(@name,'" + newsHeading + "')]/../..//UIATableGroup[2]/UIAStaticText[1]"))
                    .getText().trim().equalsIgnoreCase("Yesterday");
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify that 'No News' message exists on news List page
     *
     * @return true if No News message exists on news list page, else false
     */
    public boolean displaysNoNewsMessage() {
        try {
            return iOSDriver.findElementByXPath("//*[contains(@name,'No news')]").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }
}
