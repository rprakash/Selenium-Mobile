package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NotificationViewPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification View Tool Page Class
 */
public class NotificationViewToolIOS {
    private IOSDriver iOSDriver;

    public NotificationViewToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NotificationViewPageIOS class.
     *
     * @return Instance of page object which models NotificationViewPageIOS
     */
    public NotificationViewPageIOS notificationViewPageIOS() {
        return PageFactory.initElements(iOSDriver, NotificationViewPageIOS.class);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        notificationViewPageIOS().navigateBack();
    }

    /**
     * Tap on the Image
     */
    public void tapOnImage() {
        notificationViewPageIOS().tapOnImage();
    }

    /**
     * Scroll to Bottom of the page on Notification List
     *
     */
    public void scrollToBottom() {
        notificationViewPageIOS().scrollToBottom();
    }
}
