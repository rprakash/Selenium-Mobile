package org.fronter.apps.automation.sdk.notification.pages.android;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.android.AndroidDriver;

public class HeaderPageAndroid {
    private AndroidDriver androidDriver;

    public HeaderPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Verify fronter logo is displayed
     *
     * @return true if fronter logo is displayed otherwise false
     */
    public boolean hasFronterLogo() {
        try {
            return androidDriver.findElementById("com.fronter.android:id/toolbar_logo").isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Open account page after login
     */
    public void openAccount() {
        androidDriver.findElementByAndroidUIAutomator("new UiSelector().description(\"Open menu\")").click();
    }

    /**
     * Tap on Notification Counter
     */
    public void tapOnNotificationCounter() {
        androidDriver.findElementByAccessibilityId("Notifications").click();
    }
}
