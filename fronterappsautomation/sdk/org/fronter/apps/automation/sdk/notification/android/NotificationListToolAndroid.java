package org.fronter.apps.automation.sdk.notification.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.notification.pages.android.NotificationListPageAndroid;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification List Tool Page Class
 */
public class NotificationListToolAndroid {
    private AndroidDriver androidDriver;

    public NotificationListToolAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationAndroidListPage class.
     *
     * @return Instance of page object which models NotificationAndroidListPage
     */
    public NotificationListPageAndroid notificationListPageAndroid() {
        return PageFactory.initElements(androidDriver, NotificationListPageAndroid.class);
    }

    /**
     * Tap on the Notification
     *
     * @param notification Notification heading
     */
    public void openNotification(String notification) {
        notificationListPageAndroid().openNotification(notification);
    }
}
