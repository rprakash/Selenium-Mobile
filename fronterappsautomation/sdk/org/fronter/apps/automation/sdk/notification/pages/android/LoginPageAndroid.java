package org.fronter.apps.automation.sdk.notification.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.fronter.apps.automation.sdk.AndroidWait;
import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification Login Page Class
 */
public class LoginPageAndroid {
    AndroidDriver androidDriver;
    AndroidWait wait;

    // Mobile Elements identifiers
    String installer = "new UiSelector().description(\"Search for your Fronter building\")";
    String installerName = "//android.widget.EditText[1]";
    String userIdTestBox = "//android.widget.EditText[1]";
    String passwdTestBox = "//android.widget.EditText[2]";

    public LoginPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
        wait = new AndroidWait(androidDriver);
    }

    /**
     * Enter Installation Name
     *
     * @param installationName name of the url
     */
    public void enterInstallationName(String installationName) {
        wait.untilElementIsVisible(By.xpath(installerName));
        androidDriver.findElementByAndroidUIAutomator(installer).click();
        androidDriver.findElementByAndroidUIAutomator(installer).sendKeys(installationName);
        androidDriver.hideKeyboard();
        androidDriver.findElement(
                By.xpath("//*[contains(@class,'android.view.View') and contains(@content-desc,'" + installationName + "')]"))
                .click();
    }

    /**
     * Select Login Provider
     *
     * @param loginProvider Login Provider
     */
    public void selectLoginProvider(String loginProvider) {
        androidDriver.findElementByAccessibilityId(loginProvider).click();
    }

    /**
     * Enter Login Credentials of Students
     *
     * @param userName userId of the student
     * @param password password of the student
     */
    public void login(String userName, String password) {
        androidDriver.findElementByAccessibilityId("Username").click();
        androidDriver.findElement(By.xpath(userIdTestBox)).sendKeys(userName);
        androidDriver.hideKeyboard();
        androidDriver.findElementByAccessibilityId("Password").click();
        androidDriver.findElement(By.xpath(passwdTestBox)).sendKeys(password);;
        androidDriver.hideKeyboard();
        androidDriver.findElementByClassName("android.widget.Button").click();
    }


    /**
     * Verify 'Wrong username or password!' Message appears when user entered invalid user credentials
     *
     * @param errorMessage error Message
     * @return true if 'Wrong username or password!' message appears else false
     */
    public boolean hasMessageDisplayed(String errorMessage) {
        try {
            return androidDriver.findElementByAccessibilityId(errorMessage).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Selects Installation and login
     *
     * @param installationName Installation Name
     * @param userName Username
     * @param password Password
     */
    public void login(String installationName, String userName, String password) {
        wait.untilElementIsVisible(By.xpath("//*[contains(@class,'Text')]"));
        if (isLoginPageDisplayed()) {
            enterInstallationName(installationName);
            selectLoginProvider("Fronter Login");
            login(userName, password);
        }
    }

    /**
     * Is login page displayed
     *
     * @return true if login page is displayed
     */
    public boolean isLoginPageDisplayed() {
        WebDriverFactory.getInstance().disableImplicitWaits(androidDriver);
        try {
            boolean installerDisplay =
                androidDriver.findElementByAndroidUIAutomator(installer).isDisplayed();
            return installerDisplay;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(androidDriver);
        }
    }
}
