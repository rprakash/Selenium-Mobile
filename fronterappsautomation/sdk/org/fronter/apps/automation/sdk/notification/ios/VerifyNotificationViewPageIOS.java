package org.fronter.apps.automation.sdk.notification.ios;

import org.fronter.apps.automation.sdk.notification.pages.ios.NotificationViewPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

/**
 * Verify Notification View Page Class
 */
public class VerifyNotificationViewPageIOS {
    IOSDriver iOSDriver;

    public VerifyNotificationViewPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NotificationViewPageIOS class.
     *
     * @return Instance of page object which models NotificationViewPageIOS
     */
    public NotificationViewPageIOS notificationViewPageIOS() {
        return PageFactory.initElements(iOSDriver, NotificationViewPageIOS.class);
    }

    /**
     * Verify Room Name exists on Notification View page
     *
     * @param roomName Name of the room
     */
    public void hasRoomName(String roomName) {
        Verify.checkPoint(notificationViewPageIOS().hasRoomName(roomName), "Room name exists on Notification Detalied View page");
    }

    /**
     * Verify 'No Due date' message exists on Notification View page
     */
    public void hasNoDueDateMsg() {
        Verify.checkPoint(notificationViewPageIOS().hasNoDueDateMsg(),
                "'No Due date'message exists on Notification Detailed View page");
    }

    /**
     * Verify Test Heading exists on Notification View page
     *
     * @param testNotification Notification of the test
     */
    public void hasTestNotification(String testNotification) {
        Verify.checkPoint(notificationViewPageIOS().hasTestNotification(testNotification),
                "Test Heading exists on Notification Detalied View page");
    }

    /**
     * Verify Test Description exists on Notification View page
     *
     * @param testDescription Description of the test
     */
    public void hasTestDescription(String testDescription) {
        Verify.checkPoint(notificationViewPageIOS().hasTestDescription(testDescription),
                "Test Description exists on Notification Detalied View page");
    }

    /**
     * Verify Assignment Heading exists on Notification View page
     *
     * @param assignmentNotification Notification of the Assignment
     */
    public void hasAssignmentNotification(String assignmentNotification) {
        Verify.checkPoint(notificationViewPageIOS().hasAssignmentNotification(assignmentNotification),
                "Assignment Heading exists on Notification Detalied View page");

    }

    /**
     * Verify Assignment Description exists on Notification View page
     *
     * @param assignmentDescription Description of the Assignment
     */
    public void hasAssignmentDescription(String assignmentDescription) {
        Verify.checkPoint(notificationViewPageIOS().hasAssignmentDescription(assignmentDescription),
                "Assignment Description exists on Notification Detalied View page");
    }

    /**
     * Verify News Heading exists on Notification View page
     *
     * @param newsHeading Notification of the News
     */
    public void hasNewsNotification(String newsHeading) {
        Verify.checkPoint(notificationViewPageIOS().hasNewsNotification(newsHeading),
                "News Heading exists on Notification Detalied View page");

    }

    /**
     * Verify News Description exists on Notification View page
     *
     * @param newsDescription Description of the News
     */
    public void hasNewsDescription(String newsDescription) {
        Verify.checkPoint(notificationViewPageIOS().hasNewsNotification(newsDescription),
                "News Description exists on Notification Detalied View page");

    }

    /**
     * Verify Full Image with Save Button exists on Notification View page
     */
    public void hasFullImageViewWithSaveButton() {
        Verify.checkPoint(notificationViewPageIOS().hasFullImageViewWithSaveButton(),
                "Full Image with Save Button exists on Notification Detalied View page");
    }

    /**
     * Verify Author First and Last Name exists on Notification View page
     *
     * @param teacher1FirstName first name of author
     * @param teacher1LastName last name of author
     */
    public void hasAuthorName(String teacher1FirstName, String teacher1LastName) {
        Verify.checkPoint(notificationViewPageIOS().hasAuthorName(teacher1FirstName, teacher1LastName), "Author First name '"
                + teacher1FirstName + "' and Last Name '" + teacher1LastName + "' exists on News Notification View page");
    }
}
