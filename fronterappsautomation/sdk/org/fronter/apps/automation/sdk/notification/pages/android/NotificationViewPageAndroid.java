package org.fronter.apps.automation.sdk.notification.pages.android;

import io.appium.java_client.android.AndroidDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Notification Notification detailed view Page Class
 */
public class NotificationViewPageAndroid {
    private AndroidDriver androidDriver;

    public NotificationViewPageAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Verify Test Notification exists on Notification detailed view page
     *
     * @param testNotification Name of the Test Notification
     * @param roomName Name of the Room
     * @return true if testNotification present else false
     */
    public boolean displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        try {
            boolean isTestNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + testNotification + "')]")).isDisplayed();
            boolean isRoomNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + roomName + "')]")).isDisplayed();

            return isTestNameDisplayed && isRoomNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify 'Open in Fronter' link exists on Notification detailed view page
     *
     * @return true if link present else false
     */
    public boolean displaysOpenInFronterLink() {
        try {
            return androidDriver.findElement(By.name("Open in Fronter")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Click Open In Fronter link
     */
    public void clickOpenInFronterLink() {
        androidDriver.findElement(By.name("Open in Fronter")).click();
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        androidDriver.findElementByAccessibilityId("Navigate up").click();
    }

    /**
     * Tap on the Back Button
     */
    public void clickBackButtonOnPhone() {
        androidDriver.navigate().back();
    }

    /**
     * Verify Assignment Notification exists on Notification detailed view page
     *
     * @param assignmentNotification Name of the Assignment Notification
     * @param roomName Name of the Room
     * @return true if assignmentNotification present else false
     */
    public boolean displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        try {
            boolean isAssignmentNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + assignmentNotification + "')]")).isDisplayed();
            boolean isRoomNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + roomName + "')]")).isDisplayed();

            return isAssignmentNameDisplayed && isRoomNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Notification exists on Notification detailed view page
     *
     * @param newsHeading Name of the News Notification
     * @param roomName Name of the Room
     * @return true if newsHeading and room name present else false
     */
    public boolean displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        try {
            boolean isAssignmentNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + newsHeading + "')]")).isDisplayed();
            boolean isRoomNameDisplayed =
                    androidDriver.findElement(By.xpath("//*[contains(@text,'" + roomName + "')]")).isDisplayed();

            return isAssignmentNameDisplayed && isRoomNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }
}
