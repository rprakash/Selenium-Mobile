package org.fronter.apps.automation.sdk.notification.worklist.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.worklist.pages.ios.WorkListPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Provides methods to assert different fields of the WorkList Page
 */
public class VerifyWorkListPageIOS {
    IOSDriver iOSDriver;

    public VerifyWorkListPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of WorkListPageIOS class.
     *
     * @return Instance of page object which models WorkListPageIOS
     */
    public WorkListPageIOS workListPageIOS() {
        return PageFactory.initElements(iOSDriver, WorkListPageIOS.class);
    }

    /**
     * Verify Not Submitted tab is displayed on worklist Page
     */
    public void hasNotSubmittedTab() {
        Verify.checkPoint(workListPageIOS().hasTab("Not submitted"), "Not Submitted tab is displayed on Worklist Page");
    }

    /**
     * Verify Submitted tab is displayed on worklist Page
     */
    public void hasSubmittedTab() {
        Verify.checkPoint(workListPageIOS().hasTab("Submitted"), "Submitted tab is displayed on Worklist Page");
    }

    /**
     * Verify Evaluated tab is displayed on worklist Page
     */
    public void hasEvaluatedTab() {
        Verify.checkPoint(workListPageIOS().hasTab("Evaluated"), "Evaluated tab is displayed on Worklist Page");
    }

    /**
     * Verify that assignment is displayed in worklist
     *
     * @param assignmentName Name of assignment
     * @param tabName Name of tab i.e. 'Not Submitted', 'Submitted', or 'Evaluated'
     */
    private void hasAssignment(String assignmentName, String tabName) {
        Verify.checkPoint(workListPageIOS().hasAssignment(assignmentName), "Assignment '" + assignmentName
                + "' is displayed under '" + tabName + "' tab");
    }

    /**
     * Verify that assignment is displayed in worklist under Not Submitted tab
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of the room
     * @param dueDate Assignment due date
     */
    public void hasAssignmentWithDueDateAndRoomName(String assignmentName, String roomName, String dueDate) {
        Verify.checkPoint(workListPageIOS().hasAssignmentWithRoomNameAndDate(assignmentName, roomName, dueDate, "Due"),
                "Assignment '" + assignmentName + "' is displayed with room name '" + roomName + "' and due date '" + dueDate
                        + "'");
    }

    /**
     * Verify that assignment is displayed in worklist under Submitted tab
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of the room
     * @param submittedDate Assignment submitted date
     */
    public void hasAssignmentWithSubmittedDateAndRoomName(String assignmentName, String roomName, String submittedDate) {
        Verify.checkPoint(workListPageIOS()
                .hasAssignmentWithRoomNameAndDate(assignmentName, roomName, submittedDate, "Submitted"), "Assignment '"
                + assignmentName + "' is displayed with room name '" + roomName + "' and submitted date '" + submittedDate + "'");
    }

    /**
     * Verify that assignment is displayed in worklist under Evaluated tab
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of the room
     * @param evaluatedDate Assignment evaluation date
     */
    public void hasAssignmentWithEvaluationDateAndRoomName(String assignmentName, String roomName, String evaluatedDate) {
        Verify.checkPoint(workListPageIOS()
                .hasAssignmentWithRoomNameAndDate(assignmentName, roomName, evaluatedDate, "Evaluated"), "Assignment '"
                + assignmentName + "' is displayed with room name '" + roomName + "' and evaluation date '" + evaluatedDate + "'");
    }

    /**
     * Verify that assignment is displayed with correct room name and no due date
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of the room
     */
    public void hasAssignmentWithNoDueDateAndRoomName(String assignmentName, String roomName) {
        Verify.checkPoint(workListPageIOS().hasAssignmentWithRoomNameAndDate(assignmentName, roomName, "No due date", null),
                "Assignment '" + assignmentName + "' is displayed with room name '" + roomName + "' and no due date");
    }

    /**
     * Verify that number of due days left for assignment is displayed
     *
     * @param assignmentName Name of assignment
     * @param dueDaysLeft Due days left
     */
    public void displaysDueDaysLeftForAssignment(String assignmentName, String dueDaysLeft) {
        Verify.checkPoint(workListPageIOS().displaysDueDaysLeftForAssignment(assignmentName, dueDaysLeft),
                "Correct number of due day(s) left i.e. '" + dueDaysLeft + "' is displayed for assignment '" + assignmentName
                        + "'");
    }

    /**
     * Verify that assignment is displayed in worklist under Not Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderNotSubmittedTab(String assignmentName) {
        hasAssignment(assignmentName, "Not Submitted");
    }

    /**
     * Verify that assignment is displayed in worklist under Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderSubmittedTab(String assignmentName) {
        hasAssignment(assignmentName, "Submitted");
    }

    /**
     * Verify that assignment is displayed in worklist under Evaluated tab
     *
     * @param assignmentName Name of assignment
     */
    public void hasAssignmentUnderEvaluatedTab(String assignmentName) {
        hasAssignment(assignmentName, "Evaluated");
    }

    /**
     * Verify that assignment is not displayed in worklist
     *
     * @param assignmentName Name of assignment
     * @param tabName Name of the tab
     */
    private void doesNotHaveAssignment(String assignmentName, String tabName) {
        Verify.checkPoint(!workListPageIOS().hasAssignment(assignmentName), "Assignment '" + assignmentName
                + "' is not displayed under '" + tabName + "' tab");
    }

    /**
     * Verify that assignment is not displayed in worklist under Not Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderNotSubmittedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Not Submitted");
    }

    /**
     * Verify that assignment is not displayed in worklist under Submitted tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderSubmittedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Submitted");
    }

    /**
     * Verify that assignment is not displayed in worklist under Evaluated tab
     *
     * @param assignmentName Name of assignment
     */
    public void doesNotHaveAssignmentUnderEvaluatedTab(String assignmentName) {
        doesNotHaveAssignment(assignmentName, "Evaluated");
    }

    /**
     * Verify that due at time is displayed for assignment
     *
     * @param assignmentName Name of assignment
     * @param assignmentClosingTime Due at time
     */
    public void displaysDueAtTime(String assignmentName, String assignmentClosingTime) {
        Verify.checkPoint(workListPageIOS().displaysDueAtTime(assignmentName, assignmentClosingTime), "Correct due time i.e. '"
                + assignmentClosingTime + "' is displayed for assignment '" + assignmentName + "'");
    }

    /**
     * Verify that 'No Assignment' message appears on Worklist page
     */
    public void displaysNoAssignmentMessage() {
        Verify.checkPoint(workListPageIOS().displaysNoAssignmentMessage(),
                "'No Assignment' message is displayed when there is no assignments on Worklist page");
    }
}
