package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NewsViewPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

/**
 * Verify News View Page Class
 */
public class VerifyNewsViewPageIOS {
    IOSDriver iOSDriver;

    public VerifyNewsViewPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NewsViewPageIOS class.
     *
     * @return Instance of page object which models NewsViewPageIOS
     */
    public NewsViewPageIOS newsViewPageIOS() {
        return PageFactory.initElements(iOSDriver, NewsViewPageIOS.class);
    }

    /**
     * Verify Latest News exists on News View page
     *
     * @param newsHeading Heading of the News
     */
    public void displaysNews(String newsHeading) {
        Verify.checkPoint(newsViewPageIOS().displaysNews(newsHeading), "Latest News exists on News List page");
    }

    /**
     * Verify Room Name exists on News View page
     *
     * @param roomName Name of the room
     */
    public void hasRoomName(String roomName) {
        Verify.checkPoint(newsViewPageIOS().hasRoomName(roomName), "Room name exists on News Detalied View page");
    }

    /**
     * Verify Full Image with Save Button exists on News View page
     */
    public void hasFullImageViewWithSaveButton() {
        Verify.checkPoint(newsViewPageIOS().hasFullImageViewWithSaveButton(),
                "Full Image with Save Button exists on News Detalied View page");
    }

    /**
     * Verify Author First and Last Name exists on News View page
     *
     * @param teacher1FirstName first name of author
     * @param teacher1LastName last name of author
     */
    public void hasAuthorName(String teacher1FirstName, String teacher1LastName) {
        Verify.checkPoint(newsViewPageIOS().hasAuthorName(teacher1FirstName, teacher1LastName), "Author First name '"
                + teacher1FirstName + "' and Last Name '" + teacher1LastName + "' exists on News View page");
    }
}
