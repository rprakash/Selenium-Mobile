package org.fronter.apps.automation.sdk.notification.worklist.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Models the Worklist Page Class
 */
public class WorkListPageIOS {
    private IOSDriver iOSDriver;

    public WorkListPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Taps on WorkList icon
     */
    public void tapOnWorkListIcon() {
        iOSDriver.findElementByName("Worklist").click();
    }

    /**
     * Verify given tab is displayed on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is displayed
     */
    public boolean hasTab(String tabName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            return iOSDriver.findElementByName(tabName).isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Tap on any tab in WorkList Page
     *
     * @param tabName Name of the tab
     */
    public void clickOnTab(String tabName) {
        iOSDriver.findElementByName(tabName).click();
    }

    /**
     * Verify that assignment is displayed in worklist
     *
     * @param assignmentName Name of assignment
     * @return true if assignment is displayed in worklist
     */
    public boolean hasAssignment(String assignmentName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean displayedAssignmentTitle =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentName + "')]").isDisplayed();
            return displayedAssignmentTitle;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that assignment with correct date and Room name is displayed
     *
     * @param assignmentName Name of assignment
     * @param roomName Name of room
     * @param date Date of assignment
     * @param assignmentStatus Status of the assignment
     * @return true if assignment with correct date and Room name is displayed
     */
    public boolean hasAssignmentWithRoomNameAndDate(String assignmentName, String roomName, String date, String assignmentStatus) {
        try {
            boolean displayedAssignmentTitle =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentName + "')]").isDisplayed();
            String displayedRoom =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@value,'" + assignmentName + "')]//following-sibling::UIAStaticText[1]").getAttribute(
                            "name");
            String displayedDate =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@value,'" + assignmentName + "')]//following-sibling::UIAStaticText[2]").getAttribute(
                            "name");

            if (assignmentStatus != null) {
                date = assignmentStatus + ": " + date;
            }

            return displayedAssignmentTitle && displayedRoom.equals(roomName) && displayedDate.contains(date);
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify correct number of days left is displayed
     *
     * @param assignmentName Name of assignment
     * @param dueDaysLeft Days left for assignment overdue
     * @return true if assignment with correct number of days left is displayed, else false
     */
    public boolean displaysDueDaysLeftForAssignment(String assignmentName, String dueDaysLeft) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            String daysDisplayed =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@value,'" + assignmentName + "')]//following-sibling::UIAStaticText[4]").getAttribute(
                            "name");
            String displayedDaysText =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@value,'" + assignmentName + "')]//following-sibling::UIAStaticText[3]").getAttribute(
                            "name");
            String dueDaysDisplayed = daysDisplayed + " " + displayedDaysText;

            String expectedDaysText;
            if (dueDaysLeft.equals("1")) {
                expectedDaysText = "day left";
            } else {
                expectedDaysText = "days left";
            }
            return dueDaysDisplayed.equals(dueDaysLeft + " " + expectedDaysText);
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that due at time is displayed for assignment
     *
     * @param assignmentName Name of assignment
     * @param assignmentClosingTime Due at time
     * @return true if due at time is displayed for assignment
     */
    public boolean displaysDueAtTime(String assignmentName, String assignmentClosingTime) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            String displayedClosingTime =
                    iOSDriver.findElementByXPath(
                            "//*[contains(@value,'" + assignmentName + "')]//following-sibling::UIAStaticText[4]").getAttribute(
                            "name");
            return displayedClosingTime.equals(assignmentClosingTime);
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that 'No Assignment' message appears on Worklist page
     *
     * @return true if 'No Assignment' message appears on worklist page, else false
     */
    public boolean displaysNoAssignmentMessage() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            return iOSDriver.findElementByXPath("//*[contains(@name,'No assignments')]").isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Tab on assignment name to open it
     *
     * @param assignmentName Name of the assignment
     * @return Instance of page object which models AssignmentHeaderPageIOS
     */
    public AssignmentDetailsPageIOS clickOnAssignmentHeading(String assignmentName) {
        iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentName + "')]").click();
        return PageFactory.initElements(iOSDriver, AssignmentDetailsPageIOS.class);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        iOSDriver.findElementByName("Back").click();
    }
}
