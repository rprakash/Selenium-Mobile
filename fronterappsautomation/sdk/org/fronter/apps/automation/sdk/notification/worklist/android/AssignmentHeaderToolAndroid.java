package org.fronter.apps.automation.sdk.notification.worklist.android;

import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentHeaderPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.pages.android.AssignmentSubmissionPageAndroid;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

public class AssignmentHeaderToolAndroid {
    private AndroidDriver androidDriver;

    public AssignmentHeaderToolAndroid(WebDriver driver) {
        this.androidDriver = (AndroidDriver) driver;
    }

    /**
     * Returns an instance of AssignmentDetailsPageAndroid class.
     *
     * @return Instance of page object which models AssignmentDetailsPageAndroid
     */
    public AssignmentHeaderPageAndroid assignmentHeaderPageAndroid() {
        return PageFactory.initElements(androidDriver, AssignmentHeaderPageAndroid.class);
    }

    /**
     * Returns an instance of AssignmentDetailsPageAndroid class.
     *
     * @return Instance of page object which models AssignmentDetailsPageAndroid
     */
    public AssignmentSubmissionPageAndroid assignmentSubmissionPageAndroid() {
        return PageFactory.initElements(androidDriver, AssignmentSubmissionPageAndroid.class);
    }

    /**
     * Tap on Details tab in details view
     */
    public void clickOnDetailsTab() {
        assignmentHeaderPageAndroid().clickOnTab("Details");
    }

    /**
     * Tap on Submission tab in Submission tab
     */
    public void clickOnSubmissionTab() {
        assignmentHeaderPageAndroid().clickOnTab("Submission");
    }

    /**
     * Tap on Evaluation tab in Evaluation tab
     */
    public void clickOnEvaluationTab() {
        assignmentHeaderPageAndroid().clickOnTab("Evaluation");
    }
}
