package org.fronter.apps.automation.sdk.notification.pages.ios;

import java.util.List;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.ios.IOSDriver;

/**
 * Notification View Page Class
 */
public class NotificationViewPageIOS {
    private IOSDriver iOSDriver;

    public NotificationViewPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    // Mobile Elements
    String fullScreenImage = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAImage[1]";
    String saveButton = "Save";

    /**
     * Verify Room Name exists on Notification View page
     *
     * @param roomName Name of the Room
     * @return true if Room Name present else false
     */
    public boolean hasRoomName(String roomName) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + roomName + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify 'No Due date' message exists on Notification View page
     *
     * @return true if 'No Due date' message exists on Notification View page, else false
     */
    public boolean hasNoDueDateMsg() {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            return iOSDriver.findElementByXPath("//*[contains(@name,'No Due date')]").isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        iOSDriver.findElementByName("Back").click();
    }

    /**
     * Verify Test Heading exists on Notification View page
     *
     * @param testNotification Notification of the test
     * @return true if Test Heading present else false
     */
    public boolean hasTestNotification(String testNotification) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + testNotification + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Test Description exists on Notification View page
     *
     * @param testDescription Notification of the test
     * @return true if Test Description present else false
     */
    public boolean hasTestDescription(String testDescription) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + testDescription + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Assignment Heading exists on Notification View page
     *
     * @param assignmentNotification Notification of the Assignment
     * @return true if Assignment Heading present else false
     */
    public boolean hasAssignmentNotification(String assignmentNotification) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + assignmentNotification + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Assignment Description exists on Notification View page
     *
     * @param assignmentDescription Description of the Assignment
     * @return true if Assignment Description present else false
     */
    public boolean hasAssignmentDescription(String assignmentDescription) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + assignmentDescription + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Description exists on Notification View page
     *
     * @param newsDescription Description of the News
     * @return true if News Description present else false
     */
    public boolean hasNewsDescription(String newsDescription) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + newsDescription + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify News Heading exists on Notification View page
     *
     * @param newsHeading Heading of the News
     * @return true if News Heading present else false
     */
    public boolean hasNewsNotification(String newsHeading) {
        try {
            return iOSDriver.findElement(By.xpath("//*[contains(@name,'" + newsHeading + "')]")).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on the Image
     */
    public void tapOnImage() {
        iOSDriver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[1]/UIALink[1]")
                .click();
    }

    /**
     * Verify Full Image with Save Button exists on Notification View page
     *
     * @return true if Full Image with Save Button present else false
     */
    public boolean hasFullImageViewWithSaveButton() {
        try {
            boolean isFullImageDisplayed = iOSDriver.findElementByXPath(fullScreenImage).isDisplayed();
            boolean isDownloadBtnDisplayed = iOSDriver.findElementByName(saveButton).isDisplayed();

            return isFullImageDisplayed && isDownloadBtnDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Scroll to Bottom of the page on Notification List
     */
    public void scrollToBottom() {
        List<WebElement> newsDates =
                iOSDriver.findElementsByXPath("//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell");
        iOSDriver.scrollTo(iOSDriver.findElementByXPath(
                "//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[" + newsDates.size() + "]/UIAStaticText[1]")
                .getText());

    }

    /**
     * Verify Author First and Last Name exists on Notification View page
     *
     * @param teacher1FirstName first name of author
     * @param teacher1LastName last name of author
     * @return true if Author first and last name present else flase
     */
    public boolean hasAuthorName(String teacher1FirstName, String teacher1LastName) {
        boolean isAuthorNameDisplayed = iOSDriver
                .findElement(By.xpath("//*[contains(@name,'" + teacher1FirstName + " " + teacher1LastName + "')]")).isDisplayed();

        return isAuthorNameDisplayed;
    }
}
