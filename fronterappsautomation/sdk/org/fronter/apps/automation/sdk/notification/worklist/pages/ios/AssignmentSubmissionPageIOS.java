package org.fronter.apps.automation.sdk.notification.worklist.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;

/**
 * Models the assignment submission page class
 */
public class AssignmentSubmissionPageIOS {
    private IOSDriver iOSDriver;

    public AssignmentSubmissionPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Verify that submission date is displayed in assignment submission page
     *
     * @param submittedDate Assignment submission date
     * @return true if submitted date is displayed, else false
     */
    public boolean hasSubmittedDate(String submittedDate) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean displayedSubmittedDate =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + submittedDate + "')]").isDisplayed();
            return displayedSubmittedDate;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }
}
