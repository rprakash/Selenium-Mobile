package org.fronter.apps.automation.sdk.notification.worklist.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;

/**
 * Models the assignment details page class
 */
public class AssignmentDetailsPageIOS {
    private IOSDriver iOSDriver;

    public AssignmentDetailsPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    /**
     * Verify given tab is displayed on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is displayed
     */
    public boolean hasTab(String tabName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            return iOSDriver.findElementByName(tabName).isDisplayed();
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify given tab is disabled on Worklist Page
     *
     * @param tabName Name of tab
     * @return true if given tab is enabled
     */
    public boolean isTabEnabled(String tabName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean isTabEnabled = iOSDriver.findElementByName(tabName).isEnabled();
            return isTabEnabled;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that assignment with correct due date and status is displayed in assignment detail view
     *
     * @param assignmentName Name of assignment
     * @param assignmentDueDate Due Date of assignment
     * @param assignmentStatus Status for the assignment e.g "Not Submitted"
     * @return true if the given assignment is displayed with the correct due date and status, else false
     */
    public boolean hasAssignmentWithDateAndStatus(String assignmentName, String dueDate, String assignmentStatus) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean displayedAssignmentTitle =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentName + "')]").isDisplayed();

            boolean assignmentDueDateTime = iOSDriver.findElementByXPath("//*[contains(@name,'" + dueDate + "')]").isDisplayed();
            boolean displayedAssignmentStatus =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentStatus + "')]").isDisplayed();

            return displayedAssignmentTitle && assignmentDueDateTime && displayedAssignmentStatus;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verifies that the given text is displayed
     *
     * @param iconText Text displayed corresponding to the icon
     * @return true if text is displayed, else false
     */
    public boolean displaysIconText(String iconText) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean iconTextName = iOSDriver.findElementByXPath("//*[contains(@name,'" + iconText + "')]").isDisplayed();
            return iconTextName;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that description is displayed
     *
     * @param assignmentDesc text of the description
     * @return true if assignment description is displayed, else false
     */
    public boolean hasDescription(String assignmentDesc) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean assignmentDescription =
                    iOSDriver.findElementByXPath("//*[contains(@name,'" + assignmentDesc + "')]").isDisplayed();
            return assignmentDescription;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().enableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Verify that file is displayed
     *
     * @param fileName Name of the file
     * @return true if the file is present on assignment details page, else false
     */
    public boolean hasFile(String fileName) {
        WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        try {
            boolean fileTitle = iOSDriver.findElementByXPath("//*[contains(@name,'" + fileName + "')]").isDisplayed();
            return fileTitle;
        } catch (Exception e) {
            return false;
        } finally {
            WebDriverFactory.getInstance().disableImplicitWaits(iOSDriver);
        }
    }

    /**
     * Tap on any tab in WorkList Page
     *
     * @param tabName Name of the tab
     */
    public void clickOnTab(String tabName) {
        iOSDriver.findElementByName(tabName).click();
    }
}
