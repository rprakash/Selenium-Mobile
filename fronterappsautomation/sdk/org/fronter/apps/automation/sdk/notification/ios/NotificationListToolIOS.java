package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NotificationListPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification List Tool Page Class
 */
public class NotificationListToolIOS {
    private IOSDriver iOSDriver;

    public NotificationListToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NotificationListPageIOS class.
     *
     * @return Instance of page object which models NotificationListPageIOS
     */
    public NotificationListPageIOS notificationListPageIOS() {
        return PageFactory.initElements(iOSDriver, NotificationListPageIOS.class);
    }

    /**
     * Open Test Notification on Notification List
     *
     * @param testNotification Notification of the test
     */
    public void openTestNotification(String testNotification) {
        notificationListPageIOS().openTestNotification(testNotification);
    }

    /**
     * Open assignment Notification on Notification List
     *
     * @param assignmentNotification Notification of the assignment
     */
    public void openAssignmentNotification(String assignmentNotification) {
        notificationListPageIOS().openAssignmentNotification(assignmentNotification);
    }

    /**
     * Open News Notification on Notification List
     *
     * @param newsHeading Heading of the news
     */
    public void openNewsNotification(String newsHeading) {
        notificationListPageIOS().openNewsNotification(newsHeading);
    }
}
