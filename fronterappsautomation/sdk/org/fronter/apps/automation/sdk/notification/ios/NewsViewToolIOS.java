package org.fronter.apps.automation.sdk.notification.ios;

import io.appium.java_client.ios.IOSDriver;

import org.fronter.apps.automation.sdk.notification.pages.ios.NewsViewPageIOS;
import org.openqa.selenium.support.PageFactory;

/**
 * Notification News View Tool Page Class
 */
public class NewsViewToolIOS {
    private IOSDriver iOSDriver;

    public NewsViewToolIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of NewsViewPageIOS class.
     *
     * @return Instance of page object which models NewsViewPageIOS
     */
    public NewsViewPageIOS newsViewPageIOS() {
        return PageFactory.initElements(iOSDriver, NewsViewPageIOS.class);
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        newsViewPageIOS().navigateBack();
    }

    /**
     * Tap on the Image
     */
    public void tapOnImage() {
        newsViewPageIOS().tapOnImage();
    }
}
