package org.fronter.apps.automation.sdk.notification.pages.ios;

import io.appium.java_client.ios.IOSDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Notification News View Page Class
 */
public class NewsViewPageIOS {
    private IOSDriver iOSDriver;

    public NewsViewPageIOS(WebDriver driver) {
        this.iOSDriver = (IOSDriver) driver;
    }

    // Mobile Elements identifiers
    String newsTitle = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[1]";
    String room = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIAStaticText[2]";
    String fullScreenImage = "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAImage[1]";
    String saveButton = "Save";


    /**
     * Verify News exists on News View page
     *
     * @param newsHeading Heading of the News
     * @return true if News Heading present else false
     */
    public boolean displaysNews(String newsHeading) {
        WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(newsTitle)));
        try {
            return iOSDriver.findElementByXPath(newsTitle).getText().trim().equalsIgnoreCase(newsHeading);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Room Name exists on News View page
     *
     * @param roomName Name of the Room
     * @return true if Room Name present else false
     */
    public boolean hasRoomName(String roomName) {
        try {
            return iOSDriver.findElementByXPath(room).getText().trim().equalsIgnoreCase(roomName);
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Tap on the Back Button
     */
    public void navigateBack() {
        iOSDriver.findElementByName("Back").click();
    }

    /**
     * Tap on the Image
     */
    public void tapOnImage() {
        iOSDriver.findElementByXPath("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]/UIALink[1]/UIALink[1]")
                .click();
    }

    /**
     * Verify Full Image with Save Button exists on News View page
     *
     * @return true if Full Image with Save Button present else false
     */
    public boolean hasFullImageViewWithSaveButton() {
        try {
            boolean isFullImageDisplayed = iOSDriver.findElementByXPath(fullScreenImage).isDisplayed();
            boolean isDownloadBtnDisplayed = iOSDriver.findElementByName(saveButton).isDisplayed();

            return isFullImageDisplayed && isDownloadBtnDisplayed;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Verify Author First and Last Name exists on News View page
     *
     * @param teacher1FirstName first name of author
     * @param teacher1LastName last name of author
     * @return true if Author First and Last Name present else false
     */
    public boolean hasAuthorName(String teacher1FirstName, String teacher1LastName) {
        try {
            boolean isAuthorNameDisplayed =
                    iOSDriver.findElement(By.xpath("//*[contains(@name,'" + teacher1FirstName + " " + teacher1LastName + "')]"))
                            .isDisplayed();

            return isAuthorNameDisplayed;
        } catch (Exception e) {
            return false;
        }
    }
}
