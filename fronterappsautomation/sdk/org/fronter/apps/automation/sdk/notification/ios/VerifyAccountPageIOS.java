package org.fronter.apps.automation.sdk.notification.ios;

import org.fronter.apps.automation.sdk.notification.pages.ios.AccountPageIOS;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.ios.IOSDriver;

/**
 * Verify Account Page Class
 */
public class VerifyAccountPageIOS {
    IOSDriver iOSDriver;

    public VerifyAccountPageIOS(IOSDriver iOSDriver) {
        this.iOSDriver = iOSDriver;
    }

    /**
     * Returns an instance of AccountPageIOS class.
     *
     * @return Instance of page object which models AccountPageIOS
     */
    public AccountPageIOS accountPageIOS() {
        return PageFactory.initElements(iOSDriver, AccountPageIOS.class);
    }

    /**
     * Verify First name and Last name exists on Account page
     *
     * @param firstName First Name of the student
     * @param lastName Last Name of the student
     */
    public void hasFirstAndLastname(String firstName, String lastName) {
        Verify.checkPoint(accountPageIOS().hasFirstAndLastname(firstName, lastName),
                "First name and Last name exists on Account page");
    }

    /**
     * Verify that list of institutions is displayed on Account page
     *
     * @param institutionName Name of the institution
     */
    public void hasInstitutionName(String[] institutionName) {
        Verify.checkPoint(accountPageIOS().hasInstitutionName(institutionName),
                "Institution '" + institutionName + "' exists on Account page");
    }

    /**
     * Verify confirmation popup exists on Account page
     */
    public void displaysInstitutionChangeConfirmation() {
        Verify.checkPoint(accountPageIOS().displaysInstitutionChangeConfirmation(), "Confirmation pop up exists on Account page");
    }
}
