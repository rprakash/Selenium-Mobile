package org.fronter.apps.automation.sdk.notification.android;

import org.fronter.apps.automation.sdk.notification.pages.android.NotificationListPageAndroid;
import org.fronter.automation.sdk.Verify;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.android.AndroidDriver;

/**
 * Verify Notification List Page Class
 */
public class VerifyNotificationListPageAndroid {
    AndroidDriver androidDriver;

    public VerifyNotificationListPageAndroid(AndroidDriver androidDriver) {
        this.androidDriver = androidDriver;
    }

    /**
     * Returns an instance of NotificationListPageAndroid class.
     *
     * @return Instance of page object which models NotificationListPageAndroid
     */
    public NotificationListPageAndroid notificationListPageAndroid() {
        return PageFactory.initElements(androidDriver, NotificationListPageAndroid.class);
    }

    /**
     * Verify Test Notification exists on Notification List page
     *
     * @param testNotification Notification of Test
     * @param roomName Name of the room
     */
    public void displaysTestNotificationWithRoomName(String testNotification, String roomName) {
        Verify.checkPoint(notificationListPageAndroid().displaysTestNotificationWithRoomName(testNotification, roomName),
                "Test Notification exists on Notification List page");
    }

    /**
     * Verify Test Notification exists on Notification List page
     *
     * @param assignmentNotification Notification of Assignment
     * @param roomName Name of the room
     */
    public void displaysAssignmentNotificationWithRoomName(String assignmentNotification, String roomName) {
        Verify.checkPoint(
                notificationListPageAndroid().displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName),
                "Assignment Notification exists on Notification List page");
    }

    /**
     * Verify Empty Notification list has 'No notification' message
     */
    public void hasEmptyNotification() {
        Verify.checkPoint(notificationListPageAndroid().hasEmptyNotification(),
                "Empty Notification page with 'No notification' message appear on Notification List page");
    }

    /**
     * Verify News Notification exists on Notification List page
     *
     * @param newsHeading Notification of News Heading
     * @param roomName Name of the room
     */
    public void displaysNewsNotificationWithRoomName(String newsHeading, String roomName) {
        Verify.checkPoint(notificationListPageAndroid().displaysNewsNotificationWithRoomName(newsHeading, roomName),
                "News Notification exists on Notification List page");
    }

    /**
     * Verify 'No Due date' message exists on Notification View page
     */
    public void displaysNoDueDate() {
        Verify.checkPoint(notificationListPageAndroid().hasNoDueDate(),
                "'No Due date'message exists on Notification Detailed View page");
    }

    /**
     * Verify 'Today' is displayed in List header
     */
    public void displaysHeaderAsToday() {
        Verify.checkPoint(notificationListPageAndroid().displaysHeaderAsToday(), "'Today' is displayed in List header");
    }

    /**
     * Verify that notification icon is displayed in list
     */
    public void hasNotificationIcon() {
        Verify.checkPoint(notificationListPageAndroid().hasNotificationIcon(), "Notification icon is displayed in list");
    }
}
