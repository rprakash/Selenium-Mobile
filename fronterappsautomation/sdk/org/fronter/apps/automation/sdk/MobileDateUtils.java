package org.fronter.apps.automation.sdk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

/**
 * Provides global date utility methods which can be used in test cases.
 */
public class MobileDateUtils {

    /**
     * To get the date in Specified format such that 'days' specify the number of days ahead of current date
     * This format is specific to Simulator only because of changing regional setting to India
     * But date format is different on iPhone even after changing regional settings to India.
     *
     * @param days current date
     * @return Date in specified format
     */
    public String getDateIncrementedBy(int days, String dateFormatType) {
        DateFormat dateFormat = new SimpleDateFormat(dateFormatType);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        String convertedDate = dateFormat.format(cal.getTime());
        return convertedDate;
    }

    /**
     * To get the date in 'MMM d, yyyy' format
     *
     * @return Date in specified format
     */
    public String getTodayDateForSimulator() {
        return getDateIncrementedBy(0, "MMM d, yyyy");
    }

    /**
     * To get the date in 'dd-MMM-yyyy' format
     *
     * @return Date in specified format
     */
    public String getTodayDateOnIOS() {
        return getDateIncrementedBy(0, "dd-MMM-yyyy");
    }
    
    /**
     * To get the date in 'dd/mm/yy' format
     *
     * @return Date in specified format
     */
    public String getTodayDateInSimpleFormatOnIOS() {
        return getDateIncrementedBy(0, "dd/MM/yy");
    }

    /**
     * To get today's date in 'd MMMM' format
     *
     * @return Date in specified format
     */
    public String getTodayDateOnAndroid() {
        return getDateIncrementedBy(0, "d MMMM");
    }

    /**
     * To get the date in 'd MMMM YYYY' format
     *
     * @param days Incremented days
     * @return incremented date in 'd MMMM YYYY' format
     */
    public String getDateIncrementedByOnAndroid(int days) {
        return getDateIncrementedBy(days, "d MMMM YYYY");
    }

    /**
     * Get 4 digit random number as String
     *
     * @return 4 digit random number as String
     */
    public String getRandomNumber() {
        Random random = new Random();
        int randomNumber = 1000 + random.nextInt(9999);
        return String.valueOf(randomNumber);
    }
}
