package org.fronter.apps.automation.sdk;

import java.io.File;

import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;

/**
 * Class reading all required test data
 */
public class TestData {

    public String installation1, installation2;
    public String teacher1UserName, teacher1Password;
    public String teacher1FirstName, teacher1LastName;

    public String student1FirstName, student1LastName;
    public String student1UserName, student1Password;
    public String logoutConfirmationMsg;

    public String student2UserNameWithNoData, student2Password;
    public String student2FirstName, student2LastName;

    public String newsHeading, newsContent;
    public String imageFileTitle, imageFilePath;
    public String testHeading, testDescription;
    public String timeBoundAssign, twoDaysAssign, alwaysOpenAssign;
    public String assignmentDescription, uploadFilePath;
    public String roomName, newsTodayLabel, newsYesterdayLabel;
    public String invalidUserName;
    public String invalidPassword;
    public String loginErrorMessage, newsMessage, notificationMessage;
    public String sendFeedbackMessage1, sendFeedbackMessage2, emailPlaceHolder;
    public String confirmationFeedbackMessage1, confirmationFeedbackMessage2;
    public String submittedAssignmentHead, evaluatedAssignmentHead;
    public String grade, evalComment, fileName, informatoryMsg, noMoreItemMsg, noAssignment, noMoreNews;
    public String feedbackEmail, feedbackText;

    public String newsHeadingKey = "newsHeading";
    public String newsContentKey = "newsContent";
    public String newsHeading2Key = "newsHeading2";
    public String newsContent2Key = "newsContent2";

    public String testHeadingKey = "testHeading";
    public String testDescriptionKey = "testDescription";

    public String timeBoundAssignmentKey = "timeBoundAssignment";
    public String timeBoundAssignmentDescKey = "timeBoundAssignmentDesc";
    public String submittedAssignmentKey = "submittedAssignment";
    public String submittedAssignmentDescKey = "submittedAssignmentDesc";
    public String evaluatedAssignmentKey = "evaluatedAssignment";
    public String evaluatedAssignmentDescKey = "evaluatedAssignmentDesc";
    public String twoDaysAssignmentKey = "twoDaysAssignment";
    public String twoDaysAssignmentDescKey = "twoDaysAssignmentDesc";
    public String alwaysOpenAssignmentKey = "alwaysOpenAssignment";
    public String alwaysOpenAssignmentDescKey = "alwaysOpenAssignmentDesc";

    public String closingTimeOfAssignmentKey = "closingTimeOfTimeBoundAssignment";

    public TestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.NOTIFICATION_USER_DETAILS);
        TestDataCollection installationName = testData.getCollection("installation");
        installation1 = installationName.getString("name1");
        installation2 = installationName.getString("name2");

        TestDataCollection teacher = testData.getCollection("teacher1");
        teacher1UserName = teacher.getString("userId");
        teacher1Password = teacher.getString("password");
        teacher1FirstName = teacher.getString("firstName");
        teacher1LastName = teacher.getString("lastName");

        TestDataCollection student = testData.getCollection("student1");
        student1UserName = student.getString("userId");
        student1Password = student.getString("password");
        student1FirstName = student.getString("firstName");
        student1LastName = student.getString("lastName");

        student = testData.getCollection("studentWithNoData");
        student2UserNameWithNoData = student.getString("userId");
        student2Password = student.getString("password");
        student2FirstName = student.getString("firstName");
        student2LastName = student.getString("lastName");

        student = testData.getCollection("studentWithInvalidCredentials");
        invalidUserName = student.getString("userId");
        invalidPassword = student.getString("password");

        testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.NOTIFICATION_CONTENT);
        TestDataCollection news = testData.getCollection("news");
        newsHeading = news.getString("heading");
        newsContent = news.getString("content");
        newsTodayLabel = news.getString("todayLabel");
        newsYesterdayLabel = news.getString("yesterdayLabel");

        imageFileTitle = news.getString("imageTitle");
        imageFilePath = System.getProperty("user.dir");
        imageFilePath += news.getString("imageLocation").replace('/', File.separatorChar);

        TestDataCollection test = testData.getCollection("test");
        testHeading = test.getString("heading");
        testDescription = test.getString("description");

        TestDataCollection assignment = testData.getCollection("assignment");
        timeBoundAssign = assignment.getString("timeBound");
        twoDaysAssign = assignment.getString("twoDays");
        alwaysOpenAssign = assignment.getString("alwaysOpen");
        assignmentDescription = assignment.getString("description");

        uploadFilePath = System.getProperty("user.dir");
        uploadFilePath += assignment.getString("uploadLocation").replace('/', File.separatorChar);

        submittedAssignmentHead = assignment.getString("submittedAssignmentTitle");
        evaluatedAssignmentHead = assignment.getString("evaluatedAssignmentTitle");

        grade = assignment.getString("grade");
        evalComment = assignment.getString("evalComment");
        fileName = assignment.getString("fileName");
        informatoryMsg = assignment.getString("informatoryMsg");


        TestDataCollection roomData = testData.getCollection("room");
        roomName = roomData.getString("roomName");

        TestDataCollection messages = testData.getCollection("messages");
        loginErrorMessage = messages.getString("loginErrorMessage");
        newsMessage = messages.getString("news");
        notificationMessage = messages.getString("notifications");
        sendFeedbackMessage1 = messages.getString("sendFeedback1");
        sendFeedbackMessage2 = messages.getString("sendFeedback2");
        confirmationFeedbackMessage1 = messages.getString("confFeedback1");
        confirmationFeedbackMessage2 = messages.getString("confFeedback2");
        emailPlaceHolder = messages.getString("emailPlaceHolder");
        logoutConfirmationMsg = messages.getString("logoutConfrimationMsg");
        noMoreItemMsg = messages.getString("noMoreItemMsg");
        noAssignment = messages.getString("noAssignment");
        noMoreNews = messages.getString("noMoreNews");

        TestDataCollection error = testData.getCollection("messages");
        loginErrorMessage = error.getString("loginErrorMessage");
        newsMessage = error.getString("news");
        notificationMessage = error.getString("notifications");

        TestDataCollection feedback = testData.getCollection("feedback");
        feedbackEmail = feedback.getString("email");
        feedbackText = feedback.getString("text");
    }
}
