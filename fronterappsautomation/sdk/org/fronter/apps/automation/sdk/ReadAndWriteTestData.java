package org.fronter.apps.automation.sdk;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Read and write data in properties file
 */
public class ReadAndWriteTestData {

    public String testFile = "test-data/test.properties";

    /**
     * Write data in test properties file
     *
     * @param keys array of keys
     * @param values array of values
     */
    public void writeInTestFile(String[] keys, String[] values) {
        Properties prop = new Properties();
        OutputStream output = null;
        try {
            output = new FileOutputStream(testFile);

            for (int i = 0; i < keys.length; i++) {
                prop.setProperty(keys[i], values[i]);
            }

            prop.store(output, null);
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Read data from test properties file
     *
     * @param key Array of keys
     * @return String array with values
     */
    public String[] readFromTestFile(String[] key) {
        Properties prop = new Properties();
        InputStream input = null;
        String[] values = new String[key.length];
        try {
            input = new FileInputStream(testFile);
            prop.load(input);

            for (int i = 0; i < key.length; i++) {
                values[i] = prop.getProperty(key[i]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return values;
    }
}
