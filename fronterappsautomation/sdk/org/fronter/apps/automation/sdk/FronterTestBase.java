package org.fronter.apps.automation.sdk;

import org.fronter.automation.sdk.Navigator;
import org.fronter.automation.sdk.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

/**
 * Common test life cycle for all classes that will test Fronter (main application)
 */
public class FronterTestBase {
    protected WebDriver driver;
    protected Navigator navigate;
    protected String url;
    protected TestData testData = new TestData();
    protected ReadAndWriteTestData readWriteFile = new ReadAndWriteTestData();
    protected MobileDateUtils mobileDateUtils = new MobileDateUtils();

    @BeforeClass
    public void instantiateDriver() {
        driver = WebDriverFactory.getInstance().getLocalWebDriver();
        navigate = new Navigator(driver);
    }

    @AfterClass(alwaysRun = true)
    public void closeBrowser() {
        driver.close();
    }
}
