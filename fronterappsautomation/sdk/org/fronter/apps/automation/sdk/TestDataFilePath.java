package org.fronter.apps.automation.sdk;

import org.fronter.automation.sdk.YamlFileReader;

/**
 * Class having path of all test data yml files
 */
public class TestDataFilePath extends YamlFileReader {

    // Paths to the main test data files
    public static final String BASE_DIR = "test-data/";
    public static final String USER_DETAILS = BASE_DIR + "userdetails.yml";
    public static final String NOTIFICATION_USER_DETAILS = BASE_DIR + "notification-userdetails.yml";
    public static final String NOTIFICATION_CONTENT = BASE_DIR + "notification-content.yml";

    /**
     * Create a new reader that will load the given test data file
     *
     * @param path Path to the file containing the test data
     */
    public TestDataFilePath(String path) {
        super(path);
    }
}
