package org.fronter.apps.automation.sdk.login.pages;

import org.fronter.automation.sdk.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

/**
 * Green Abbey Login Page Class
 */
public class LoginPage {
    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "username")
    @CacheLookup
    private WebElement userIdTestBox;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement passwdTestBox;

    @FindBy(className = "form-button")
    @CacheLookup
    private WebElement loginButton;

    /**
     * Enter Login Credentials of User
     *
     * @param userName userId of the User
     * @param password password of the User
     */
    public void login(String userName, String password) {
        String url = Configuration.getInstance().get("url");
        driver.get(url);
        driver.findElement(By.className("login-provider-container")).click();
        userIdTestBox.sendKeys(userName);
        passwdTestBox.sendKeys(password);
        loginButton.click();
    }
}
