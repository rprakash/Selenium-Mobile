package org.fronter.apps.automation.sdk.login;

import org.fronter.apps.automation.sdk.login.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Green Abbey Login Tool Page Class
 */
public class LoginTool {
    private WebDriver driver;

    public LoginTool(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Returns an instance of LoginPage class.
     *
     * @return Instance of page object which models LoginPage
     */
    public LoginPage loginPage() {
        return PageFactory.initElements(driver, LoginPage.class);
    }

    /**
     * Enter Login Credentials of User
     *
     * @param userName userId of the User
     * @param password password of the User
     */
    public void login(String userName, String password) {
        loginPage().login(userName, password);
    }
}
