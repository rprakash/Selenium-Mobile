package org.fronter.notification.automation.testcases.android;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyAssignmentHeaderPageAndroid;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Read Tests and Assignment Notification from Notification List and view
 */
public class ReadNotifications extends FronterAppsTestBase {

    private String roomName = testData.roomName;
    private String testNotification;
    private String assignmentNotification;
    private String newsHeading;
    private String newsContent;

    private VerifyAssignmentHeaderPageAndroid verifyAssignmentHeaderPage;

    @BeforeClass
    public void instantiateObjects(){
        verifyAssignmentHeaderPage = new VerifyAssignmentHeaderPageAndroid(androidDriver);
    }

    @BeforeClass
    public void readTestData() {
        String newsHeadingKey = testData.newsHeadingKey;
        String newsContentKey = testData.newsContentKey;
        String testHeadingKey = testData.testHeadingKey;
        String assignmentHeadingKey = testData.twoDaysAssignmentKey;

        String[] key = {newsHeadingKey, assignmentHeadingKey, testHeadingKey, newsContentKey};
        String[] values = readWriteFile.readFromTestFile(key);
        newsHeading = values[0];
        assignmentNotification = values[1];
        testNotification = values[2];
        newsContent = values[3];
    }

    @BeforeClass
    public void login() {
        loginToolAndroid.loginAsStudent1();
    }

    /**
     * Test Case to Verify Test and Assignment Notification on List page
     */
    @Test
    public void readNotificationOnNotificationList() {
        newsListToolAndroid.tapOnNotificationCounter();
        verifyNotificationListPageAndroid.displaysHeaderAsToday();
        verifyNotificationListPageAndroid.hasNotificationIcon();
        verifyNotificationListPageAndroid.displaysTestNotificationWithRoomName(testNotification, roomName);
        verifyNotificationListPageAndroid.displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName);
        // Temporarily commenting this test verification, need to investigate
        // verifyNotificationListPageAndroid.displaysNewsNotificationWithRoomName(newsHeading, roomName);
    }

    /**
     * Test Case to Verify News, Test and Assignment Notification on view page
     */
    @Test(dependsOnMethods = "readNotificationOnNotificationList")
    public void readNotificationOnNotificationView() {
        notificationListToolAndroid.openNotification(testNotification);
        verifyNotificationViewPageAndroid.displaysTestNotificationWithRoomName(testNotification, roomName);
        verifyNotificationListPageAndroid.displaysNoDueDate();
        verifyNotificationViewPageAndroid.displaysOpenInFronterLink();
        notificationViewToolAndroid.clickOpenInFronterLink();
        notificationViewToolAndroid.clickBackButtonOnPhone();
        verifyNotificationViewPageAndroid.displaysOpenInFronterLink();

        notificationViewToolAndroid.navigateBack();
        notificationListToolAndroid.openNotification(assignmentNotification);
        verifyNotificationViewPageAndroid.displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName);
        verifyAssignmentHeaderPage.displaysAssignmentTitleWithNotSubStatus(assignmentNotification);
        notificationViewToolAndroid.navigateBack();

        notificationListToolAndroid.openNotification(newsContent);
        verifyNotificationViewPageAndroid.displaysNewsNotificationWithRoomName(newsHeading, roomName);
        verifyNotificationViewPageAndroid.hasImage();
        notificationViewToolAndroid.clickOnImage();
        verifyNotificationViewPageAndroid.hasFullImageWithDownloadButton();
        notificationViewToolAndroid.navigateBack();
        notificationViewToolAndroid.navigateBack();
    }
}
