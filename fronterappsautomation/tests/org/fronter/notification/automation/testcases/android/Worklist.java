package org.fronter.notification.automation.testcases.android;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.fronter.apps.automation.sdk.notification.worklist.android.AssignmentHeaderToolAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyAssignmentDetailsPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyAssignmentEvaluationPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyAssignmentHeaderPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyAssignmentSubmissionPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyWorklistPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.WorklistToolAndroid;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases for viewing submitted , not submitted and evaluated assignment
 */
public class Worklist extends FronterAppsTestBase {

    public String timeBoundAssignment, alwaysOpenAssignment;
    public String twoDaysAssignment, twoDayAssignmentDesc;
    public String submittedAssignment, submittedDesc;
    public String evaluatedAssignment, evaluatedDesc;
    public String roomName;
    public String twoDueDays = "2";
    public String closingTime;
    private String grade, evalComment, uploadedFileName, message, noMoreAssingment;

    public MobileDateUtils dateUtils = new MobileDateUtils();
    private String expectedDueDate = dateUtils.getDateIncrementedByOnAndroid(Integer.parseInt(twoDueDays));
    private String noDueDate = "No due date";
    private String todayDate = dateUtils.getDateIncrementedByOnAndroid(0);

    private VerifyWorklistPageAndroid verifyWorklistPageAndroid;
    private WorklistToolAndroid worklistToolAndroid;
    private AssignmentHeaderToolAndroid assignmentHeaderToolPageAndroid;
    private VerifyAssignmentHeaderPageAndroid verifyAssignmentHeaderPage;
    private VerifyAssignmentDetailsPageAndroid verifyAssignmentDetailsPage;
    private VerifyAssignmentSubmissionPageAndroid verifyAssignmentSubmissionPageAdroid;
    protected VerifyAssignmentEvaluationPageAndroid verifyAssignmentEvaluationPageAndroid;

    @BeforeClass
    public void instantiateObjects(){
        verifyWorklistPageAndroid = new VerifyWorklistPageAndroid(androidDriver);
        worklistToolAndroid = new WorklistToolAndroid(androidDriver);
        assignmentHeaderToolPageAndroid = new AssignmentHeaderToolAndroid(androidDriver);
        verifyAssignmentHeaderPage = new VerifyAssignmentHeaderPageAndroid(androidDriver);
        verifyAssignmentDetailsPage = new VerifyAssignmentDetailsPageAndroid(androidDriver);
        verifyAssignmentEvaluationPageAndroid = new VerifyAssignmentEvaluationPageAndroid(androidDriver);
        verifyAssignmentSubmissionPageAdroid =new VerifyAssignmentSubmissionPageAndroid(androidDriver);
    }

    @BeforeClass
    public void readTestData() {
        String[] key =
                {testData.timeBoundAssignmentKey, testData.alwaysOpenAssignmentKey, testData.twoDaysAssignmentKey,
                        testData.twoDaysAssignmentDescKey, testData.submittedAssignmentKey, testData.submittedAssignmentDescKey,
                        testData.evaluatedAssignmentKey, testData.evaluatedAssignmentDescKey, testData.closingTimeOfAssignmentKey};

        // Reading test data from properties file
        String[] values = readWriteFile.readFromTestFile(key);
        timeBoundAssignment = values[0];
        alwaysOpenAssignment = values[1];

        twoDaysAssignment = values[2];
        twoDayAssignmentDesc = values[3];

        submittedAssignment = values[4];
        submittedDesc = values[5];

        evaluatedAssignment = values[6];
        evaluatedDesc = values[7];

        closingTime = values[8];

        // Reading test data from yml file
        roomName = testData.roomName;
        grade = testData.grade;
        evalComment = testData.evalComment;
        uploadedFileName = testData.fileName;
        message = testData.informatoryMsg;
        noMoreAssingment = testData.noMoreItemMsg;
    }

    @BeforeClass
    public void login() {
        loginToolAndroid.loginAsStudent1();
        headerToolAndroid.openAccount();
        accountToolAndroid.clickOnWorklist();
    }

    /**
     * Test Case to verify that only not Submitted assignment is displayed in "Not Submitted" tab in worklist
     */
    @Test(priority = 0)
    public void notSubmittedAssignment() {
        verifyWorklistPageAndroid.hasNotSubmittedTab();
        verifyWorklistPageAndroid.hasSubmittedTab();
        verifyWorklistPageAndroid.hasEvaluatedTab();
        worklistToolAndroid.clickOnNotSubmittedTab();

        verifyWorklistPageAndroid.hasAssignmentUnderNotSubmittedTab(timeBoundAssignment);
        verifyWorklistPageAndroid.hasAssignmentUnderNotSubmittedTab(twoDaysAssignment);
        verifyWorklistPageAndroid.hasAssignmentUnderNotSubmittedTab(alwaysOpenAssignment);

        verifyWorklistPageAndroid.hasAssignmentWithDueDateAndRoomName(twoDaysAssignment, roomName, expectedDueDate);
        verifyWorklistPageAndroid.displaysDueDaysLeft(twoDaysAssignment, twoDueDays);
        verifyWorklistPageAndroid.displaysIconforAssignment(twoDaysAssignment);

        verifyWorklistPageAndroid.doesNotDisplayDueDaysField(alwaysOpenAssignment);
        verifyWorklistPageAndroid.displaysDueAtTime(timeBoundAssignment, closingTime);

        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderNotSubmittedTab(submittedAssignment);
        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderNotSubmittedTab(evaluatedAssignment);

    }

    /**
     * Test case to verify that not submitted assignment details are displayed
     */
    @Test(priority = 1)
    public void notSubmittedAssignmentDetailViews() {
        worklistToolAndroid.clickOnNotSubmittedTab();
        worklistToolAndroid.openAssignment(twoDaysAssignment);

        // Verify Tabs displayed
        verifyAssignmentHeaderPage.hasDetailsTab();
        verifyAssignmentHeaderPage.doesNotHaveSubmissionTab();
        verifyAssignmentHeaderPage.doesNotHaveEvaluationTab();
        verifyAssignmentHeaderPage.displaysAssignmentTitleWithNotSubStatus(twoDaysAssignment);

        assignmentHeaderToolPageAndroid.clickOnDetailsTab();
        verifyAssignmentDetailsPage.hasAssignmentDueDateAndRoomName(twoDaysAssignment, roomName, expectedDueDate);
        verifyAssignmentDetailsPage.hasDescription(twoDayAssignmentDesc);
        verifyAssignmentDetailsPage.hasUploadedFile(uploadedFileName);
        verifyAssignmentDetailsPage.displaysAssignmentIcon();
        verifyAssignmentDetailsPage.displaysIndividualIcon();
        verifyAssignmentDetailsPage.displaysMultipleTriesIcon();

        notificationViewToolAndroid.navigateBack();
        worklistToolAndroid.openAssignment(alwaysOpenAssignment);
        assignmentHeaderToolPageAndroid.clickOnDetailsTab();
        verifyAssignmentDetailsPage.hasAssignmentDueDateAndRoomName(alwaysOpenAssignment, roomName, noDueDate);
        verifyAssignmentDetailsPage.displaysAssignmentIcon();
        verifyAssignmentDetailsPage.displaysGroupIcon();
        verifyAssignmentDetailsPage.displaysOneTryIcon();
        notificationViewToolAndroid.navigateBack();
    }

    /**
     * Test Case to verify that only Submitted assignment is displayed in "Submitted" tab in worklist
     */
    @Test(priority = 2)
    public void submittedAssignment() {
        worklistToolAndroid.clickOnSubmittedTab();
        verifyWorklistPageAndroid.hasMessage(noMoreAssingment);
        verifyWorklistPageAndroid.displaysIconforAssignment(submittedAssignment);
        verifyWorklistPageAndroid.hasAssignmentUnderSubmittedTab(submittedAssignment);
        verifyWorklistPageAndroid.hasAssignmentWithSubmittedDateAndRoomName(submittedAssignment, roomName, todayDate);
        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderSubmittedTab(twoDaysAssignment);
        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderSubmittedTab(evaluatedAssignment);
    }

    /**
     * Test case to verify that details and submission views are displayed for submitted assignment
     */
    @Test(priority = 3)
    public void submittedAssignmentDetailViews() {
        worklistToolAndroid.clickOnSubmittedTab();
        worklistToolAndroid.openAssignment(submittedAssignment);

        // Verify Tabs displayed
        verifyAssignmentHeaderPage.hasDetailsTab();
        verifyAssignmentHeaderPage.hasSubmissionTab();
        verifyAssignmentHeaderPage.doesNotHaveEvaluationTab();
        verifyAssignmentHeaderPage.hasAssignmentTitleWithSubmittedStatus(submittedAssignment);

        assignmentHeaderToolPageAndroid.clickOnDetailsTab();
        verifyAssignmentDetailsPage.hasAssignmentDueDateAndRoomName(submittedAssignment, roomName, expectedDueDate);
        verifyAssignmentDetailsPage.hasDescription(submittedDesc);
        verifyAssignmentDetailsPage.hasUploadedFile(uploadedFileName);
        verifyAssignmentDetailsPage.displaysAssignmentIcon();
        verifyAssignmentDetailsPage.displaysIndividualIcon();
        verifyAssignmentDetailsPage.displaysMultipleTriesIcon();

        assignmentHeaderToolPageAndroid.clickOnSubmissionTab();
        verifyAssignmentSubmissionPageAdroid.hasSubmittedDate(todayDate);

        // Need to add test to verify comment added by student
        notificationViewToolAndroid.navigateBack();
    }

    /**
     * Test Case to verify that only Evaluated assignment is displayed in "Evaluated" tab in worklist
     */
    @Test(priority = 4)
    public void evaluatedAssignment() {
        worklistToolAndroid.clickOnEvaluatedTab();
        verifyWorklistPageAndroid.hasMessage(noMoreAssingment);
        verifyWorklistPageAndroid.displaysIconforAssignment(evaluatedAssignment);
        verifyWorklistPageAndroid.hasAssignmentUnderEvaluatedTab(evaluatedAssignment);
        verifyWorklistPageAndroid.hasAssignmentWithEvalDateAndRoomName(evaluatedAssignment, roomName, todayDate);
        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderEvaluatedTab(twoDaysAssignment);
        verifyWorklistPageAndroid.doesNotHaveAssignmentUnderEvaluatedTab(submittedAssignment);
    }

    /**
     * Test case to verify that details, submission and evaluation views are displayed for evaluated assignment
     */
    @Test(priority = 5)
    public void evaluatedAssignmentDetailViews() {
        worklistToolAndroid.clickOnEvaluatedTab();

        // Verify all Tabs are displayed
        worklistToolAndroid.openAssignment(evaluatedAssignment);
        verifyAssignmentHeaderPage.hasDetailsTab();
        verifyAssignmentHeaderPage.hasSubmissionTab();
        verifyAssignmentHeaderPage.hasEvaluationTab();
        verifyAssignmentHeaderPage.hasAssignmentTitleWithEvaluatedStatus(evaluatedAssignment);

        // Details Tab verifications
        assignmentHeaderToolPageAndroid.clickOnDetailsTab();
        verifyAssignmentDetailsPage.hasAssignmentDueDateAndRoomName(evaluatedAssignment, roomName, expectedDueDate);
        verifyAssignmentDetailsPage.hasDescription(evaluatedDesc);
        verifyAssignmentDetailsPage.hasUploadedFile(uploadedFileName);
        verifyAssignmentDetailsPage.displaysAssignmentIcon();
        verifyAssignmentDetailsPage.displaysIndividualIcon();
        verifyAssignmentDetailsPage.displaysMultipleTriesIcon();

        // Submission Tab verifications
        assignmentHeaderToolPageAndroid.clickOnSubmissionTab();
        verifyAssignmentSubmissionPageAdroid.hasSubmittedDate(todayDate);
        // Need to add test to verify comment

        // Evaluated Tab verifications
        assignmentHeaderToolPageAndroid.clickOnEvaluationTab();
        verifyAssignmentEvaluationPageAndroid.displaysGradeCommentAndDate(grade, evalComment, todayDate);

        verifyAssignmentEvaluationPageAndroid.hasInformatoryMessage(message);
        notificationViewToolAndroid.navigateBack();
    }
}
