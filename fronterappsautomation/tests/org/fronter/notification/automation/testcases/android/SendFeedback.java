package org.fronter.notification.automation.testcases.android;


import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases for sending feedback
 */
public class SendFeedback extends FronterAppsTestBase {

    String feedbackText = "Test";
    String feedbackEmail = "QA@test.com";

    @BeforeClass
    public void login() {
        loginToolAndroid.loginAsStudent1();
    }

    /**
     * Test Case to check give Feedback page fields
     */
    @Test
    public void giveFeedbackFields() {
        headerToolAndroid.openAccount();
        accountToolAndroid.clickOnGiveFeedback();

        verifyGiveFeedbackPage.hasTitleAsGiveFeeback();
        verifyGiveFeedbackPage.displaysPlaceHolderInEmail(testData.emailPlaceHolder);
        verifyGiveFeedbackPage.displaysMessage(testData.sendFeedbackMessage1);
        verifyGiveFeedbackPage.displaysMessage(testData.sendFeedbackMessage2);

        // Click send button without filling any field
        giveFeedbackTool.clickSendButton();
        verifyGiveFeedbackPage.displaysSendButton();
    }

    /**
     * Test case to send feedback
     *
     * Note: This test won't run since it's property is set to false so that we don't end up sending
     * many unnecessary feedbacks on production via automation, will enable it once we have robohydra in place.
     */
    @Test(dependsOnMethods = "giveFeedbackFields", enabled = false)
    public void sendFeedback() {
        giveFeedbackTool.inputFeedback(feedbackText, feedbackEmail);
        giveFeedbackTool.clickSendButton();

        verifyFeedbackConfirmationPage.displaysSendingFeedbackAlert();
        verifyFeedbackConfirmationPage.displaysProgressBarWithPleaseWaitText();

        verifyFeedbackConfirmationPage.displaysMessage(testData.confirmationFeedbackMessage1);
        verifyFeedbackConfirmationPage.displaysMessage(testData.confirmationFeedbackMessage2);
        verifyFeedbackConfirmationPage.displaysSentFeedbackText(feedbackText);
        verifyFeedbackConfirmationPage.displaysSentFeedbackEmail(feedbackEmail);

        feedbackConfirmationTool.clickSendMoreFeedbackLink();
        verifyGiveFeedbackPage.displaysSendButton();
    }
}
