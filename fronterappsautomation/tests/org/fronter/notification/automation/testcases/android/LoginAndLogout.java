package org.fronter.notification.automation.testcases.android;


import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.testng.annotations.Test;

/**
 * This class contains test cases for login and logging out
 */
public class LoginAndLogout extends FronterAppsTestBase {

    /**
     * Test Case to Login and verify User first name, last name and logout
     */
    @Test
    public void loginAndLogout() {
        String[] institutionList = {testData.installation1, testData.installation2};
        loginToolAndroid.loginAsStudent1();
        headerToolAndroid.openAccount();
        verifyAccountPageAndroid.hasFirstAndLastname(testData.student1FirstName, testData.student1LastName);
        accountToolAndroid.clickOnInstitution();
        verifyAccountPageAndroid.hasInstitutionList(institutionList);
        accountToolAndroid.clickOnInstitutionName(institutionList[0]);
        verifyAccountPageAndroid.hasInstitutionName(institutionList[0]);
        accountToolAndroid.clickOnLogout();
        verifyAccountPageAndroid.hasLogoutConfirmationMsg(testData.logoutConfirmationMsg);
        accountToolAndroid.cancelLogout();
        verifyAccountPageAndroid.hasFronterLogo();
        headerToolAndroid.openAccount();
        accountToolAndroid.logout();
    }

    /**
     * Test case to verify userid with invalid credentials
     */
    @Test(dependsOnMethods = "loginAndLogout")
    public void loginWithWrongUserId() {
        loginToolAndroid.loginWithInvalidCrendentials();
        verifyLoginPageAndroid.hasMessageDisplayed(testData.loginErrorMessage);
    }
}
