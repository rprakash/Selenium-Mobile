package org.fronter.notification.automation.testcases.android;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases for reading News on News list and View Page
 */
public class ReadNews extends FronterAppsTestBase {

    private String newsHeading1, newsContent1;
    private String newsHeading2;
    private String roomName, authorFirstName, authorLastName;
    private MobileDateUtils dateUtils = new MobileDateUtils();
    private String presentDate = dateUtils.getTodayDateOnAndroid();
//    private String noMoreNews;

    @BeforeClass
    public void readTestData() {
        String[] key = {testData.newsHeadingKey, testData.newsContentKey, testData.newsHeading2Key, testData.newsContent2Key};
        String[] values = readWriteFile.readFromTestFile(key);
        newsHeading1 = values[0];
        newsContent1 = values[1];
        newsHeading2 = values[2];

//        noMoreNews = testData.noMoreNews;
        authorFirstName = testData.teacher1FirstName;
        authorLastName = testData.teacher1LastName;
        roomName = testData.roomName;
    }

    @BeforeClass
    public void login() {
        loginToolAndroid.loginAsStudent1();
    }

    /**
     * Test Case to read News Heading with Verify Room name and News Content on News list Page
     */
    @Test
    public void readNewsOnNewsList() {
        headerToolAndroid.openAccount();
        accountToolAndroid.clickOnNews();
        verifyNewsListPageAndroid.hasNewsHeadingWithNewsDescription(newsHeading1, newsContent1);
        verifyNewsListPageAndroid.hasRoomNameWithNews(newsHeading1, roomName);
        verifyNewsListPageAndroid.hasNewsWithDate(newsHeading1,presentDate);
        verifyNewsListPageAndroid.hasTodayLabel(newsHeading1);
        verifyNewsListPageAndroid.hasYesterdayLabel(newsHeading2);
        // Need to fix
        // verifyNewsListPageAndroid.displaysNoMoreNewsMsg(noMoreNews);
       }

    /**
     * Test Case to read News Heading and Verify Room name, Author Name and News Content on News Detailed View Page
     */
    @Test
    public void readNewsOnNewsView() {
        newsListToolAndroid.openNews(newsHeading1);
        verifyNewsViewPageAndroid.hasNewsHeadingWithNewsContent(newsHeading1, newsContent1);
        verifyNewsViewPageAndroid.hasRoomName(roomName);
        verifyNewsViewPageAndroid.hasAuthorName(authorFirstName, authorLastName);
        verifyNewsViewPageAndroid.hasImage();
        newsViewToolAndroid.clickOnImage();
        verifyNewsViewPageAndroid.hasFullImageWithDownloadButton();
        newsViewToolAndroid.navigateBack();
        newsViewToolAndroid.navigateBack();
    }
}
