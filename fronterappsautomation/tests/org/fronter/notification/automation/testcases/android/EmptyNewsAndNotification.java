package org.fronter.notification.automation.testcases.android;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.notification.worklist.android.VerifyWorklistPageAndroid;
import org.fronter.apps.automation.sdk.notification.worklist.android.WorklistToolAndroid;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test Case to verify 'No news' and 'No notification' messages if user not having any content
 */
public class EmptyNewsAndNotification extends FronterAppsTestBase {

    private VerifyWorklistPageAndroid verifyWorklistPageAndroid;
    private WorklistToolAndroid worklistToolAndroid;

    @BeforeClass
    public void instantiateObjects(){
        verifyWorklistPageAndroid = new VerifyWorklistPageAndroid(androidDriver);
        worklistToolAndroid = new WorklistToolAndroid(androidDriver);
    }

    /**
     * Verify 'No news' appear on news list and 'No notification' appear on notification list
     * for user having no news and notifications
     */
    @Test
    public void noNewsAndNotification() {
        loginToolAndroid.loginAsStudent2();
        verifyNewsListPageAndroid.hasEmptyNews();
        newsListToolAndroid.tapOnNotificationCounter();
        verifyNotificationListPageAndroid.hasEmptyNotification();
        notificationViewToolAndroid.clickBackButtonOnPhone();
        headerToolAndroid.openAccount();
        accountToolAndroid.clickOnWorklist();
        worklistToolAndroid.clickOnNotSubmittedTab();
        verifyWorklistPageAndroid.hasMessage(testData.noAssignment);
        headerToolAndroid.openAccount();
        accountToolAndroid.logout();
    }
}
