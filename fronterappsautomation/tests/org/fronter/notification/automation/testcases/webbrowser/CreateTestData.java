package org.fronter.notification.automation.testcases.webbrowser;

import org.fronter.apps.automation.sdk.FronterTestBase;
import org.fronter.apps.automation.sdk.login.LoginTool;
import org.fronter.automation.sdk.DateUtils;
import org.fronter.automation.sdk.Wait;
import org.fronter.automation.sdk.modules.room.RoomPageNavigator;
import org.fronter.automation.sdk.modules.room.newstool.NewsTool;
import org.fronter.automation.sdk.modules.room.teststudio.TestTool;
import org.fronter.automation.sdk.modules.room.teststudio.TestToolNavigator;
import org.fronter.automation.sdk.modules.tools.assignment.TeacherAssignmentTool;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Create News, Test and Assignment to be used as test data for notification
 */
public class CreateTestData extends FronterTestBase {

    private RoomPageNavigator navigateFromRoomPage;
    private NewsTool newsTool;
    private TeacherAssignmentTool teacherAssignmentTool;
    private TestTool testTool;
    private TestToolNavigator navigateFromTestTool;

    private String randomNumber1 = mobileDateUtils.getRandomNumber();
    private String randomNumber2 = mobileDateUtils.getRandomNumber();
    private String randomNumber3 = mobileDateUtils.getRandomNumber();
    private String randomNumber4 = mobileDateUtils.getRandomNumber();
    private String randomNumber5 = mobileDateUtils.getRandomNumber();

    private String newsHeading1 = testData.newsHeading + " " + randomNumber1;
    private String newsContent1 = testData.newsContent + " " + randomNumber1;
    private String newsHeading2 = testData.newsHeading + " " + randomNumber2;
    private String newsContent2 = testData.newsContent + " " + randomNumber2;
    private String testHeading = testData.testHeading + " " + randomNumber1;
    private String testDescription = testData.testDescription + " " + randomNumber1;

    private String timeBoundAssignment = testData.timeBoundAssign + " " + randomNumber1;
    private String timeBoundAssignmentDesc = testData.assignmentDescription + " " + randomNumber1;

    private String submittedAssignment = testData.submittedAssignmentHead + " " + randomNumber2;
    private String submittedAssignmentDesc = testData.assignmentDescription + " " + randomNumber2;

    private String evaluatedAssignment = testData.evaluatedAssignmentHead + " " + randomNumber3;
    private String evaluatedAssignmentDesc = testData.assignmentDescription + " " + randomNumber3;

    private String twoDaysAssignment = testData.twoDaysAssign + " " + randomNumber4;
    private String twoDaysAssignmentDesc = testData.assignmentDescription + " " + randomNumber4;

    private String alwaysOpenAssignment = testData.alwaysOpenAssign + " " + randomNumber5;
    private String alwaysOpenAssignmentDesc = testData.assignmentDescription + " " + randomNumber5;

    private String uploadFilePath = testData.uploadFilePath;
    private String studentFullName = testData.student1LastName + ", " + testData.student1FirstName;

    private String roomName = testData.roomName;

    private LoginTool loginTool;
    private DateUtils dateUtils = new DateUtils();
    private String yesterdayDate = dateUtils.getDateIncrementedBy(-1);
    private String startDate = dateUtils.getTodaysDate();
    private String dateAfterTwoDays = dateUtils.getDateIncrementedBy(2);
    private String closingDate = startDate;
    private String openTime = dateUtils.getCurrentOsloTime();
    private String closeTime = dateUtils.getSpecifiedTimeIncrementedOrDecremented(openTime, 180);
    private Wait wait;

    @BeforeClass
    public void instantiateSdkObjects() {
        navigateFromRoomPage = new RoomPageNavigator(driver);
        newsTool = new NewsTool(driver);
        teacherAssignmentTool = new TeacherAssignmentTool(driver);
        testTool = new TestTool(driver);
        navigateFromTestTool = new TestToolNavigator(driver);
        loginTool = new LoginTool(driver);
        wait = new Wait(driver);
    }

    @BeforeClass(dependsOnMethods = {"instantiateSdkObjects"})
    public void loginAndGoToRoom() {
        loginTool.login(testData.teacher1UserName, testData.teacher1Password);
        navigate.toRoom(roomName);
    }

    /**
     * Creates an assignment which is due for less than 1 day, this assignment is not submitted by any student
     */
    @Test(priority = 0)
    public void assignmentDueDateWithLessThanOneDay() {
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.createNewAssignment(timeBoundAssignment, timeBoundAssignmentDesc, uploadFilePath);
        waitForApplicationHeader();
        teacherAssignmentTool.selectOpeningDatesOnAssigningPage(startDate, closingDate);
        teacherAssignmentTool.selectOpeningTimesOnAssigningPage(null, closeTime);
        teacherAssignmentTool.completeAssignment();
    }

    /**
     * Creates an always open (one try and group) assignment, this assignment is not submitted by any student
     */
    @Test(priority = 1)
    public void alwaysOpenAssignment() {
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.createNewAssignment(alwaysOpenAssignment, alwaysOpenAssignmentDesc, uploadFilePath);
        waitForApplicationHeader();
        teacherAssignmentTool.selectAlwaysOpen();
        teacherAssignmentTool.selectOneAttemptCheckBox();
        teacherAssignmentTool.selectGroupAssignmentAndComplete();
    }

    /**
     * Creates an assignment (multiple tries and individual) with 2 days (due), this assignment is submitted by teacher on behalf of student
     */
    @Test(priority = 2)
    public void submitAssignment() {
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.createNewAssignment(submittedAssignment, submittedAssignmentDesc, uploadFilePath);
        waitForApplicationHeader();
        teacherAssignmentTool.selectOpeningDatesOnAssigningPage(startDate, dateAfterTwoDays);
        teacherAssignmentTool.completeAssignment();
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.openAssignment(submittedAssignment);
        teacherAssignmentTool.openGroupAssignmentForStudent(studentFullName);
        teacherAssignmentTool.deliverOnBehalfOfStudent(uploadFilePath);
        navigateFromRoomPage.toAssignmentPageForTeacher();
    }

    /**
     * Creates an assignment with 2 days (due), this assignment is evaluated by teacher
     */
    @Test(priority = 3)
    public void evaluateAssignment() {
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.createNewAssignment(evaluatedAssignment, evaluatedAssignmentDesc, uploadFilePath);
        waitForApplicationHeader();
        teacherAssignmentTool.selectOpeningDatesOnAssigningPage(startDate, dateAfterTwoDays);
        teacherAssignmentTool.completeAssignment();
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.openAssignment(evaluatedAssignment);
        teacherAssignmentTool.openGroupAssignmentForStudent(studentFullName);
        teacherAssignmentTool.deliverOnBehalfOfStudent(uploadFilePath);
        teacherAssignmentTool.evaluateAssignment("Good", "A");
        teacherAssignmentTool.saveEvaluation();
        waitForEvaluatioinMessage();
    }

    /**
     * Creates an assignment with 2 days (due), this assignment is not submitted by any student
     */
    @Test(priority = 4)
    public void notSubmittedAssignment() {
        navigateFromRoomPage.toAssignmentPageForTeacher();
        teacherAssignmentTool.createNewAssignment(twoDaysAssignment, twoDaysAssignmentDesc, uploadFilePath);
        waitForApplicationHeader();
        teacherAssignmentTool.selectOpeningDatesOnAssigningPage(startDate, dateAfterTwoDays);
        teacherAssignmentTool.completeAssignment();
        navigateFromRoomPage.toAssignmentPageForTeacher();
    }

    /**
     * Create News
     */
    @Test(priority = 5)
    public void createNews() {
        navigateFromRoomPage.toNewsTool();
        newsTool.createNewsWithImageFromMachine(newsHeading1, newsContent1, testData.imageFilePath, testData.imageFileTitle);
        newsTool.createNewsWithDates(newsHeading2, newsContent2, yesterdayDate, null);
    }

    /**
     * Create Test
     */
    @Test(priority = 6)
    public void createTest() {
        navigateFromRoomPage.toTestStudio();
        navigateFromTestTool.toCreateNewTestPage();
        testTool.createNewTest(testHeading, testDescription);
        testTool.addTextTypeQuestion("Sample text");
        navigateFromTestTool.toSettingsTab();
        testTool.activateCurrentTest();
        navigateFromRoomPage.toTestStudio();
    }

    /**
     * Write test data in properties file
     */
    @AfterClass
    public void writeTestDataInFile() {
        String newsHeadingKey = testData.newsHeadingKey;
        String newsContentKey = testData.newsContentKey;
        String newsHeading2Key = testData.newsHeading2Key;
        String newsContent2Key = testData.newsContent2Key;

        String testHeadingKey = testData.testHeadingKey;
        String testDescriptionKey = testData.testDescriptionKey;

        String timeBoundAssignmentKey = testData.timeBoundAssignmentKey;
        String timeBoundAssignmentDescKey = testData.timeBoundAssignmentDescKey;
        String submittedAssignmentKey = testData.submittedAssignmentKey;
        String submittedAssignmentDescKey = testData.submittedAssignmentDescKey;
        String evaluatedAssignmentKey = testData.evaluatedAssignmentKey;
        String evaluatedAssignmentDescKey = testData.evaluatedAssignmentDescKey;
        String twoDaysAssignmentKey = testData.twoDaysAssignmentKey;
        String twoDaysAssignmentDescKey = testData.twoDaysAssignmentDescKey;
        String alwaysOpenAssignmentKey = testData.alwaysOpenAssignmentKey;
        String alwaysOpenAssignmentDescKey = testData.alwaysOpenAssignmentDescKey;

        String closingTimeOfAssignmentKey = testData.closingTimeOfAssignmentKey;

        String[] key =
                {newsHeadingKey, newsContentKey, newsHeading2Key, newsContent2Key, timeBoundAssignmentKey,
                        timeBoundAssignmentDescKey, submittedAssignmentKey, submittedAssignmentDescKey, evaluatedAssignmentKey,
                        evaluatedAssignmentDescKey, twoDaysAssignmentKey, twoDaysAssignmentDescKey, alwaysOpenAssignmentKey,
                        alwaysOpenAssignmentDescKey, testHeadingKey, testDescriptionKey, closingTimeOfAssignmentKey};
        String[] value =
                {newsHeading1, newsContent1, newsHeading2, newsContent2, timeBoundAssignment, timeBoundAssignmentDesc,
                        submittedAssignment, submittedAssignmentDesc, evaluatedAssignment, evaluatedAssignmentDesc,
                        twoDaysAssignment, twoDaysAssignmentDesc, alwaysOpenAssignment, alwaysOpenAssignmentDesc, testHeading,
                        testDescription, closeTime};

        readWriteFile.writeInTestFile(key, value);
    }

    /**
     * Wait until application header class is displayed
     *
     * @note This method should be moved inside jar file and removed from here
     */
    public void waitForApplicationHeader() {
        wait.untilElementIsVisible(driver.findElement(By.className("applicationHeader")));
    }

    /**
     * Wait until 'Your evaluation was successfully saved' message is displayed
     *
     * @note This method should be moved inside jar file and removed from here
     */
    public void waitForEvaluatioinMessage() {
        wait.untilElementIsVisible(driver.findElement(By.xpath("//*[contains(text(),'Your evaluation was successfully saved')]")));
    }
}
