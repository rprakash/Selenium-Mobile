package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.fronter.apps.automation.sdk.notification.ios.LoginToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NewsListToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NewsViewToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNewsListPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNewsViewPageIOS;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ReadNews extends FronterAppsTestBase {
    private NewsListToolIOS notificationNewsListToolIOS;
    private VerifyNewsListPageIOS verifyNewsListPageIOS;
    private VerifyNewsViewPageIOS verifyNewsViewPageIOS;
    private NewsViewToolIOS notificationNewsViewToolIOS;
    private LoginToolIOS notificationIOSLoginTool;
    public String newsHeading1, newsContent1;
    public String newsHeading2, newsContent2;
    public String labelToday = testData.newsTodayLabel;
    MobileDateUtils dateUtils = new MobileDateUtils();
    private String presentDate = dateUtils.getTodayDateOnIOS();

    @BeforeClass
    public void readTestData() {
        String[] key = {testData.newsHeadingKey, testData.newsContentKey, testData.newsHeading2Key, testData.newsContent2Key};
        String[] values = readWriteFile.readFromTestFile(key);
        newsHeading1 = values[0];
        newsContent1 = values[1];
        newsHeading2 = values[2];
        newsContent2 = values[3];
    }

    @BeforeClass
    protected void initializeSDKObjects() {
        notificationNewsListToolIOS = new NewsListToolIOS(iOSDriver);
        verifyNewsListPageIOS = new VerifyNewsListPageIOS(iOSDriver);
        verifyNewsViewPageIOS = new VerifyNewsViewPageIOS(iOSDriver);
        notificationNewsViewToolIOS = new NewsViewToolIOS(iOSDriver);
        notificationIOSLoginTool = new LoginToolIOS(iOSDriver);
    }

    /**
     * Test Case to Login using keyboard 'Go' button
     */
    @Test
    public void loginWithGoButton() {
        notificationIOSLoginTool.enterInstallationName(testData.installation1);
        notificationIOSLoginTool.selectFronterLoginProvider();
        notificationIOSLoginTool.loginWithGoButton(testData.student1UserName, testData.student1Password);
    }

    /**
     * Test Case to read Latest News and Verify Room name on News list Page
     */
    @Test
    public void readNewsOnNewsList() {
        verifyNewsListPageIOS.displaysNewsWithRoomName(newsHeading1, testData.roomName);
        verifyNewsListPageIOS.hasNewsWithDate(newsHeading1, presentDate);
        verifyNewsListPageIOS.hasTodayLabel(newsHeading1);
        verifyNewsListPageIOS.hasYesterdayLabel(newsHeading2);
        // notificationNewsListToolIOS.scrollToBottom();
        verifyNewsListPageIOS.displaysMessage(testData.newsMessage);
    }

    /**
     * Test Case to read Latest News and Verify Room name on News Detailed View Page
     */
    @Test
    public void openNews() {
        notificationNewsListToolIOS.openNews(newsHeading1);
        verifyNewsViewPageIOS.displaysNews(newsHeading1);
        verifyNewsViewPageIOS.hasRoomName(testData.roomName);
        verifyNewsViewPageIOS.hasAuthorName(testData.teacher1FirstName, testData.teacher1LastName);
        notificationNewsViewToolIOS.tapOnImage();
        verifyNewsViewPageIOS.hasFullImageViewWithSaveButton();
        notificationNewsViewToolIOS.navigateBack();
        notificationNewsViewToolIOS.navigateBack();
    }
}
