package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.testng.annotations.Test;

/**
 * This class contains test cases for login and logging out
 */
public class LoginAndLogout extends FronterAppsTestBase {

    /**
     * Test Case to Login and verify User first name, last name and logout
     */
    @Test
    public void Login() {
        loginToolIOS.enterInstallationName(testData.installation1);
        loginToolIOS.selectFronterLoginProvider();
        loginToolIOS.login(testData.student1UserName, testData.student1Password);
        newsListToolIOS.openAccount();
        verifyAccountPageIOS.hasFirstAndLastname(testData.student1FirstName, testData.student1LastName);
        accountToolIOS.logout();
    }

    /**
     * Test case to verify error message appears when user enter invalid credentials
     */
    @Test(dependsOnMethods = "Login")
    public void loginWithWrongUserId() {
        loginToolIOS.enterInstallationName(testData.installation1);
        loginToolIOS.selectFronterLoginProvider();
        loginToolIOS.login(testData.invalidUserName, testData.invalidPassword);
        verifyLoginPageIOS.hasMessageDisplayed(testData.loginErrorMessage);
    }
}
