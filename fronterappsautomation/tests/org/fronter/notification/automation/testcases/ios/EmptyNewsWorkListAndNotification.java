package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.notification.ios.AccountToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.LoginToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NewsListToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNotificationListPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.VerifyWorkListPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.WorkListToolIOS;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test Case to verify 'No news', 'No Assignment' and 'No notification' messages if user not having any content
 */
public class EmptyNewsWorkListAndNotification extends FronterAppsTestBase {
    private LoginToolIOS notificationIOSLoginTool;
    private WorkListToolIOS workListToolIOS;
    private VerifyWorkListPageIOS verifyWorkListPageIOS;
    private NewsListToolIOS notificationNewsListToolIOS;
    private VerifyNotificationListPageIOS verifyNotificationListPageIOS;
    private AccountToolIOS accountPageIOS;

    @BeforeClass
    protected void initializeSDKObjects() {
        notificationIOSLoginTool = new LoginToolIOS(iOSDriver);
        workListToolIOS = new WorkListToolIOS(iOSDriver);
        verifyWorkListPageIOS = new VerifyWorkListPageIOS(iOSDriver);
        notificationNewsListToolIOS = new NewsListToolIOS(iOSDriver);
        verifyNotificationListPageIOS = new VerifyNotificationListPageIOS(iOSDriver);
        accountPageIOS = new AccountToolIOS(iOSDriver);
    }

    /**
     * Verify that 'No News' appears on news list, 'No Assignment' appears on worklist and "No Notification" appears on
     * notification page
     */
    @Test
    public void noNewsWorkListAndNotification() {
        notificationIOSLoginTool.enterInstallationName(testData.installation1);
        notificationIOSLoginTool.selectFronterLoginProvider();
        notificationIOSLoginTool.loginWithGoButton(testData.student2UserNameWithNoData, testData.student2Password);
        verifyNewsListPageIOS.displaysNoNewsMessage();

        workListToolIOS.tapOnWorkListIcon();
        workListToolIOS.clickOnNotSubmittedTab();
        verifyWorkListPageIOS.displaysNoAssignmentMessage();

        workListToolIOS.clickOnSubmittedTab();
        verifyWorkListPageIOS.displaysNoAssignmentMessage();

        workListToolIOS.clickOnEvaluatedTab();
        verifyWorkListPageIOS.displaysNoAssignmentMessage();

        notificationNewsListToolIOS.tapOnNotificationIcon();
        verifyNotificationListPageIOS.displaysNoNotificationsMessage();
    }

    @AfterClass
    protected void logout() {
        accountPageIOS.tapOnAccountIcon();
        accountPageIOS.logout();
    }

}
