package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.notification.ios.NewsListToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NotificationListToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.NotificationViewToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNotificationListPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyNotificationViewPageIOS;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Read News, Tests and Assignment Notification from Notification List and View Page
 */
public class ReadNotification extends FronterAppsTestBase {
    private NewsListToolIOS notificationNewsListToolIOS;
    private VerifyNotificationListPageIOS verifyNotificationListPageIOS;
    private NotificationListToolIOS notificationListToolIOS;
    private VerifyNotificationViewPageIOS verifyNotificationViewPageIOS;
    private NotificationViewToolIOS notificationViewToolIOS;
    private String roomName = testData.roomName;
    private String newsHeading, newsContent;
    private String assignmentNotification, assignmentDescription;
    private String testNotification, testDescription;

    @BeforeClass
    public void readTestData() {
        String newsHeadingKey = testData.newsHeadingKey;
        String newsContentKey = testData.newsContentKey;
        String testHeadingKey = testData.testHeadingKey;
        String testDescriptionKey = testData.testDescriptionKey;
        String assignmentHeadingKey = testData.timeBoundAssignmentKey;
        String assignmentDescriptionKey = testData.timeBoundAssignmentDescKey;

        String[] key = {newsHeadingKey, newsContentKey, assignmentHeadingKey, assignmentDescriptionKey, testHeadingKey,
                testDescriptionKey};
        String[] values = readWriteFile.readFromTestFile(key);
        newsHeading = values[0];
        newsContent = values[1];
        assignmentNotification = values[2];
        assignmentDescription = values[3];
        testNotification = values[4];
        testDescription = values[5];
    }

    @BeforeClass
    protected void initializeSDKObjects() {
        notificationNewsListToolIOS = new NewsListToolIOS(iOSDriver);
        verifyNotificationListPageIOS = new VerifyNotificationListPageIOS(iOSDriver);
        notificationListToolIOS = new NotificationListToolIOS(iOSDriver);
        verifyNotificationViewPageIOS = new VerifyNotificationViewPageIOS(iOSDriver);
        notificationViewToolIOS = new NotificationViewToolIOS(iOSDriver);
    }

    /**
     * Test Case to Verify Test, Assignment and News Notification on Notification List page
     */
    @Test
    public void readNotificationOnNotificationList() {
        notificationNewsListToolIOS.tapOnNotificationIcon();
        verifyNotificationListPageIOS.displaysTestNotificationWithRoomName(testNotification, roomName);
        verifyNotificationListPageIOS.displaysAssignmentNotificationWithRoomName(assignmentNotification, roomName);
        verifyNotificationListPageIOS.displaysNewsNotificationWithRoomName(newsHeading, roomName);
    }

    /**
     * Test Case to Verify Test and Assignment and News Notification on detailed view page
     */
    @Test(dependsOnMethods = "readNotificationOnNotificationList")
    public void readNotificationOnNotificationView() {
        notificationListToolIOS.openTestNotification(testNotification);
        verifyNotificationViewPageIOS.hasTestNotification(testNotification);
        verifyNotificationViewPageIOS.hasTestDescription(testDescription);
        verifyNotificationViewPageIOS.hasRoomName(roomName);
        verifyNotificationViewPageIOS.hasNoDueDateMsg();
        notificationViewToolIOS.navigateBack();

        notificationListToolIOS.openAssignmentNotification(assignmentNotification);
        verifyNotificationViewPageIOS.hasAssignmentNotification(assignmentNotification);
        verifyNotificationViewPageIOS.hasAssignmentDescription(assignmentDescription);
        verifyNotificationViewPageIOS.hasRoomName(roomName);
        notificationViewToolIOS.navigateBack();

        notificationListToolIOS.openNewsNotification(newsHeading);
        verifyNotificationViewPageIOS.hasNewsNotification(newsHeading);
        verifyNotificationViewPageIOS.hasNewsDescription(newsContent);
        verifyNotificationViewPageIOS.hasRoomName(roomName);
        verifyNotificationViewPageIOS.hasAuthorName(testData.teacher1FirstName, testData.teacher1LastName);
        notificationViewToolIOS.tapOnImage();
        verifyNotificationViewPageIOS.hasFullImageViewWithSaveButton();
        notificationViewToolIOS.navigateBack();
        notificationViewToolIOS.navigateBack();
        notificationViewToolIOS.scrollToBottom();
        verifyNotificationListPageIOS.displaysMessage(testData.notificationMessage);
    }
}
