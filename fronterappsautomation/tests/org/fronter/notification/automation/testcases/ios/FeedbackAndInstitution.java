package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.notification.ios.AccountToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.FeedbackToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.LoginToolIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyAccountPageIOS;
import org.fronter.apps.automation.sdk.notification.ios.VerifyFeedbackPageIOS;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases for verifying the institution name and sending feedback
 */
public class FeedbackAndInstitution extends FronterAppsTestBase {
    private LoginToolIOS notificationIOSLoginTool;
    private AccountToolIOS accountPageIOS;
    private VerifyAccountPageIOS verifyAccountPageIOS;
    private FeedbackToolIOS feedbackPageIOS;
    private VerifyFeedbackPageIOS verifyFeedbackPageIOS;
    private String feedbackText = testData.feedbackText;
    private String senderEmail = testData.feedbackEmail;

    String[] institutionName = {testData.installation1, testData.installation2};

    @BeforeClass
    protected void initializeSDKObjects() {
        notificationIOSLoginTool = new LoginToolIOS(iOSDriver);
        accountPageIOS = new AccountToolIOS(iOSDriver);
        verifyAccountPageIOS = new VerifyAccountPageIOS(iOSDriver);
        feedbackPageIOS = new FeedbackToolIOS(iOSDriver);
        verifyFeedbackPageIOS = new VerifyFeedbackPageIOS(iOSDriver);
    }

    @BeforeClass
    public void login() {
        notificationIOSLoginTool.enterInstallationName(testData.installation1);
        notificationIOSLoginTool.selectFronterLoginProvider();
        notificationIOSLoginTool.loginWithGoButton(testData.student1UserName, testData.student1Password);
    }

    /**
     * Test case to verify that user is able to change institution and send feedback
     */
    @Test
    public void changeInstitutionAndSendFeedback() {
        accountPageIOS.tapOnAccountIcon();
        verifyAccountPageIOS.hasInstitutionName(institutionName);

        accountPageIOS.tapOnInstitutionName(institutionName[0]);
        verifyAccountPageIOS.displaysInstitutionChangeConfirmation();
        accountPageIOS.changeInstitution();

        accountPageIOS.tapOnFeedbackLink();
        verifyFeedbackPageIOS.hasFeedbackTitle();

        feedbackPageIOS.sendFeedback();
        verifyFeedbackPageIOS.displaysErrorMessage();
        feedbackPageIOS.clickOkButton();

        feedbackPageIOS.inputFeedback(senderEmail, feedbackText);
        feedbackPageIOS.sendFeedback();
        verifyFeedbackPageIOS.displaysConfirmationMessage();
        feedbackPageIOS.cancelFeedback();
        feedbackPageIOS.navigateBack();
        accountPageIOS.logout();
    }
}
