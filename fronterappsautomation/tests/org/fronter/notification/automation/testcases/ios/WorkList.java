package org.fronter.notification.automation.testcases.ios;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.fronter.apps.automation.sdk.notification.worklist.ios.AssignmentDetailsToolIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.VerifyAssignmentDetailsPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.VerifyAssignmentEvaluationPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.VerifyAssignmentSubmissionPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.VerifyWorkListPageIOS;
import org.fronter.apps.automation.sdk.notification.worklist.ios.WorkListToolIOS;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases for viewing submitted ,not sumbitted and evaluated assignment list and detail view
 */
public class WorkList extends FronterAppsTestBase {
    private WorkListToolIOS workListToolIOS;
    private VerifyWorkListPageIOS verifyWorkListPageIOS;
    private VerifyAssignmentDetailsPageIOS verifyAssignmentDetailsPageIOS;
    private AssignmentDetailsToolIOS assignmentDetailsToolIOS;
    private VerifyAssignmentSubmissionPageIOS verifyAssignmentSubmissionPageIOS;
    private VerifyAssignmentEvaluationPageIOS verifyAssignmentEvaluationPageIOS;
    MobileDateUtils dateUtils = new MobileDateUtils();
    private String notSubmittedAssignmentTitle, notSubmittedAssignmentDesc;
    private String timeBoundAssignment;
    private String alwaysOpenAssignment;
    private String assignmentClosingTime;
    private String submittedAssignmentTitle, submittedAssignmentDesc;
    private String evaluatedAssignmentTitle, evaluatedAssignmentDesc;
    public String dueDays = "2";
    private String roomName = testData.roomName;
    private String assignmentGrade = testData.grade;
    private String evaluationComment = testData.evalComment;
    private String uploadedFile = testData.fileName;
    // The date format was changed in the app but we might need this if the date format is changed again
    // private String todayDate = dateUtils.getTodayDateInSimpleFormatOnIOS();
    // private String duedate = dateUtils.getDateIncrementedBy(2, "dd/MM/yy");
    private String detailViewDueDate = dateUtils.getDateIncrementedBy(2, "dd-MMM-yyyy");
    private String assignmentSubmissionDate = dateUtils.getTodayDateOnIOS();


    @BeforeClass
    public void readTestData() {

        String assignmentHeadingKey = testData.twoDaysAssignmentKey;
        String assignmentHeading2Key = testData.timeBoundAssignmentKey;
        String assignmentHeading3Key = testData.alwaysOpenAssignmentKey;
        String assignmentHeading4Key = testData.submittedAssignmentKey;
        String assignmentHeading5Key = testData.evaluatedAssignmentKey;

        String assignmentDescKey = testData.twoDaysAssignmentDescKey;
        String assignmentDesc4Key = testData.submittedAssignmentDescKey;
        String assignmentDesc5Key = testData.evaluatedAssignmentDescKey;
        String assignmentClosingKey = testData.closingTimeOfAssignmentKey;


        String[] key = {assignmentHeadingKey, assignmentDescKey, assignmentHeading2Key, assignmentHeading3Key,
                assignmentHeading4Key, assignmentDesc4Key, assignmentHeading5Key, assignmentDesc5Key, assignmentClosingKey};
        String[] values = readWriteFile.readFromTestFile(key);
        notSubmittedAssignmentTitle = values[0];
        notSubmittedAssignmentDesc = values[1];
        timeBoundAssignment = values[2];
        alwaysOpenAssignment = values[3];
        submittedAssignmentTitle = values[4];
        submittedAssignmentDesc = values[5];
        evaluatedAssignmentTitle = values[6];
        evaluatedAssignmentDesc = values[7];
        assignmentClosingTime = values[8];
    }

    @BeforeClass
    protected void initializeSDKObjects() {
        workListToolIOS = new WorkListToolIOS(iOSDriver);
        verifyWorkListPageIOS = new VerifyWorkListPageIOS(iOSDriver);
        verifyAssignmentDetailsPageIOS = new VerifyAssignmentDetailsPageIOS(iOSDriver);
        assignmentDetailsToolIOS = new AssignmentDetailsToolIOS(iOSDriver);
        verifyAssignmentSubmissionPageIOS = new VerifyAssignmentSubmissionPageIOS(iOSDriver);
        verifyAssignmentEvaluationPageIOS = new VerifyAssignmentEvaluationPageIOS(iOSDriver);
    }

    /**
     * Test Case to verify that only not Submitted assignment is displayed in "Not Submitted" tab in worklist
     */
    @Test(priority = 0)
    public void notSubmittedAssignment() {
        workListToolIOS.tapOnWorkListIcon();
        verifyWorkListPageIOS.hasNotSubmittedTab();
        verifyWorkListPageIOS.hasSubmittedTab();
        verifyWorkListPageIOS.hasEvaluatedTab();
        workListToolIOS.clickOnNotSubmittedTab();

        verifyWorkListPageIOS.hasAssignmentUnderNotSubmittedTab(notSubmittedAssignmentTitle);
        verifyWorkListPageIOS.hasAssignmentWithDueDateAndRoomName(notSubmittedAssignmentTitle, roomName, detailViewDueDate);
        verifyWorkListPageIOS.displaysDueDaysLeftForAssignment(notSubmittedAssignmentTitle, dueDays);
        verifyWorkListPageIOS.displaysDueAtTime(timeBoundAssignment, assignmentClosingTime);
        verifyWorkListPageIOS.hasAssignmentWithNoDueDateAndRoomName(alwaysOpenAssignment, roomName);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderNotSubmittedTab(submittedAssignmentTitle);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderNotSubmittedTab(evaluatedAssignmentTitle);
    }

    /**
     * Test case to verify that not submitted assignment details are displayed
     */
    @Test(priority = 1)
    public void notSubmittedAssignmentDetailsView() {
        workListToolIOS.clickOnNotSubmittedTab();
        workListToolIOS.openAssignment(notSubmittedAssignmentTitle);
        verifyAssignmentDetailsPageIOS.hasDetailsTab();
        verifyAssignmentDetailsPageIOS.hasDisabledSubmissionTab();
        verifyAssignmentDetailsPageIOS.hasDisabledEvaluationTab();
        verifyAssignmentDetailsPageIOS.hasNotSubmittedAssignmentWithDate(notSubmittedAssignmentTitle, detailViewDueDate);
        verifyAssignmentDetailsPageIOS.displaysAssignmentIconText();
        verifyAssignmentDetailsPageIOS.displaysIndividualIconText();
        verifyAssignmentDetailsPageIOS.displaysMultipleTriesIconText();
        verifyAssignmentDetailsPageIOS.hasDescription(notSubmittedAssignmentDesc);
        verifyAssignmentDetailsPageIOS.hasUploadedFile(uploadedFile);
        workListToolIOS.navigateBack();
    }

    /**
     * Test case to verify that only Submitted assignment is displayed in "Submitted" tab in worklist
     */
    @Test(priority = 2)
    public void submittedAssignment() {
        workListToolIOS.tapOnWorkListIcon();
        workListToolIOS.clickOnSubmittedTab();
        verifyWorkListPageIOS.hasAssignmentWithSubmittedDateAndRoomName(submittedAssignmentTitle, roomName,
                assignmentSubmissionDate);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderSubmittedTab(notSubmittedAssignmentTitle);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderSubmittedTab(evaluatedAssignmentTitle);
    }

    /**
     * Test case to verify that submission detail view in worklist
     */
    @Test(priority = 3)
    public void submittedAssignmentDetailView() {
        workListToolIOS.clickOnSubmittedTab();
        workListToolIOS.openAssignment(submittedAssignmentTitle);
        verifyAssignmentDetailsPageIOS.hasDetailsTab();
        verifyAssignmentDetailsPageIOS.hasSubmissionTab();
        verifyAssignmentDetailsPageIOS.hasDisabledEvaluationTab();
        verifyAssignmentDetailsPageIOS.hasSubmittedAssignmentWithDate(submittedAssignmentTitle, detailViewDueDate);
        verifyAssignmentDetailsPageIOS.displaysAssignmentIconText();
        verifyAssignmentDetailsPageIOS.displaysIndividualIconText();
        verifyAssignmentDetailsPageIOS.displaysMultipleTriesIconText();
        verifyAssignmentDetailsPageIOS.hasDescription(submittedAssignmentDesc);
        verifyAssignmentDetailsPageIOS.hasUploadedFile(uploadedFile);
        assignmentDetailsToolIOS.clickOnSubmissionTab();
        verifyAssignmentSubmissionPageIOS.hasSubmittedDate(assignmentSubmissionDate);
        verifyAssignmentSubmissionPageIOS.hasUploadedFile(uploadedFile);
        workListToolIOS.navigateBack();
    }

    /**
     * Test Case to verify that only Evaluated assignment is displayed in "Evaluated" tab in worklist
     */
    @Test(priority = 4)
    public void evaluatedAssignment() {
        workListToolIOS.clickOnEvaluatedTab();
        verifyWorkListPageIOS.hasAssignmentWithEvaluationDateAndRoomName(evaluatedAssignmentTitle, roomName,
                assignmentSubmissionDate);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderEvaluatedTab(notSubmittedAssignmentTitle);
        verifyWorkListPageIOS.doesNotHaveAssignmentUnderEvaluatedTab(submittedAssignmentTitle);
    }

    /**
     * Test case to verify that evaluation detail view in worklist
     */
    @Test(priority = 5)
    public void evaluatedAssignmentDetailView() {
        workListToolIOS.clickOnEvaluatedTab();
        workListToolIOS.openAssignment(evaluatedAssignmentTitle);
        verifyAssignmentDetailsPageIOS.hasDetailsTab();
        verifyAssignmentDetailsPageIOS.hasSubmissionTab();
        verifyAssignmentDetailsPageIOS.hasEvaluationTab();
        verifyAssignmentDetailsPageIOS.hasEvaluatedAssignmentWithDate(evaluatedAssignmentTitle, detailViewDueDate);
        verifyAssignmentDetailsPageIOS.displaysAssignmentIconText();
        verifyAssignmentDetailsPageIOS.displaysIndividualIconText();
        verifyAssignmentDetailsPageIOS.displaysMultipleTriesIconText();
        verifyAssignmentDetailsPageIOS.hasDescription(evaluatedAssignmentDesc);
        verifyAssignmentDetailsPageIOS.hasUploadedFile(uploadedFile);
        assignmentDetailsToolIOS.clickOnSubmissionTab();
        verifyAssignmentSubmissionPageIOS.hasSubmittedDate(assignmentSubmissionDate);
        verifyAssignmentSubmissionPageIOS.hasUploadedFile(uploadedFile);
        assignmentDetailsToolIOS.clickOnEvaluationTab();
        verifyAssignmentEvaluationPageIOS.hasEvaluationGradeAndComments(assignmentGrade, evaluationComment);
        verifyAssignmentEvaluationPageIOS.displaysMessage();
        // Need to add verification for files uploaded by teacher during evaluating assignment
        workListToolIOS.navigateBack();
    }
}
