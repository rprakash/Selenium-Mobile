package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelListTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelListPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyKolibriRegisterPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains test cases, Register as User and Send Message on channel and and also Verify Time Stamp
 */
public class RegisterAndSendMessage extends FronterAppsTestBase {
    private VerifyKolibriRegisterPage verifyKolibriRegisterPage;
    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private KolibriChannelListTool kolibriChannelListTool;
    private VerifyChannelListPage verifyChannelListPage;
    private String welcomeMessage;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    private String messageOnChannel;
    String randomText = Long.toString(new Date().getTime());
    MobileDateUtils dateUtils = new MobileDateUtils();
    private String presentDate = dateUtils.getTodayDateForSimulator();

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        kolibriChannelListTool = new KolibriChannelListTool(driver);
        verifyKolibriRegisterPage = new VerifyKolibriRegisterPage(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
        verifyChannelListPage = new VerifyChannelListPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        welcomeMessage = userData.getString("welcomeMessage");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");

        TestDataCollection channelData = testData.getCollection("Channel");
        messageOnChannel = channelData.getString("message");
    }

    /**
     * Test Case to verify Welcome page and Registration by entering user
     * details and verifying userName as channel name on Channel Page
     */
    @Test
    public void register() {
        verifyKolibriRegisterPage.hasWelcomeMessage(welcomeMessage);
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
        verifyChannelPage.hasChannelName(channelName);
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasChannelName(channelName);
    }

    /**
     * Send Message on the channel page and Verify on Channel page and Channel List page and also Verify Time Stamp on Channel
     * Page and Channel list
     */
    @Test(dependsOnMethods = "register")
    public void sendMessage() {
        kolibriChannelListTool.swipeOnChannel();
        kolibriChannelTool.sendMessage(messageOnChannel);
        String currentTime = kolibriChannelTool.getCurrentTimeOnSimulator();
        verifyChannelPage.hasMessage(messageOnChannel);
        verifyChannelPage.hasTimeStampWithUserName(presentDate, currentTime, channelName);
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasTimeStamp(currentTime);
        verifyChannelListPage.hasMessage(messageOnChannel);
    }
}
