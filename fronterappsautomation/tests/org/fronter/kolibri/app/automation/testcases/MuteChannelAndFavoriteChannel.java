package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This Class contains test cases Mute Channel and Favorite channel functionalities
 */
public class MuteChannelAndFavoriteChannel extends FronterAppsTestBase {
    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    String randomText = Long.toString(new Date().getTime());

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void register() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
    }

    /**
     * Enable Mute Channel function and verify Mute Icon present on channel page
     */
    @Test
    public void muteChannel() {
        kolibriChannelTool.tapOnSettings();
        kolibriChannelTool.tapOnMuteChannel();
        verifyChannelPage.hasMuteChannelEnabled();
    }

    /**
     * Enable Favorite Channel function and verify Favorite Icon present on channel page
     */
    @Test(dependsOnMethods = "muteChannel")
    public void favoriteChannel() {
        kolibriChannelTool.tapOnSettings();
        kolibriChannelTool.tapOnFavoriteChannel();
        verifyChannelPage.hasFavoriteChannelEnabled();
    }
}
