package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelListTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelListPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * The class contains test cases for Creating, Editing and Deleting Channels
 */
public class CreateEditAndDeleteChannel extends FronterAppsTestBase {

    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private KolibriChannelListTool kolibriChannelListTool;
    private VerifyChannelListPage verifyChannelListPage;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    private String newChannel;
    private String editChannel;
    String randomText = Long.toString(new Date().getTime());

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        kolibriChannelListTool = new KolibriChannelListTool(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
        verifyChannelListPage = new VerifyChannelListPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");

        TestDataCollection channelData = testData.getCollection("Channel");
        newChannel = channelData.getString("newChannelName");
        editChannel = channelData.getString("editedChannelName");
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void register() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
        kolibriChannelTool.tapOnBackButton();
    }

    /**
     * Create a channel and verify channel is created on Channel page and channel list page
     */
    @Test
    public void createChannel() {
        kolibriChannelListTool.tapOnAddIcon();
        kolibriChannelListTool.createNewChannel(newChannel);
        verifyChannelPage.hasNewChannelName(newChannel);
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasNewChannelName(newChannel);
    }

    /**
     * Edit Channel and verify channel is created on Channel page and channel list page
     */
    @Test(dependsOnMethods = "createChannel")
    public void editChannel() {
        kolibriChannelListTool.swipeOnNewChannel();
        kolibriChannelTool.tapOnSettings();
        kolibriChannelTool.editChannelName(editChannel);
        verifyChannelListPage.hasEditedChannelName(editChannel);
        kolibriChannelListTool.swipeOnNewChannel();
        verifyChannelPage.hasEditedChannelName(editChannel);
    }

    /**
     * Delete edited channel and verify on Channel Page and Channel List Page
     */
    @Test(dependsOnMethods = "editChannel")
    public void deleteChannel() {
        kolibriChannelTool.tapOnSettings();
        kolibriChannelTool.deleteChannel();
        verifyChannelListPage.doesNotHaveChannel(editChannel);
    }
}
