package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelListTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This Class Contains Joining Existing Channel Test case
 */
public class JoinChannel extends FronterAppsTestBase {
    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private KolibriChannelListTool kolibriChannelListTool;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    private String newChannel;
    String randomText = Long.toString(new Date().getTime());

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        kolibriChannelListTool = new KolibriChannelListTool(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");

        TestDataCollection channelData = testData.getCollection("Channel");
        newChannel = channelData.getString("newChannelName");
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void registerAndCreateChannel() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
        kolibriChannelTool.tapOnBackButton();
        kolibriChannelListTool.tapOnAddIcon();
        kolibriChannelListTool.createNewChannel(newChannel);
        kolibriChannelTool.tapOnBackButton();
    }

    /**
     * Join The channel which is created and verify joined channel is present
     */
    @Test
    public void joinChannel() {
        kolibriChannelListTool.swipeOnNewChannel();
        kolibriChannelListTool.joinChannel();
        verifyChannelPage.hasJoinedChannelName(newChannel);
    }
}
