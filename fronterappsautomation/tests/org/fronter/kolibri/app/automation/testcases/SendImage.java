package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelListPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This class contains Send Image on channel test case
 */
public class SendImage extends FronterAppsTestBase {
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private VerifyChannelListPage verifyChannelListPage;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    String randomText = Long.toString(new Date().getTime());

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        verifyChannelListPage = new VerifyChannelListPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void register() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
    }

    /**
     * Send Image Via Gallery on Channel Page
     */
    @Test
    public void sendImage() {
        kolibriChannelTool.sendImage();
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasImage();
    }
}
