package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.MobileDateUtils;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriAccountsTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelListTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyAccountsPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelListPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Class Contains test cases Account Editing, Edit UserName and Edit E-mail Id also Send Feedback
 */
public class ChangeAccountSettings extends FronterAppsTestBase {
    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private KolibriChannelListTool kolibriChannelListTool;
    private KolibriAccountsTool kolibriAccountsTool;
    private VerifyChannelListPage verifyChannelListPage;
    private VerifyAccountsPage verifyAccountsPage;
    private String emailId;
    private String completeEmailId;
    private String completeEditedEmailId;
    private String channelName;
    private String messageOnChannel;
    private String editEmail;
    private String editUserName;
    private String feedbackText;
    String randomText = Long.toString(new Date().getTime());
    MobileDateUtils dateUtils = new MobileDateUtils();
    private String presentDate = dateUtils.getTodayDateForSimulator();

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        kolibriChannelListTool = new KolibriChannelListTool(driver);
        kolibriAccountsTool = new KolibriAccountsTool(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
        verifyChannelListPage = new VerifyChannelListPage(driver);
        verifyAccountsPage = new VerifyAccountsPage(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");
        editEmail = userData.getString("editedEmailId") + randomText;
        completeEditedEmailId = editEmail + "@fronter.com";
        editUserName = userData.getString("editedUserScreenName");
        TestDataCollection channelData = testData.getCollection("Channel");
        messageOnChannel = channelData.getString("message");
        feedbackText = channelData.getString("feedBackMessage");
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void register() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
        kolibriChannelTool.tapOnBackButton();
    }

    /**
     * Edit User Email Id and Verify Edited E-mail Id on Accounts page
     */
    @Test
    public void changeEmailId() {
        kolibriChannelListTool.tapOnAccountSettings();
        kolibriAccountsTool.changeEmailId(completeEditedEmailId);
        verifyAccountsPage.hasEditedEmailId(completeEditedEmailId);
        kolibriChannelListTool.tapOnChannelsIcon();
    }

    /**
     * Edit UserName and Verify on Account settings, Channel Page and Channel List page
     */
    @Test
    public void changeUserName() {
        kolibriChannelListTool.tapOnAccountSettings();
        kolibriAccountsTool.changeUserName(editUserName);
        verifyAccountsPage.hasEditedUserScreenName(editUserName);
        kolibriChannelListTool.tapOnChannelsIcon();
        kolibriChannelListTool.swipeOnChannel();
        kolibriChannelTool.sendMessage(messageOnChannel);
        String currentTime = kolibriChannelTool.getCurrentTimeOnSimulator();
        verifyChannelPage.hasEditedUserName(presentDate, currentTime, editUserName);
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasEditedUserScreenName(editUserName);
    }

    /**
     * Send Feedback and Verify Feedback send message appears on alert box
     */
    @Test
    public void sendFeedback() {
        kolibriChannelListTool.tapOnAccountSettings();
        kolibriAccountsTool.sendFeedback(feedbackText);
        verifyAccountsPage.hasFeedbackAlert();
        kolibriAccountsTool.acceptConfirmationAlert();
    }
}
