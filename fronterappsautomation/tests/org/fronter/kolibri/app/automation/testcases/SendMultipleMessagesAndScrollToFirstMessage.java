package org.fronter.kolibri.app.automation.testcases;

import java.util.Date;

import org.fronter.apps.automation.sdk.FronterAppsTestBase;
import org.fronter.apps.automation.sdk.TestDataFilePath;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelListTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriChannelTool;
import org.fronter.apps.automation.sdk.kolibri.register.KolibriRegisterTool;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelListPage;
import org.fronter.apps.automation.sdk.kolibri.register.VerifyChannelPage;
import org.fronter.automation.sdk.TestDataCollection;
import org.fronter.automation.sdk.TestDataReader;
import org.fronter.automation.sdk.TestDataReaderFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * This Class contains Test cases Sending Multiple Different Messages and scrolling to the First or Top message
 */
public class SendMultipleMessagesAndScrollToFirstMessage extends FronterAppsTestBase {
    private VerifyChannelPage verifyChannelPage;
    private KolibriRegisterTool kolibriRegisterTool;
    private KolibriChannelTool kolibriChannelTool;
    private VerifyChannelListPage verifyChannelListPage;
    private KolibriChannelListTool kolibriChannelListTool;
    private String emailId;
    private String completeEmailId;
    private String channelName;
    private String messageA;
    private String messageB;
    private String messageC;
    private String messageD;
    private String messageE;
    private String[] messages;
    String randomText = Long.toString(new Date().getTime());

    @BeforeClass
    protected void initializeSDKObjects() {
        kolibriRegisterTool = new KolibriRegisterTool(driver);
        kolibriChannelTool = new KolibriChannelTool(driver);
        verifyChannelPage = new VerifyChannelPage(driver);
        verifyChannelListPage = new VerifyChannelListPage(driver);
        kolibriChannelListTool = new KolibriChannelListTool(driver);
    }

    @BeforeClass
    public void populateTestData() {
        TestDataReader testData = TestDataReaderFactory.getInstance().open(TestDataFilePath.USER_DETAILS);
        TestDataCollection userData = testData.getCollection("User");
        emailId = userData.getString("userEmailId") + randomText;
        completeEmailId = emailId + "@fronter.com";
        channelName = userData.getString("userScreenName");

        TestDataCollection channelData = testData.getCollection("Channel");
        messageA = channelData.getString("message1");
        messageB = channelData.getString("message2");
        messageC = channelData.getString("message3");
        messageD = channelData.getString("message4");
        messageE = channelData.getString("message5");
        messages = new String[] {messageA, messageB, messageC, messageD, messageE};
    }

    @BeforeClass(dependsOnMethods = "populateTestData")
    public void registerChannel() {
        kolibriRegisterTool.tapGetStarted();
        kolibriRegisterTool.enterUserDetails(completeEmailId, channelName);
    }

    /**
     * Send Multiple Different messages on the channel page and verify all messages
     */
    @Test
    public void SendMultipleDifferentMessages() {
        kolibriChannelTool.sendMultipleMessage(messages);
        verifyChannelPage.hasMultipleMessages(messages);
    }

    /**
     * Scroll to First message and verify last message on Channel Page exists on channel list page
     */
    @Test(dependsOnMethods = "SendMultipleDifferentMessages")
    public void ScrollToFirstMessage() {
        kolibriChannelTool.scrollToTopMessage();
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasMessage(messageE);
    }

    /**
     * Send Message in Multiple Line using return key and Verify that First line of the Message appears on Channel List page
     */
    @Test(dependsOnMethods = "ScrollToFirstMessage")
    public void SendMultipleLineMessage() {
        kolibriChannelListTool.swipeOnChannel();
        kolibriChannelTool.sendMultipleLineMessage(messages);
        kolibriChannelTool.tapOnBackButton();
        verifyChannelListPage.hasFirstLineMessage(messageA);
    }
}
