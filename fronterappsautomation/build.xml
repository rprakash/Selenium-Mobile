<?xml version="1.0" encoding="UTF-8"?>

<project name="Fronter Selenium suite" default="run" basedir=".">
    <property file="build.properties"/>
    <property name="compiler.out" value="${basedir}/bin"/>
    <property name="sdk.src" value="${basedir}/sdk"/>
    <property name="build.dir" value="${basedir}/build"/>
    <property name="tests.src" value="${basedir}/tests"/>
    <property name="tests.out" value="${basedir}/test-output"/>
    <property name="tests.report" value="${basedir}/report"/>
    <property name="docs.out" value="${basedir}/docs"/>
    <property name="testng.suites" value="${basedir}/testng-xml-suites"/>

    <target name="setClassPath" unless="classpath">
        <path id="classpathJars">
            <fileset dir="${basedir}/libs" includes="**/*.jar"/>
            <filelist>
                <file name="${compiler.out}"/>
            </filelist>
        </path>
        <pathconvert pathsep=":" property="classpath" refid="classpathJars"/>
    </target>

    <target name="init" depends="setClassPath">
        <taskdef resource="testngtasks" classpath="${classpath}"/>
    </target>

    <target name="clean" description="Removes generated files" unless="is_clean">
        <delete dir="${docs.out}"/>
        <delete dir="${tests.out}"/>
        <delete dir="${tests.report}"/>
        <property name="is_clean" value="true"/>
    </target>

    <target name="clean-all" depends="clean" description="Removes ALL the generated files, including the compiled files">
        <delete dir="${compiler.out}"/>
        <property name="is_clean" value="true"/>
    </target>

    <target name="compile-sdk" depends="init, clean" description="Compiles all the Java classes">
        <mkdir dir="${compiler.out}"/>
        <echo message="Compiling SDK..."/>
        <javac debug="true"
            srcdir="${sdk.src}"
            destdir="${compiler.out}"
            includes="**/*.java"
            target="1.7"
            classpath="${classpath}"
            includeantruntime="false">
        </javac>
    </target>

    <target name="compile-tests" depends="init, clean" description="Compiles all the Java classes">
        <echo message="Compiling tests..."/>
        <javac debug="true"
            srcdir="${tests.src}"
            destdir="${compiler.out}"
            includes="**/*.java"
            target="1.7"
            classpath="${classpath}"
            includeantruntime="false">
        </javac>
    </target>

    <target name="javadoc" depends="init" description="Generates API documentation">
        <delete dir="${docs.out}"/>
        <javadoc packagenames="org.fronter.automation.sdk.*"
            sourcepath="${sdk.src}"
            classpath="${classpath}"
            defaultexcludes="yes"
            destdir="${docs.out}"
            version="true"
            use="true"
            windowtitle="Fronter Selenium Automation API">
        </javadoc>
    </target>

    <target name="build" depends="compile-sdk" description="Exports the SDK in JAR files to be used in other projects">
        <fail unless="build.version" message="Version not set. Use ant build -Dbuild.version=xxx (where xxx is the version number)"/>
        <delete dir="${build.dir}"/>
        <mkdir dir="${build.dir}"/>

        <!-- Create a standard JAR file -->
        <jar destfile="${build.dir}/fronter-automation-sdk-${build.version}.jar" basedir="${compiler.out}" includes="**/*.class"></jar>

        <!-- Create a JAR file attaching the source (for debugging and javadocs) -->
        <jar destfile="${build.dir}/fronter-automation-sdk-${build.version}-sources.jar">
            <fileset dir="${compiler.out}" includes="**/*.class"/>
            <fileset dir="${sdk.src}" includes="**/*.java"/>
        </jar>

        <echo>${line.separator}</echo>
        <echo>===========================================================</echo>
        <echo>Build successful!</echo>
        <echo>Remember to commit the generated files and then</echo>
        <echo>create a tag for this version: git tag ${build.version}</echo>
        <echo>===========================================================</echo>
        <echo>${line.separator}</echo>
    </target>

    <target name="run" depends="compile-sdk, compile-tests" description="Run Selenium tests using TestNG">

        <!-- Run the tests -->
        <property name="suite" value="*"/>
        <testng classpath="${classpath}" parallel="none"
            listeners="org.fronter.apps.automation.sdk.ReportListener.class">

            <xmlfileset dir="${testng.suites}" includes="${suite}.xml"/>

            <!-- Properties that can be changed through build.properties or the command line -->
            <env key="url" value="${url}"/>
            <env key="firefox.binary" value="${firefox.binary}"/>
            <env key="invoke.android.driver" value="${invoke.android.driver}"/>
            <env key="invoke.ios.driver" value="${invoke.ios.driver}"/>
            <env key="browser.name" value="${browser.name}"/>
            <env key="initialize.browser" value="${initialize.browser}"/>
            <env key="mobile.app.path" value="${mobile.app.path}"/>
            <env key="grid.enabled" value="${grid.enabled}"/>
            <env key="grid.hub" value="${grid.hub}"/>
            <env key="selenium.explicitWaitSeconds" value="${selenium.explicitWaitSeconds}"/>
            <env key="selenium.implicitWaitSeconds" value="${selenium.implicitWaitSeconds}"/>
            <env key="tests.report" value="${tests.report}"/>
            <env key="maxRetryCount" value="${maxRetryCount}"/>
        </testng>

        <!-- Generate the report -->
        <mkdir dir="${tests.report}"/>
        <xslt in="${tests.out}/testng-results.xml" style="${basedir}/testng-template-v1.5.xsl" out="${tests.report}/index.html" classpathref="classpathJars" processor="SaxonLiaison">
            <param name="testNgXslt.outputDir" expression="${tests.report}" />
            <param name="testNgXslt.sortTestCaseLinks" expression="true" />
            <param name="testNgXslt.testDetailsFilter" expression="FAIL,SKIP,PASS,BY_CLASS" />
            <param name="testNgXslt.showRuntimeTotals" expression="true" />
        </xslt>

        <fail message="Some tests failed">
            <condition>
                <available file="${tests.out}/tests-failed"/>
            </condition>
        </fail>
    </target>
</project>
